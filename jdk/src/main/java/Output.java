import java.io.*;
public class Output{
	public static native void printNative(String s);
	public static void init(){
	PrintStream myStream = new PrintStream(System.out) {
			@Override
			public void print(String x) {
				printNative(x);
			}
			@Override
			public void println(String x) {
				printNative(x);
				printNative("\n");
			}
			@Override
			public void print(int x) {
				printNative(Integer.toString(x));
			}
			@Override
			public void println(int x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(long x) {
				printNative(Long.toString(x));
			}
			@Override
			public void println(long x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(boolean x) {
				printNative(Boolean.toString(x));
			}
			@Override
			public void println(boolean x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(float x) {
				printNative(Float.toString(x));
			}
			@Override
			public void println(float x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(char x) {
				printNative(Character.toString(x));
			}
			@Override
			public void println(char x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(char[] x) {
				printNative(new String(x));
			}
			@Override
			public void println(char[] x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(double x) {
				printNative(Double.toString(x));
			}
			@Override
			public void println(double x) {
				print(x);
				printNative("\n");
			}
			@Override
			public void print(Object x) {
				if(x==null)x="null";
				printNative(x.toString());
			}
			@Override
			public void println(Object x) {
				print(x);
				printNative("\n");
			}
		};
		System.setOut(myStream);
	}
	public static void main(String[] args){
		init();
		System.out.println("huhu");
	}
}
