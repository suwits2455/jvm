/*
 * Copyright (c) 2012, Codename One and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Codename One designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *  
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 * 
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Please contact Codename One through http://www.codenameone.com/ if you 
 * need additional information or have any questions.
 */

package java.util;

/**
 *
 * @author Shai Almog
 */
public class Locale {
	public static final Locale ENGLISH = new Locale("en", "");
	public static final Locale US = new Locale("en", "US");
	public static final Locale ROOT = new Locale("","","");


    private static Locale defaultLocale;

    static {
        String language = System.getProperty("user.language", "en");
        String region = System.getProperty("user.region", "US");
        String variant = System.getProperty("user.variant", "");
        defaultLocale = new Locale(language, region);
    }

    private String language;
    private String country;
    private String variant="";

    public Locale(String language, String locale) {
        this.language = language;
        this.country = locale;
    }

    public Locale(String language, String locale,String variant) {
        this.language = language;
        this.country = locale;
	this.variant = variant;
    }
    
    
    public static Locale getDefault() {
        return defaultLocale;
    }
    
    public static void setDefault(Locale l) {
        defaultLocale = l;
    }
    
    public String getLanguage() {
        return language;
    }
        
    public String getCountry() {
        return country;
    }

    public String getVariant() {
        return country;
    }

}
