package com.neoexpert.jvm.method;

import java.nio.ByteBuffer;

public class MException {
	public int start_pc;

	public int end_pc;

	public int handler_pc;

	public int catch_type;

	public MException(ByteBuffer buf) {
		start_pc = buf.getChar();
		end_pc = buf.getChar();
		handler_pc = buf.getChar();
		catch_type = buf.getChar();
	}
}
