package com.neoexpert.jvm.method;

import com.neoexpert.jvm.Attribute;
import com.neoexpert.jvm._class.Class;

import java.nio.ByteBuffer;

public class CodeAttribute extends Attribute {

	private static boolean printparams=false;
	private LineNumberTableAttribute line_number_table;
	public LineNumberTableAttribute getLineNumberTableAttribute(){
		return line_number_table;
	}

	private LocalVariableTableAttribute local_variable_table;

	private LocalVariableTypeTableAttribute local_variable_type_table;


	public CodeAttribute(Attribute a, Class c) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		max_stack = buf.getChar();
		max_locals = buf.getChar();
		code_length = buf.getInt();
		byte[] code = new byte[code_length];
		this.code=code;
		buf.get(code);
		//for (int i = 0; i < code_length; i++)
		//	code[i] = buf.get();
		exception_table_length = buf.getChar();

		exception_table = new MException[exception_table_length];
		for (int i = 0; i < exception_table_length; i++) {
			exception_table[i] = new MException(buf);
		}
		attributes_count = buf.getChar();
		//attrs = new Attribute[attributes_count];
		for (int ix = 0; ix < attributes_count; ix++) {
			Attribute attr = new Attribute(buf);
			int i = attr.attribute_name_index;
			if (i == 0) continue;
			String name = c.getConstant(i).str;
			if (name.equals("LineNumberTable")) {
				line_number_table = new LineNumberTableAttribute(attr);
			}
			if (name.equals("LocalVariableTable")) {
				local_variable_table = new LocalVariableTableAttribute(attr);
			}
			if (name.equals("LocalVariableTypeTable")) {
				local_variable_type_table = new LocalVariableTypeTableAttribute(attr);
			}

		}
		//delete info
		info=null;
	}
	public MException[] getExceptionTable() {
		return exception_table;
	}
//	public CodeAttribute(ByteBuffer buf)
//	{
//		attribute_name_index = buf.getChar();
//		attribute_length = buf.getInt();
//		max_stack = buf.getChar();
//		max_locals = buf.getChar();
//		code_length = buf.getInt();
//		code = new byte[code_length];
//		for (int i=0; i < code_length;i++)
//			code[i] = buf.get();
//		exception_table_length = buf.getChar();
//
//		exception_table = new MException[exception_table_length];
//		for (int i=0;i < exception_table_length;i++)
//		{
//			exception_table[i] = new MException(buf);
//		}
//		attributes_count = buf.getChar();
//		attrs = new Attribute[attributes_count];
//		for (int ix=0; ix < attributes_count; ix++)
//		{
//			attrs[ix] = new Attribute(buf);
//		}
//	}

	int attribute_name_index;
	int attribute_length;
	public int max_stack;
	public int max_locals;
	int code_length;
	Object code;
	int[] params;
	int code_type;
	public Object getCode(){
		return code;
	}
	public void setCode(Object code){
		this.code=code;
	}
	public void setParams(int[] params){
		this.params=params;
	}
	public int[] getParams(){
		return params;
	}
	char exception_table_length;

	public int getCurrentLine(int pc) {
		return line_number_table.getLineNumber(pc);
	}

	MException checkException(int pc) {
		for (MException e : exception_table)
			if (e.start_pc <= pc && e.end_pc >= pc)
				return e;
		return null;
	}

	MException[] exception_table;
	char attributes_count;
	//Attribute[] attrs;

	@Override
	public String toString() {
		String s = "";
		s += "attribute_name_index: " + attribute_name_index + "\n";
		s += "attribute_length: " + attribute_length + "\n";
		s += "max_stack: " + max_stack + "\n";
		s += "max_locals: " + max_locals + "\n";
		s += "code_length: " + code_length + "\n";
		s += "code:\n\n";
		s += "----------------------\n";
		s += parseCode((byte[])code);
		s += "----------------------\n";
		return s;
	}

	String cs;

	private String parseCode(byte[] code) {
		StringBuilder cs = new StringBuilder();
		int pc = 0;
		while (pc < code.length) {
			//StringBuilder cs = new StringBuilder();
			pc += CodeAttribute.parseOp(pc, code, cs);
			cs.append("\n");
			//ops.add(cs.toString());
		}
		return cs.toString();
	}

	public static int parseOp(int pc, Object _code, StringBuilder cs) {
		//byte[] code= (byte[]) _code;
		short[] code= (short[]) _code;
		int op = code[pc];

		switch (op) {
			case (byte) 0x00:
				cs.append("nop");
				return 1;
			case (byte) 0x01:
				cs.append("aconst_null");
				return 1;
			case (byte) 0x02:
				cs.append("iconst_m1");
				return 1;
			case (byte) 0x03:
				cs.append("iconst_0");
				return 1;
			case (byte) 0x04:
				cs.append("iconst_1");
				return 1;
			case (byte) 0x05:
				cs.append("iconst_2");
				return 1;
			case (byte) 0x06:
				cs.append("iconst_3");
				return 1;
			case (byte) 0x07:
				cs.append("iconst_4");
				return 1;
			case (byte) 0x08:
				cs.append("iconst_5");
				return 1;
			case (byte) 0x09:
				cs.append("lconst_0");
				return 1;
			case (byte) 0x0A:
				cs.append("lconst_1");
				return 1;
			case (byte) 0x0B:
				cs.append("fconst_0");
				return 1;
			case (byte) 0x0C:
				cs.append("fconst_1");
				return 1;
			case (byte) 0x0D:
				cs.append("fconst_2");
				return 1;
			case (byte) 0x0E:
				cs.append("dconst_0");
				return 1;
			case (byte) 0x0F:
				cs.append("dconst_1");
				return 1;
			case (byte) 0x10:
				cs.append("bipush ");
				if(printparams)
					cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x11:
				cs.append("sipush ");
				if(printparams)
					cs.append(i((byte) code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0x12:
				cs.append("ldc ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x13:
				cs.append("ldc_w ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0x14:
				cs.append("ldc2_w ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0x15:
				cs.append("iload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x16:
				cs.append("lload ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 1] + 1);
				return 2;
			case (byte) 0x17:
				cs.append("fload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x18:
				cs.append("dload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x19:
				cs.append("aload ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x1A:
				cs.append("iload_0");
				return 1;
			case (byte) 0x1B:
				cs.append("iload_1");
				return 1;
			case (byte) 0x1C:
				cs.append("iload_2");
				return 1;
			case (byte) 0x1D:
				cs.append("iload_3");
				return 1;
			case (byte) 0x1E:
				cs.append("lload_0");
				return 1;
			case (byte) 0x1F:
				cs.append("lload_1");
				return 1;
			case (byte) 0x20:
				cs.append("lload_2");
				return 1;
			case (byte) 0x21:
				cs.append("lload_3");
				return 1;
			case (byte) 0x22:
				cs.append("fload_0");
				return 1;
			case (byte) 0x23:
				cs.append("fload_1");
				return 1;
			case (byte) 0x24:
				cs.append("fload_2");
				return 1;
			case (byte) 0x25:
				cs.append("fload_3");
				return 1;
			case (byte) 0x26:
				cs.append("dload_0");
				return 1;
			case (byte) 0x27:
				cs.append("dload_1");
				return 1;
			case (byte) 0x28:
				cs.append("dload_2");
				return 1;
			case (byte) 0x29:
				cs.append("dload_3");
				return 1;
			case (byte) 0x2A:
				cs.append("aload_0");
				return 1;
			case (byte) 0x2B:
				cs.append("aload_1");
				return 1;
			case (byte) 0x2C:
				cs.append("aload_2");
				return 1;
			case (byte) 0x2D:
				cs.append("aload_3");
				return 1;
			case (byte) 0x2E:
				cs.append("iaload");
				return 1;
			case (byte) 0x2F:
				cs.append("laload");
				return 1;
			case (byte) 0x30:
				cs.append("faload");
				return 1;
			case (byte) 0x31:
				cs.append("daload");
				return 1;
			case (byte) 0x32:
				cs.append("aaload");
				return 1;
			case (byte) 0x33:
				cs.append("baload");
				return 1;
			case (byte) 0x34:
				cs.append("caload");
				return 1;
			case (byte) 0x35:
				cs.append("saload");
				return 1;
			case (byte) 0x36:
				cs.append("istore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x37:
				cs.append("lstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x38:
				cs.append("fstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x39:
				cs.append("dstore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x3A:
				cs.append("astore ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0x3B:
				cs.append("istore_0");
				return 1;
			case (byte) 0x3C:
				cs.append("istore_1");
				return 1;
			case (byte) 0x3D:
				cs.append("istore_2");
				return 1;
			case (byte) 0x3E:
				cs.append("istore_3");
				return 1;
			case (byte) 0x3F:
				cs.append("lstore_0");
				return 1;
			case (byte) 0x40:
				cs.append("lstore_1");
				return 1;
			case (byte) 0x41:
				cs.append("lstore_2");
				return 1;
			case (byte) 0x42:
				cs.append("lstore_3");
				return 1;

			case (byte) 0x43:
				cs.append("fstore_0");
				return 1;
			case (byte) 0x44:
				cs.append("fstore_1");
				return 1;
			case (byte) 0x45:
				cs.append("fstore_2");
				return 1;
			case (byte) 0x46:
				cs.append("fstore_3");
				return 1;
			case (byte) 0x47:
				cs.append("dstore_0");
				return 1;
			case (byte) 0x48:
				cs.append("dstore_1");
				return 1;
			case (byte) 0x49:
				cs.append("dstore_2");
				return 1;
			case (byte) 0x4A:
				cs.append("dstore_3");
				return 1;
			case (byte) 0x4B:
				cs.append("astore_0");
				return 1;
			case (byte) 0x4C:
				cs.append("astore_1");
				return 1;
			case (byte) 0x4D:
				cs.append("astore_2");
				return 1;
			case (byte) 0x4E:
				cs.append("astore_3");
				return 1;
			case (byte) 0x4F:
				cs.append("iastore");
				return 1;
			case (byte) 0x50:
				cs.append("lastore");
				return 1;
			case (byte) 0x51:
				cs.append("fastore");
				return 1;
			case (byte) 0x52:
				cs.append("dastore");
				return 1;
			case (byte) 0x53:
				cs.append("aastore");
				return 1;
			case (byte) 0x54:
				cs.append("bastore");
				return 1;
			case (byte) 0x55:
				cs.append("castore");
				return 1;
			case (byte) 0x56:
				cs.append("sastore");
				return 1;
			case (byte) 0x57:
				cs.append("pop");
				return 1;
			case (byte) 0x58:
				cs.append("pop2");
				return 1;
			case (byte) 0x59:
				cs.append("dup");
				return 1;
			case (byte) 0x5A:
				cs.append("dup_x1");
				return 1;
			case (byte) 0x5B:
				cs.append("dup_x2");
				return 1;
			case (byte) 0x5C:
				cs.append("dup2");
				return 1;
			case (byte) 0x5D:
				cs.append("dup2_x1");
				return 1;
			case (byte) 0x5E:
				cs.append("dup2_x2");
				return 1;
			case (byte) 0x5F:
				cs.append("swap");
				return 1;
			case (byte) 0x60:
				cs.append("iadd");
				return 1;
			case (byte) 0x61:
				cs.append("ladd");
				return 1;
			case (byte) 0x62:
				cs.append("fadd");
				return 1;
			case (byte) 0x63:
				cs.append("dadd");
				return 1;
			case (byte) 0x64:
				cs.append("isub");
				return 1;
			case (byte) 0x65:
				cs.append("lsub");
				return 1;
			case (byte) 0x66:
				cs.append("fsub");
				return 1;
			case (byte) 0x67:
				cs.append("dsub");
				return 1;
			case (byte) 0x68:
				cs.append("imul");
				return 1;
			case (byte) 0x69:
				cs.append("lmul");
				return 1;
			case (byte) 0x6A:
				cs.append("fmul");
				return 1;
			case (byte) 0x6B:
				cs.append("dmul");
				return 1;
			case (byte) 0x6C:
				cs.append("idiv");
				return 1;
			case (byte) 0x6D:
				cs.append("ldiv");
				return 1;
			case (byte) 0x6E:
				cs.append("fdiv");
				return 1;
			case (byte) 0x6F:
				cs.append("ddiv");
				return 1;
			case (byte) 0x70:
				cs.append("irem");
				return 1;
			case (byte) 0x71:
				cs.append("lrem");
				return 1;
			case (byte) 0x72:
				cs.append("frem");
				return 1;
			case (byte) 0x73:
				cs.append("drem");
				return 1;
			case (byte) 0x74:
				cs.append("ineg");
				return 1;
			case (byte) 0x75:
				cs.append("lneg");
				return 1;
			case (byte) 0x76:
				cs.append("fneg");
				return 1;
			case (byte) 0x77:
				cs.append("dneg");
				return 1;
			case (byte) 0x78:
				cs.append("ishl");
				return 1;
			case (byte) 0x79:
				cs.append("lshl");
				return 1;
			case (byte) 0x7A:
				cs.append("ishr");
				return 1;
			case (byte) 0x7B:
				cs.append("lshr");
				return 1;
			case (byte) 0x7C:
				cs.append("iushr");
				return 1;
			case (byte) 0x7D:
				cs.append("lushr");
				return 1;
			case (byte) 0x7E:
				cs.append("iand");
				return 1;
			case (byte) 0x7F:
				cs.append("land");
				return 1;
			case (byte) 0x80:
				cs.append("ior");
				return 1;
			case (byte) 0x81:
				cs.append("lor");
				return 1;
			case (byte) 0x82:
				cs.append("ixor");
				return 1;
			case (byte) 0x83:
				cs.append("lxor");
				return 1;
			case (byte) 0x84:
				cs.append("iinc ");
				if(printparams)
						cs.append((int) code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0x85:
				cs.append("i2l ");
				return 1;
			case (byte) 0x86:
				cs.append("i2f");
				return 1;
			case (byte) 0x87:
				cs.append("i2d ");
				return 1;
			case (byte) 0x88:
				cs.append("l2i ");
				return 1;
			case (byte) 0x89:
				cs.append("l2f ");
				return 1;
			case (byte) 0x8A:
				cs.append("l2d ");
				return 1;
			case (byte) 0x8B:
				cs.append("f2i ");
				return 1;
			case (byte) 0x8C:
				cs.append("f2l ");
				return 1;
			case (byte) 0x8D:
				cs.append("f2d ");
				return 1;
			case (byte) 0x8E:
				cs.append("d2i ");
				return 1;
			case (byte) 0x8F:
				cs.append("d2l ");
				return 1;
			case (byte) 0x90:
				cs.append("d2f ");
				return 1;
			case (byte) 0x91:
				cs.append("i2b ");
				return 1;
			case (byte) 0x92:
				cs.append("i2c ");
				return 1;
			case (byte) 0x93:
				cs.append("i2s ");
				return 1;
			case (byte) 0x94:
				cs.append("i2s ");
				return 1;
			case (byte) 0x95:
				cs.append("fcmpl ");
				return 1;
			case (byte) 0x96:
				cs.append("fcmpg ");
				return 1;
			case (byte) 0x97:
				cs.append("dcmpl ");
				return 1;
			case (byte) 0x98:
				cs.append("dcmpg ");
				return 1;
			case (byte) 0x99:
				cs.append("ifeq ");
				return 3;
			case (byte) 0x9A:
				cs.append("ifne ");
				return 3;
			case (byte) 0x9B:
				cs.append("iflt ");
				return 3;
			case (byte) 0x9C:
				cs.append("ifge ");
				return 3;
			case (byte) 0x9D:
				cs.append("ifgt ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (byte) 0x9E:
				cs.append("ifle ");
				return 3;
			case (byte) 0x9F:
				cs.append("if_icmpeq ");
				return 3;
			case (byte) 0xA0:
				cs.append("if_icmpne ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (byte) 0xA1:
				cs.append("if_icmplt ");
				if(printparams)
						cs.append(((int) code[pc + 1] << 8) | (int) code[pc + 2]);
				return 3;
			case (byte) 0xA2:
				cs.append("if_icmpge ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA3:
				cs.append("if_icmpgt ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA4:
				cs.append("if_icmple ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA5:
				cs.append("if_acmpeq ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA6:
				cs.append("if_acmpne ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA7:
				cs.append("goto ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xA8:
				cs.append("jsr ");
				if(printparams)
					cs.append(i((byte) code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xA9:
				cs.append("ret ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0xAA:
				cs.append("tableswitch ");
				int pc2 = pc + (4 - pc % 4);
				//int default_offset=pc(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				int low = i((byte)code[pc2 + 0], (byte)code[pc2 + 1], (byte)code[pc2 + 2], (byte)code[pc2 + 3]);
				pc2 += 4;
				int high = i((byte)code[pc2 + 0], (byte)code[pc2 + 1], (byte)code[pc2 + 2], (byte)code[pc2 + 3]);
				int n = high - low + 1;
				pc2 += n * 4;
				return pc2 - pc + 1;
			case (byte) 0xAB:
				cs.append("lookupswitch ");
				pc2 = pc + (4 - pc % 4);

				//int default_offset=pc(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
				pc2 += 4;
				n = i((byte)code[pc2 + 0], (byte)code[pc2 + 1], (byte)code[pc2 + 2], (byte)code[pc2 + 3]);
				pc2 += 4;
				for (pc = 0; pc < n; pc++) {
					//int key = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					//int value = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
				}
				return pc2 - pc + 1;
			case (byte) 0xAC:
				cs.append("ireturn");
				return 1;
			case (byte) 0xAD:
				cs.append("lreturn");
				return 1;
			case (byte) 0xAE:
				cs.append("freturn");
				return 1;
			case (byte) 0xAF:
				cs.append("dreturn");
				return 1;
			case (byte) 0xB0:
				cs.append("areturn");
				return 1;
			case (byte) 0xB1:
				cs.append("return");
				return 1;
			case (byte) 0xB2:
				cs.append("getstatic ");
				if(printparams)
						cs.append((byte)i((byte)code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xB3:
				cs.append("putstatic ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0xB4:
				cs.append("getfield ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0xB5:
				cs.append("putfield ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0xB6:
				cs.append("invokevirtual ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xB7:
				cs.append("invokespecial ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xB8:
				cs.append("invokestatic ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2])).append(" ");
				return 3;
			case (byte) 0xB9:
				cs.append("invokeinterface ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2])).append(" ");
				return 5;
			case (byte) 0xBA:
				cs.append("invokedynamic ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2])).append(" ");
				return 5;
			case (byte) 0xBB:
				cs.append("new ");
				if(printparams)
						cs.append(code[pc + 1]).append(" ").append(code[pc + 2]);
				return 3;
			case (byte) 0xBC:
				cs.append("newarray ");
				if(printparams)
						cs.append(code[pc + 1]);
				return 2;
			case (byte) 0xBD:
				cs.append("anewarray ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xBE:
				cs.append("arraylength ");
				return 1;
			case (byte) 0xBF:
				cs.append("athrow");
				return 1;
			case (byte) 0xC0:
				cs.append("checkcast");
				return 3;
			case (byte) 0xC1:
				cs.append("instanceof ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2]));
				return 3;
			case (byte) 0xC2:
				cs.append("monitorenter");
				return 1;
			case (byte) 0xC3:
				cs.append("monitorexit");
				return 1;
			case (byte) 0xC4:
				cs.append("wide");
				if (code[pc + 1] == 0x3A)
					return 5;
				else
					return 3;
			case (byte) 0xC5:
				cs.append("multianewarray");
				return 4;
			case (byte) 0xC6:
				cs.append("ifnull");
				return 3;
			case (byte) 0xC7:
				cs.append("ifnonnull ");
				if(printparams)
						cs.append((code[pc + 1] << 8) | code[pc + 2]);
				return 3;
			case (byte) 0xC8:
				cs.append("goto_w ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2], (byte)code[pc + 3], (byte)code[pc + 4]));
				return 5;
			case (byte) 0xC9:
				cs.append("jsr_w ");
				if(printparams)
						cs.append(i((byte)code[pc + 1], (byte)code[pc + 2], (byte)code[pc + 3], (byte)code[pc + 4]));
				return 5;
			case (byte) 0xCA:
				cs.append("breakpoint");
				return 1;
			default:
				cs.append(byteToHex((byte)code[pc]));
		}
		return 1;
	}

	private static int i(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(byte b1, byte b2, byte b3, byte b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static String byteToHex(byte b) {
		char[] hexChars = new char[2];
		int v = b & 0xFF;
		hexChars[0] = hexArray[v >>> 4];
		hexChars[1] = hexArray[v & 0x0F];

		return new String(hexChars);
	}


}
