package com.neoexpert.jvm.method;

import com.neoexpert.jvm.Attribute;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.executor.optimization.ByteCodeToShortCode;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;


public class Method extends AMethod {

	private int paramscount;
	private final String signature;

	public int getMaxStack() {
		if (codeattr != null)
			return codeattr.max_stack;
		return 0;
	}

	public int getMaxLocals() {
		return codeattr.max_locals;
	}


	public static final int ACC_PUBLIC = 0x0001;//	Declared public; may be accessed from outside its package.
	public static final int ACC_PRIVATE = 0x0002;//	Declared private; accessible only within the defining class.
	public static final int ACC_PROTECTED = 0x0004;//	Declared protected; may be accessed within subclasses.
	public static final int ACC_STATIC = 0x0008;//	Declared static.
	public static final int ACC_FINAL = 0x0010;//	Declared final; must not be overridden (§5.4.5).
	public static final int ACC_SYNCHRONIZED = 0x0020;//	Declared synchronized; invocation is wrapped by a monitor use.
	public static final int ACC_BRIDGE = 0x0040;//	A bridge method, generated by the compiler.
	public static final int ACC_VARARGS = 0x0080;//	Declared with variable number of arguments.
	public static final int ACC_NATIVE = 0x0100;//	Declared native; implemented in a language other than Java.
	public static final int ACC_ABSTRACT = 0x0400;//	Declared abstract; no implementation is provided.
	public static final int ACC_STRICT = 0x0800;//	Declared strictfp; floating-point mode is FP-strict.
	public static final int ACC_SYNTHETIC = 0x1000;

	public int getModifiers() {
		return access_flags;
	}

	public int getCurrentLine(int pc) {
		return codeattr.getCurrentLine(pc);
	}

	public MException checkException(int pc) {
		return codeattr.checkException(pc);
	}



	public boolean isAbstract() {
		// TODO: Implement this method
		return (access_flags & 0x0400) != 0;
	}

	@Override
	public boolean isStatic() {
		return (access_flags & ACC_STATIC) != 0;
	}

	public boolean isNative() {
		return (access_flags & ACC_NATIVE) != 0;
	}

	@Override
	public void setNative(boolean b) {
		access_flags |= 0x0100;
	}





	private String desc;


	private CodeAttribute codeattr;

	public Method(ByteBuffer buf, Class cl) {
		super(cl);
		access_flags = buf.getShort();
		//log("access flags: "+Integer.toHexString(af1));
		name_index = buf.getChar();
		//log("name_index: "+Integer.toHexString(af1));
		descriptor_index = buf.getChar();
		if (name_index != 0)
			name = cl.getConstant(name_index).str;
		if (descriptor_index != 0) {
			desc = cl.getConstant(descriptor_index).str;
			args = parseParams(desc);
		}
		signature=name+desc;//.substring(0,desc.indexOf(")")+1);
		//log("descriptor_index: "+Integer.toHexString(af1));
		attributes_count = buf.getChar();
		//log("attributes_count: "+Integer.toHexString(num));
		//attrs = new Attribute[attributes_count];
		for (int ix = 0; ix < attributes_count; ix++) {
			Attribute a = new Attribute(buf);
			//attrs[ix] = a;

			String name =
					cl.getConstant(a.attribute_name_index).str;
			if (name.equals("Code")) {
				codeattr = new CodeAttribute(a, cl);
				a = codeattr;
				if(name.equals("<clinit>()V"))
					code=codeattr.code;
				//attrs[ix] = a;
			}
		}
		//attrs=new Attribute[attributes_count];
	}

	public void write(DataOutput raf) throws IOException {
		raf.writeShort(access_flags);
		raf.writeChar(name_index);
		raf.writeChar(descriptor_index);
		raf.writeChar(attrs.length);
		for (Attribute a : attrs) {
			a.write(raf);
		}
	}


	public ArrayList<Character> parseParams(String desc) {
		int paramscount = 0;
		desc = desc.substring(1);
		ArrayList<Character> params = new ArrayList<>(16);

		while (true) {
			char c = desc.charAt(0);
			if (c == ')')
				break;
			switch (c) {
				case 'Z':
				case 'B':
				case 'C':
				case 'S':
				case 'I':
				case 'F':
					params.add(c);
					break;
				case 'D':
				case 'J':
					params.add(c);
					params.add(c);
					break;
				case 'L':
					params.add(c);
					desc = desc.substring(desc.indexOf(';') + 1);
					continue;
				case '[':
					params.add(c);
					char at = desc.charAt(1);
					while(at=='['){
						desc=desc.substring(1);
						at = desc.charAt(1);

					}
					if(at=='L')
						desc = desc.substring(desc.indexOf(';') + 1);
					else
						desc = desc.substring(2);
					continue;
			}
			desc = desc.substring(1);
		}
		this.paramscount = paramscount;
		return params;
	}

	//	method_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}
	private int access_flags;
	public int name_index;
	public int descriptor_index;
	public int attributes_count;
	Attribute[] attrs;

	@Override
	public int getParamsArrayLength() {
		return args.size();
	}

	@Override
	public int getParameterCount() {
		return paramscount;
	}


	private boolean tried_to_optimize=false;
	public int invoked=0;
	public Object getCode() {
		if(code==null)
			if (codeattr != null)
			{
				invoked++;
				if(invoked<4) {
					return codeattr.code;
				}
				if (!tried_to_optimize)
				{
					tried_to_optimize=true;
					/*
					Optimizer o = new Optimizer(codeattr.code);
					if(o.success()){
						params=o.getParams();
						code=o.getCode();
						code_type=OPTIMIZED_BYTE_CODE;
						return code;
					}
					//*/
					///*
					ByteCodeToShortCode os = new ByteCodeToShortCode(codeattr);
					if(os.success()){
						params=codeattr.getParams();
						code_type=OPTIMIZED_SHORT_CODE;
					}
					else
					{
						throw new RuntimeException("optimization error");
					}
					//*/
				}
				code= codeattr.code;
			}
		return code;
	}
	private int[] params=null;
	public static final int BYTE_CODE=2;
	public static final int OPTIMIZED_BYTE_CODE=3;
	public static final int OPTIMIZED_SHORT_CODE=4;
	public int code_type=BYTE_CODE;
	private Object code=null;
	public int[] getParams(){
		return params;
	}
	public int getCodeType() {
		return code_type;
	}


	public String getSignature() {
		return signature;
	}

	public String getDesc() {
		return desc;
	}

	@Override
	public String toString() {
		return signature;
	}

}
