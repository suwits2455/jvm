package com.neoexpert.jvm.method;

import com.neoexpert.jvm.Attribute;

import java.nio.ByteBuffer;

public class ExceptionsAttribute extends Attribute {
	/*
	 Exceptions_attribute {
	 	u2 attribute_name_index;
	 	u4 attribute_length;
	 	u2 number_of_exceptions;
	 	u2 exception_index_table[number_of_exceptions];
	 }
	*/
	public ExceptionsAttribute(Attribute a) {
		super(a);

		ByteBuffer buf = ByteBuffer.wrap(a.info);
	}
}
