package com.neoexpert.jvm.method;

import com.neoexpert.jvm.Attribute;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class LineNumberTableAttribute extends Attribute {
	private Map<Integer, Integer> table = new HashMap<>();
	public Map<Integer, Integer> getTable(){
		return table;
	}
	public void setTable(Map<Integer,Integer> table){
		this.table=table;
	}

	public LineNumberTableAttribute(Attribute a) {
		super(a);

		ByteBuffer buf = ByteBuffer.wrap(a.info);
		int line_number_table_length = buf.getChar();
		for (int i = 0; i < line_number_table_length; i++) {
			table.put((int) buf.getChar(), (int) buf.getChar());
		}
		info=null;
	}

	public int getLineNumber(int pc) {
		if (pc < 0)
			return 0;
		Integer l = table.get(pc);
		if (l == null)
			return getLineNumber(pc - 1);
		return l;
	}
}
