package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.instance.*;
import com.neoexpert.jvm.classpath.JarClassPath;
import com.neoexpert.jvm.natives.*;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.net.*;


public class JVM {
	public static int system_thread_group;
	private static ThreadScheduler ts=new ThreadScheduler();
	public static boolean verbose=false;

	public static void notify(MyThread t){
		ts.notify(t);
	}
	public static void addThread(MyThread t){
		ts.add(t);
	}


	private static void createSystemThreadGroup() {
		AClass c = Class.getClass("java/lang/ThreadGroup");
		MyThread t = new MyThread(c, "system_thread_group_creater");
		system_thread_group = t.newInstance("<init>()V");
		Heap.makePermanent(system_thread_group);
	}

	public static void init() {

		Class.getClass("java/lang/Class");
		Class.getClass("java/lang/Object");
		Class.getClass("java/lang/Runtime");
		Class.getClass("java/lang/Math");
		Class.getClass("java/lang/Thread");
		Class.getClass("java/lang/Class");
		Class.getClass("java/lang/String");
		createSystemThreadGroup();

		Class.getClass("sun/reflect/ReflectionFactory$GetReflectionFactoryAction");
		Class.getClass("java/lang/ClassLoader");

		Class.getClass("java/util/HashMap");
		createSystemThreadGroup();

		Class.getClass("java/util/Map");
		Class.getClass("java/util/AbstractMap");
		Class.getClass("java/util/HashMap$Node");
		Class.getClass("java/util/HashMap$TreeNode");
		Class.getClass("java/util/LinkedHashMap$Entry");
		Class.getClass("java/util/Map$Entry");
		Class.getClass("java/lang/Comparable");


		Class.getClass("java/util/Arrays");

		Class.getClass("sun/reflect/Reflection");
		///*
		Class.getClass("sun/reflect/ReflectionFactory");
		Class.getClass("java/lang/reflect/Field");
		Class.getClass("java/security/AccessControlContext");
		Class.getClass("sun/reflect/ReflectionFactory$GetReflectionFactoryAction");

		Class.getClass("java/lang/reflect/Modifier");

		///*
		Class.getClass("java/io/BufferedInputStream");
		Class.getClass("sun/nio/cs/StreamEncoder");

		if(verbose)
			System.out.println("before initializeSystemClass...");
		System.gc();
		initializeSystemClass();
		initOutput();
		NOutput.setOut(System.out);

		Class.getClass("java/io/File");
		Class.getClass("java/io/File$PathStatus");
		//*/
	}

	private static void initOutput() {
		AClass c = Class.getClass("Output");

		//t=c.createInitMethod();
		MyThread t = new MyThread(c, "main" );


		t.setMethodToRun("init()V");
		t.run();
	}

	public static void initializeSystemClass() {
		AClass c = Class.getClass("java/lang/System");

		//t=c.createInitMethod();
		MyThread t = new MyThread(c, "main",true);
		//t.setMethodToRun("main([Ljava/lang/String;)V");


		t.setMethodToRun("initializeSystemClass()V");
		t.run();
	}


	public static void main(String[] args) throws Exception {
		if(args==null){
			printUsage();
			return;
		}
		if(args.length==0){
			printUsage();
			return;
		}
		String mainClass = "Main";
		String jarfile = null;
		int args_offset=0;
		loop:for(int i=0;i<args.length;i++){
			String arg=args[i];
			if(arg.charAt(0)=='-'){
				switch (arg){
					case "-jar":
						jarfile = args[i+1];
						args_offset=i+2;
						break loop;
					case "-verbose":
						verbose=true;
						break;
					case "-gcstats":
						Heap.printgcstats=true;
						break;
				}
			}
			else{
				mainClass=arg;
				args_offset=i+1;
				break;
			}

		}
		if(verbose)
			System.out.println("starting JVM...");

		String cp = ".";

		if (jarfile != null) {
			ZipFile jar = new ZipFile(jarfile);
			ZipEntry e = jar.getEntry("META-INF/MANIFEST.MF");
			if (e == null) {
				throw new RuntimeException("META-INF/MANIFEST.MF not found");
			}
			InputStream is = jar.getInputStream(e);
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(is));
			String line;  ;
			while ((line=reader.readLine())!=null) {
				if (line.startsWith("Main-Class: ")) {
					mainClass = line.substring("Main-Class: ".length());
					mainClass = mainClass.replaceAll("\\.", "/");
					Class.setJar(new File(jarfile));
					break;
				}
			}


		}
		//META-INF/MANIFEST.MF
		String s = new File(cp).getAbsolutePath();
		Class.addToClasspath(s);

		URL url=JVM.class.getProtectionDomain().getCodeSource().getLocation();
		File rt = new File(
				url.toURI());
		if (rt.isDirectory()) {
			rt = new File(rt.getParent(), "java.jar");
			JarClassPath jcp = new JarClassPath(rt, "rt/");
			Class.addToClasspath(jcp);
			JarClassPath jcp2 = new JarClassPath(rt);
			Class.addToClasspath(jcp2);
			//Class.addToClasspath(rt.getAbsolutePath() + "/rt/");
			//Class.addToClasspath(rt.getAbsolutePath());
		} else {
			JarClassPath jcp = new JarClassPath(rt, "rt/");
			Class.addToClasspath(jcp);
		}
		//Class.addJar("rt.jar");
		//Class.addJar("charsets.jar");
		if(verbose)
			System.out.println("before init...");
		init();
		if(verbose)
			System.out.println("intialized...");
		AClass c = Class.getClass(mainClass);


		MyThread t = new MyThread(c, "main",true );
		try {
			ArrayInstance ai=new ArrayInstance(args.length-args_offset,ArrayInstance.T_OBJECT);
			for(int i=args_offset;i<args.length;i++){
				((int[])ai.a)[i-args_offset]= NString.createInstance(args[i]);
			}
			int aref = Heap.add(ai);
			t.setStaticMethodToRun("main([Ljava/lang/String;)V", aref);
			ts.setMainThread(t);
			ts.run();
		} catch (Exception e) {
			//t.getCurrentFrame();
			throw new RuntimeException(t.getStackTrace(), e);
		}
		//Heap.gc();
		//System.out.println(Heap.tostring());
		//print all loaded classes
	    /*
	       Set<String> classes=Class.classes.keySet();
	       for(String cl:classes)
	       System.out.println(cl);
	       */
		//System.out.println(OptimizedShortCodeExecutor.s);


	}

	private static void printUsage() {
		System.out.println("Usage: java -jar java.jar");
	}
}
