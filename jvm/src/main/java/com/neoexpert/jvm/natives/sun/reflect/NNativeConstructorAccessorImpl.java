package com.neoexpert.jvm.natives.sun.reflect;

import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.natives.java.lang.NString;

public class NNativeConstructorAccessorImpl {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "newInstance0(Ljava/lang/reflect/Constructor;[Ljava/lang/Object;)Ljava/lang/Object;":
				newInstance0(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void newInstance0(Frame cF, Object[] lv, MyThread t) {
		int oref1 = (int) lv[0];
		int oref2 = (int) lv[1];
		ClassInstance constructor = (ClassInstance) Heap.get(oref1);
		int oref3 = (int) constructor.getField("signature");
		String signature = NString.toNative(oref3);
		int oref4 = (int) constructor.getField("clazz");
		ClassInstance clazz = (ClassInstance) Heap.get(oref4);
		int created_instance = new MyThread((AClass) clazz.cl,"NNativeConstructorAccessorImpl").newInstance(signature);
		Object args = Heap.get(oref2);
		cF.push(created_instance);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
