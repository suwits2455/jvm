package com.neoexpert.jvm.natives.java.io;


import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.io.File;


public class NUnixFileSystem {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "initIDs()V":
				initIDs(cF, lv, t);
				return false;
			case "getBooleanAttributes0(Ljava/io/File;)I":
				getBooleanAttributes0(cF, lv, t);
				return false;
			case "getLastModifiedTime(Ljava/io/File;)J":
				getLastModifiedTime(cF, lv, t);
				return false;
			case "getLength(Ljava/io/File;)J":
				getLength(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static int l2i1(long l) {
		return (int) (l >> 32);
	}

	private static int l2i2(long l) {
		return (int) l;
	}

	private static void getLength(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance _this = (ClassInstance) Heap.get(oref);
		int oref2 = (int) lv[1];
		ClassInstance _file = (ClassInstance) Heap.get(oref2);
		String path = NString.toNative((Integer) _file.getField("path"));
		int ba = 0;
		File f = new File(path);
		long len = f.length();
		cF.push(len);
		cF.push(null);
	}
	private static void getLastModifiedTime(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance _this = (ClassInstance) Heap.get(oref);
		int oref2 = (int) lv[1];
		ClassInstance _file = (ClassInstance) Heap.get(oref2);
		String path = NString.toNative((Integer) _file.getField("path"));
		int ba = 0;
		File f = new File(path);
		long last_modified = f.lastModified();
		cF.push(last_modified);
		cF.push(null);
	}

	private static final int BA_EXISTS = 0x01;
	private static final int BA_REGULAR = 0x02;
	private static final int BA_DIRECTORY = 0x04;
	private static final int BA_HIDDEN = 0x08;

	private static void getBooleanAttributes0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance _this = (ClassInstance) Heap.get(oref);
		int oref2 = (int) lv[1];
		ClassInstance _file = (ClassInstance) Heap.get(oref2);
		String path = NString.toNative((Integer) _file.getField("path"));
		int ba = 0;
		File f = new File(path);
		if (f.exists())
			ba |= BA_EXISTS;
		if (f.isFile())
			ba |= BA_REGULAR;
		if (f.isDirectory())
			ba |= BA_DIRECTORY;
		if (f.isHidden())
			ba |= BA_HIDDEN;
		cF.push(ba);
	}

	private static void initIDs(Frame cF, Object[] lv, MyThread t) {
		// TODO: Implement this method
	}


	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}


}
