package com.neoexpert.jvm.natives.java.lang.reflect;

import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.natives.NativeMethod;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class NArray extends AClass {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "clone()Ljava/lang/Object;":
				clone(cF, lv, t);
				return false;
			case "getClass()Ljava/lang/Class;":
				getClass(cF, lv, t);
				return false;
			case "getComponentType()Ljava/lang/Class;":
				getComponentType(cF, lv, t);
				return false;
			case "newArray(Ljava/lang/Class;I)Ljava/lang/Object;":
				newArray(cF, lv, t);
				return false;
			case "isArray()Z":
				isArray(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void isArray(Frame cF, Object[] lv, MyThread t) {
		cF.push(1);
	}

	private static void getComponentType(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		String s = (String) ci.cl;
		int coref = 0;
		if (s.charAt(0) == '[') {
			s = s.substring(1);

			if (s.charAt(0) == 'L') {
				coref = Class.getClass("java/lang/Object").getMyInstanceRef();
			} else
				coref = Class.getPrimitiveClass(s);
		}
		cF.push(coref);
	}

	private static void getClass(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ArrayInstance ai = (ArrayInstance) Heap.get(oref);
		AClass cl = ai.getMyClass();
		cF.push(cl.getMyInstanceRef());
	}

	private static void clone(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ArrayInstance ai = (ArrayInstance) Heap.get(oref);
		ai = ai.cloneme();
		oref = Heap.add(ai);
		cF.push(oref);

	}

	private static void newArray(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int size = (int) lv[1];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		ArrayInstance ai;
		if (ci.cl instanceof String)
			ai = new ArrayInstance(size, ((String) ci.cl).charAt(0));
		else
			ai = new ArrayInstance(size, 'L');
		cF.push(Heap.add(ai));
	}

	private final char type;

	public NArray(char at) {
		this.type = at;
	}

	@Override
	public void putStatic(String fname, Object v) {

	}

	@Override
	public void putStatic(int id, Object obj) {

	}

	@Override
	public boolean isInitialized() {
		return false;
	}

	@Override
	public void clinit() {

	}

	@Override
	public Constant getConstant(int i) {
		return null;
	}

	@Override
	public boolean isInstance(Object obj) {
		return false;
	}

	@Override
	public boolean isInstance(AClass cl) {
		return false;
	}

	@Override
	public String getName() {
		return "NArray";
	}

	@Override
	public Iterator<Constant> constantsIterator() {
		return null;
	}

	@Override
	public void getStatic(String str, String desc, Frame cF) {

	}

	@Override
	public Object getStatic(String name) {
		return null;
	}

	@Override
	public AMethod getMethod(int i) {
		return null;
	}

	@Override
	public AMethod getMethod(String signature) {
		NativeMethod m = new NativeMethod(this, signature);
		switch (signature) {
			case "clone()Ljava/lang/Object;":
				m.setParamsArrayLength(1);
				break;
			case "getClass()Ljava/lang/Class;":
				m.setParamsArrayLength(1);
				break;
			case "getComponentType()Ljava/lang/Class;":
				m.setParamsArrayLength(1);
				break;
			case "newArray(Ljava/lang/Class;I)Ljava/lang/Object;":
				m.setParamsArrayLength(2);
				break;
			case "isArray()Z":
				m.setParamsArrayLength(1);
				break;
		}
		return m;
	}

	@Override
	public void getStatic(int i, Frame cF) {

	}

	@Override
	public void ldc(int id, Frame cF) {

	}

	@Override
	public void ldc2(int id, Frame cF) {

	}

	@Override
	public AClass getClass(int id) {
		return null;
	}

	@Override
	public AMethod getMyMethod(String nameAndDesc) {
		return null;
	}

	@Override
	public HashMap<String, Object> getStaticFields() {
		return null;
	}

	@Override
	public AClass getSuperClass() {
		return null;
	}


	@Override
	public int getAccessFlags() {
		return 0;
	}

	@Override
	public void getStaticReferecnes(Set<Integer> refs) {
		refs.add(myoref);
	}

	@Override
	public List getReferenceNames() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public AClass[] getInterfaces() {
		return new AClass[0];
	}

	@Override
	public String getSourceFileName() {
		return null;
	}

}
