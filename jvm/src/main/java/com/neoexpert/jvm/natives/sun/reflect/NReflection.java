package com.neoexpert.jvm.natives.sun.reflect;

import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm._abstract.AClass;

public class NReflection {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "getCallerClass()Ljava/lang/Class;":
				getCallerClass(cF, lv, t);
				return false;
			case "getClassAccessFlags(Ljava/lang/Class;)I":
				getClassAccessFlags(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void getClassAccessFlags(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		int access_flags = ((AClass) ci.cl).getAccessFlags();
		cF.push(access_flags);
	}

	private static void getCallerClass(Frame cF, Object[] lv, MyThread t) {
		int oref = t.getPrevFrame().getMethod().getMyClass().getMyInstanceRef();
		cF.push(oref);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}


}
