package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NDouble {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "doubleToRawLongBits(D)J":
				doubleToRawLongBits(cF, lv, t);
				return false;
			case "longBitsToDouble(J)D":
				longBitsToDouble(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static void longBitsToDouble(Frame cF, Object[] lv, MyThread t) {
		long l1 = (long) lv[1];// ((Integer) lv[1]).longValue() << 32 | ((Integer) (lv[0])).longValue() & 0xFFFFFFFFL;

		double d = Double.longBitsToDouble(l1);

		long result = Double.doubleToRawLongBits(d);
		//int i1 = (int) (result >> 32);
		//int i2 = (int) result;

		cF.push(result);
		cF.push(null);


	}

	private static void doubleToRawLongBits(Frame cF, Object[] lv, MyThread t) {
		long l1 = (long) lv[1];//((Integer) lv[1]).longValue() << 32 | ((Integer) (lv[0])).longValue() & 0xFFFFFFFFL;
		double d = Double.longBitsToDouble(l1);
		long result = Double.doubleToRawLongBits(d);
		//int i1 = (int) (result >> 32);
		//int i2 = (int) result;

		cF.push(result);
		cF.push(null);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
