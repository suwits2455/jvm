package com.neoexpert.jvm.natives.sun.misc;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;

public class NUnsafe {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "arrayBaseOffset(Ljava/lang/Class;)I":
				arrayBaseOffset(cF, lv, t);
				return false;
			case "arrayIndexScale(Ljava/lang/Class;)I":
				arrayIndexScale(cF, lv, t);
				return false;
			case "addressSize()I":
				addressSize(cF, lv, t);
				return false;
			case "objectFieldOffset(Ljava/lang/reflect/Field;)J":
				objectFieldOffset(cF, lv, t);
				return false;
			case "compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z":
				compareAndSwapObject(cF, lv, t);
				return false;
			case "getIntVolatile(Ljava/lang/Object;J)I":
				getIntVolatile(cF, lv, t);
				return false;
			case "compareAndSwapInt(Ljava/lang/Object;JII)Z":
				compareAndSwapInt(cF, lv, t);
				return false;
			case "allocateMemory(J)J":
				allocateMemory(cF, lv, t);
				return false;
			case "putLong(JJ)V":
				putLong(cF, lv, t);
				return false;
			case "getByte(J)B":
				getByte(cF, lv, t);
				return false;
			case "freeMemory(J)V":
				freeMemory(cF, lv, t);
				return false;
			case "ensureClassInitialized(Ljava/lang/Class;)V":
				ensureClassInitialized(cF, lv, t);
				return false;
			case "staticFieldOffset(Ljava/lang/reflect/Field;)J":
				staticFieldOffset(cF, lv, t);
				return false;
			case "staticFieldBase(Ljava/lang/reflect/Field;)Ljava/lang/Object;":
				staticFieldBase(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void staticFieldBase(Frame cF, Object[] lv, MyThread t) {
		cF.push(0);
	}

	private static void staticFieldOffset(Frame cF, Object[] lv, MyThread t) {
		cF.push(0L);
		cF.push(null);
	}
	private static void ensureClassInitialized(Frame cF, Object[] lv, MyThread t) {
		int oref= (int) lv[1];
		ClassInstance ci= (ClassInstance) Heap.get(oref);
		if(!((AClass)ci.cl).isInitialized())
			((AClass)ci.cl).clinit();
	}

	private static void freeMemory(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int l1 = (int) lv[1];
		int l2 = (int) lv[2];
		Heap.removeFromHeap(l1);
	}

	private static void getByte(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int l1 = (int) lv[1];
		int l2 = (int) lv[2];
		long l = (long) Heap.get(l1);
		byte b = (byte) l;
		cF.push((int) b);
	}

	private static void putLong(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int l1 = (int) lv[1];
		int l2 = (int) lv[2];
		//int _l1 = (int) lv[3];
		long _l2 = (long) lv[4];
		Heap.addToHeap(l1, _l2);
	}

	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}

	private static void allocateMemory(Frame cF, Object[] lv, MyThread t) {
		Object oref = lv[0];
		int addr = Heap.add(null);
		cF.push(addr);
		cF.push(0);
	}

	private static void compareAndSwapInt(Frame cF, Object[] lv, MyThread t) {
		/*int oref3=(int)cF.stack.pop();
		int oref2=(int)cF.stack.pop();
		int l2=(int)cF.stack.pop();
		int l1=(int)cF.stack.pop();
		int oref1=(int)cF.stack.pop();
		int oref0=(int)cF.stack.pop();*/

		int oref3 = (int) lv[5];
		int oref2 = (int) lv[4];
		long l2 = (long) lv[3];
		//int l1 = (int) lv[2];
		int oref1 = (int) lv[1];
		int oref0 = (int) lv[0];


		Object target = Heap.get(oref1);
		Object _this = Heap.get(oref0);
		//ClassInstance newdata=(ClassInstance)Heap.getFromHeap(oref3);

		//int referent=newdata.getField("referent");
		//newdata=(ClassInstance) Heap.getFromHeap(referent);
		Object olddata = Heap.get(oref2);
		//Heap.addToHeap(oref1, newdata);
		Object ci = Heap.get(oref0);
		cF.push(1);
	}

	private static void getIntVolatile(Frame cF, Object[] lv, MyThread t) {
		/*int oref1=(int)cF.stack.pop();
		int l1=(int)cF.stack.pop();
		int l0=(int)cF.stack.pop();
		int oref0=(int)cF.stack.pop();*/

		long oref1 = (long) lv[3];
		//int l1 = (int) lv[2];
		int l0 = (int) lv[1];
		int oref0 = (int) lv[0];

		Object ci = Heap.get((int) oref1);
		ci = Heap.get(l0);
		cF.push((int)oref1);
	}

	private static void compareAndSwapObject(Frame cF, Object[] lv, MyThread t) {
		/*int oref3=(int)cF.stack.pop();
		int oref2=(int)cF.stack.pop();
		int l2=(int)cF.stack.pop();
		int l1=(int)cF.stack.pop();
		int oref1=(int)cF.stack.pop();
		int oref0=(int)cF.stack.pop();*/

		int oref3 = (int) lv[5];
		int oref2 = (int) lv[4];
		long l2 = (long) lv[3];
		//int l1 = (int) lv[2];
		int oref1 = (int) lv[1];
		int oref0 = (int) lv[0];


		Object target = Heap.get(oref1);
		Object _this = Heap.get(oref0);
		ClassInstance newdata = (ClassInstance) Heap.get(oref3);

		int referent = (int) newdata.getField("referent");
		newdata = (ClassInstance) Heap.get(referent);
		Object olddata = Heap.get(oref2);
		//Heap.addToHeap(oref1, newdata);
		Object ci = Heap.get(oref0);
		cF.push(1);
	}

	private static void objectFieldOffset(Frame cF, Object[] lv, MyThread t) {
		//int oref1=(int)cF.stack.pop();
		//int oref2=(int)cF.stack.pop();
		int oref1 = (int) lv[0];
		int oref2 = (int) lv[1];
		Object ci1 = Heap.get(oref1);
		Object ci2 = Heap.get(oref2);
		cF.push((long)oref1);
		cF.push(null);
	}

	private static void addressSize(Frame cF, Object[] lv, MyThread t) {
		cF.push(4);
	}

	private static void arrayIndexScale(Frame cF, Object[] lv, MyThread t) {
		//Object oref=cF.getLv().get(0);
		cF.push(1);
	}

	private static void arrayBaseOffset(Frame cF, Object[] lv, MyThread t) {
		//Object oref=cF.getLv().get(0);
		cF.push(0);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
