package com.neoexpert.jvm.natives.java.util.concurent;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NAtomicLong {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "VMSupportsCS8()Z":
				VMSupportsCS8(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void VMSupportsCS8(Frame cF, Object[] lv, MyThread t) {
		cF.push(0);
	}


	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}


}
