package com.neoexpert.jvm.natives.java.io;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class NFileOutputStream {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "open(Ljava/lang/String;Z)V":
				open(cF, lv, t);
				return false;
			case "writeBytes([BIIZ)V":
				writeBytes(cF, lv, t);
				return false;
			case "close0()V":
				close0(cF, lv, t);
				return false;
			case "initIDs()V":
				initIDs(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void close0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		FileOutputStream fos = (FileOutputStream) ci.getField("native_fos");
		try {
			fos.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void writeBytes(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int aref = (int) lv[1];
		int off = (int) lv[2];
		int len = (int) lv[3];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		byte[] ba = (byte[]) ((ArrayInstance) Heap.get(aref)).a;
		FileOutputStream fos;
		Object nfos = ci.getField("native_fos");
		if (nfos instanceof FileOutputStream)
			fos = (FileOutputStream) nfos;
		else {
			ClassInstance fd = (ClassInstance) Heap.get((int) ci.getField("fd"));
			int _fd = (int) fd.getField("fd");
			fos = getFOS(fd);
		}
		try {
			fos.write(ba, off, len);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static FileOutputStream getFOS(ClassInstance fd) {
		// TODO: Implement this method
		return null;
	}

	private static void open(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int sref = (int) lv[1];
		int caref = (int) ((ClassInstance) Heap.get(sref))
				.getField("value");
		char[] ca = (char[]) ((ArrayInstance) Heap.get(caref)).a;

		ClassInstance ci = (ClassInstance) Heap.get(oref);
		try {
			boolean append = (int) ci.getField("append") != 0;
			FileOutputStream fos = new FileOutputStream(new File(new String(ca)), append);
			ci.putField("native_fos", fos);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private static void initIDs(Frame cF, Object[] lv, MyThread t) {
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
