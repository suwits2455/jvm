package com.neoexpert.jvm.natives.sun.misc;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

public class NSignal {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "findSignal(Ljava/lang/String;)I":
				findSignal(cF, lv, t);
				return false;
			case "handle0(IJ)J":
				handle0(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void handle0(Frame cF, Object[] lv, MyThread t) {
		int s = (int) lv[0];
		int l1 = (int) lv[1];
		int l2 = (int) lv[2];
		cF.push(0);
		cF.push(0);
	}

	private static void findSignal(Frame cF, Object[] lv, MyThread t) {
		ClassInstance ci = (ClassInstance) Heap.get((int) lv[0]);
		ArrayInstance ai = (ArrayInstance) Heap.get((int) ci.getField("value"));
		char[] a = (char[]) ai.a;
		/*for(int i=0;i<a.length;i++){
			a[i]=(char)ai.a[i];
		}*/
		String s = new String(a);
		switch (s) {
			case "HUP":
				cF.push(1);
				break;
			default:
				cF.push(0);
				break;
		}


	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
