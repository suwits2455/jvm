package com.neoexpert.jvm.natives.java.io;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NFileDescriptor {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, t);
				return false;
			case "initIDs()V":
				initIDs(cF, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void initIDs(Frame cF, MyThread t) {
	}

	private static void registerNatives(Frame cF, MyThread t) {
	}
}
