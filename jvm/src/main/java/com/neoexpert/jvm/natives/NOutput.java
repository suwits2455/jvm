package com.neoexpert.jvm.natives;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

import java.io.PrintStream;

public class NOutput {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "printNative(Ljava/lang/String;)V":
				printNative(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static PrintStream out;

	public static void setOut(PrintStream _out) {
		out = _out;
	}

	private static void printNative(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		oref = (int) ci.getField("value");
		ArrayInstance ai = (ArrayInstance) Heap.get(oref);

		char[] ca = (char[]) ai.a;
		out.print(ca);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
		// TODO: Implement this method
	}
}
