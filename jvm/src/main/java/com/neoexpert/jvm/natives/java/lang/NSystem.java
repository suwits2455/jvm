package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

public class NSystem {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {

			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "initProperties(Ljava/util/Properties;)Ljava/util/Properties;":
				initProperties(cF, lv, t);
				return false;
			case "arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V":
				arraycopy(cF, lv, t);
				return false;
			case "setIn0(Ljava/io/InputStream;)V":
				setIn0(cF, lv, t);
				return false;
			case "setOut0(Ljava/io/PrintStream;)V":
				setOut0(cF, lv, t);
				return false;
			case "setErr0(Ljava/io/PrintStream;)V":
				setErr0(cF, lv, t);
				return false;
			case "mapLibraryName(Ljava/lang/String;)Ljava/lang/String;":
				mapLibraryName(cF, lv, t);
				return false;
			case "nanoTime()J":
				nanoTime(cF, lv, t);
				return false;
			case "currentTimeMillis()J":
				currentTimeMillis(cF, lv, t);
				return false;
			case "identityHashCode(Ljava/lang/Object;)I":
				identityHashCode(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void identityHashCode(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		cF.push(ci.hashCode());
	}

	private static int l2i1(long l) {
		return (int) (l >> 32);
	}

	private static int l2i2(long l) {
		return (int) l;
	}
	private static void nanoTime(Frame cF, Object[] lv, MyThread t) {
		long nanotime = System.nanoTime();
		cF.push(l2i1(nanotime));
		cF.push(l2i2(nanotime));
	}

	private static void currentTimeMillis(Frame cF, Object[] lv, MyThread t) {
		long time = System.currentTimeMillis();
		cF.push(l2i1(time));
		cF.push(l2i2(time));
	}

	private static void mapLibraryName(Frame cF, Object[] lv, MyThread t) {
		cF.push(lv[0]);
	}


	public static int in;
	public static int out;
	public static int err;

	private static void setIn0(Frame cF, Object[] lv, MyThread t) {
		in = (int) lv[0];
		Class.getClass("java/lang/System").putStatic("in", in);
	}

	private static void setErr0(Frame cF, Object[] lv, MyThread t) {
		err = (int) lv[0];
		Class.getClass("java/lang/System").putStatic("err", err);
	}

	private static void setOut0(Frame cF, Object[] lv, MyThread t) {
		out = (int) lv[0];
		Class.getClass("java/lang/System").putStatic("out", out);
	}

	private static void arraycopy(Frame cF, Object[] lv, MyThread t) {
		int newlength = (int) lv[4];
		int i2 = (int) lv[3];
		int aref2 = (int) lv[2];
		ArrayInstance dest = (ArrayInstance) Heap.get(aref2);
		int i4 = (int) lv[1];
		int aref1 = (int) lv[0];

		ArrayInstance src = (ArrayInstance) Heap.get(aref1);
		System.arraycopy(src.a, i4, dest.a, i2, newlength);

	}

	private static void initProperties(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];


		setProp(oref, "file.encoding", "US_ASCII");
		setProp(oref, "sun.stdout.encoding", "US_ASCII");
		setProp(oref, "sun.stderr.encoding", "US_ASCII");
		setProp(oref, "file.separator", "/");
		setProp(oref, "path.separator", "/");
		setProp(oref, "sun.boot.library.path", "lib/");
		setProp(oref, "line.separator", "\n");
		setProp(oref, "sun.io.useCanonCaches", "false");
		setProp(oref, "sun.io.useCanonPrefixCache", "false");
		setProp(oref, "user.dir", System.getProperty("user.dir"));
		setProp(oref, "user.language", "en");
		setProp(oref, "user.region", "DE");
		setProp(oref, "user.variant", "");
		cF.push(oref);
	}


	private static void setProp(int oref, String key, String value) {
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		MyThread th = new MyThread(ci.getMyClass(),"setProp");
		int k = NString.createInstance(key);
		int v = NString.createInstance(value);
		th.setMethodToRun("setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;", oref, k, v);
		th.run();

	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}

}
