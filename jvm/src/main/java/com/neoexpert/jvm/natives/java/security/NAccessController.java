package com.neoexpert.jvm.natives.java.security;

import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.method.Method;

public class NAccessController {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "doPrivileged(Ljava/security/PrivilegedExceptionAction;)Ljava/lang/Object;":
				doPrivileged(cF, lv, t);
				return false;
			case "doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;":
				doPrivileged2(cF, lv, t);
				return false;
			case "getStackAccessControlContext()Ljava/security/AccessControlContext;":
				getStackAccessControlContext(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void getStackAccessControlContext(Frame cF, Object[] lv, MyThread t) {
		cF.push(0);
	}

	private static void doPrivileged(Frame cF, Object[] lv, MyThread t) {
		//cF.pc += 3;
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		Method m = (Method) ci.getMyClass().getMethod("run()Ljava/lang/reflect/Field;");
		if(m==null){
			System.out.println("method not found");
		}
		//AMethod m = ci.getMyClass().getMethod("run()");
		Frame f = new Frame(0, 1);
		f.push(oref);
		t.pushFrame(f, m);

	}

	private static void doPrivileged2(Frame cF, Object[] lv, MyThread t) {
		//cF.pc += 3;
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		Method m = (Method) ci.getMyClass().getMethod("run()Ljava/lang/Object;");
		//AMethod m = ci.getMyClass().getMethod("run()V");
		Frame f = new Frame(0, 1);
		f.push(oref);
		t.pushFrame(f, m);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}

}
