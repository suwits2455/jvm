package com.neoexpert.jvm.natives;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm.natives.java.io.NFileDescriptor;
import com.neoexpert.jvm.natives.java.io.NFileInputStream;
import com.neoexpert.jvm.natives.java.io.NFileOutputStream;
import com.neoexpert.jvm.natives.java.io.NUnixFileSystem;
import com.neoexpert.jvm.natives.java.lang.*;
import com.neoexpert.jvm.natives.java.lang.reflect.NArray;
import com.neoexpert.jvm.natives.java.security.NAccessController;
import com.neoexpert.jvm.natives.java.util.concurent.NAtomicLong;
import com.neoexpert.jvm.natives.java.util.zip.NZipFile;
import com.neoexpert.jvm.natives.sun.misc.NSignal;
import com.neoexpert.jvm.natives.sun.misc.NUnsafe;
import com.neoexpert.jvm.natives.sun.misc.NVM;
import com.neoexpert.jvm.natives.sun.reflect.NNativeConstructorAccessorImpl;
import com.neoexpert.jvm.natives.sun.reflect.NReflection;

public class Natives {

	public static boolean invoke(String classname, String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (classname) {
			case "java/lang/Class":
				return NClass.invoke(methodname, cF, lv, t);
			case "java/lang/reflect/Array":
				return NArray.invoke(methodname, cF, lv, t);
			case "NArray":
				return NArray.invoke(methodname, cF, lv, t);
			case "java/lang/ClassLoader":
				return NClassLoader.invoke(methodname, cF, lv, t);
			case "java/lang/System":
				return NSystem.invoke(methodname, cF, lv, t);
			case "java/lang/Object":
				return NObject.invoke(methodname, cF, lv, t);
			case "java/lang/Float":
				return NFloat.invoke(methodname, cF, lv, t);
			case "java/lang/Double":
				return NDouble.invoke(methodname, cF, lv, t);
			case "sun/misc/VM":
				return NVM.invoke(methodname, cF, lv, t);
			case "java/io/FileInputStream":
				return NFileInputStream.invoke(methodname, cF, lv, t);
			case "java/io/FileDescriptor":
				return NFileDescriptor.invoke(methodname, cF, lv, t);
			case "sun/misc/Unsafe":
				return NUnsafe.invoke(methodname, cF, lv, t);
			case "sun/reflect/Reflection":
				return NReflection.invoke(methodname, cF, lv, t);
			case "java/io/FileOutputStream":
				return NFileOutputStream.invoke(methodname, cF, lv, t);
			case "java/security/AccessController":
				return NAccessController.invoke(methodname, cF, lv, t);
			case "java/lang/String":
				return NString.invoke(methodname, cF, lv, t);
			case "java/lang/Thread":
				return NThread.invoke(methodname, cF, lv, t);
			case "java/lang/Throwable":
				return NThrowable.invoke(methodname, cF, lv, t);
			case "java/util/concurrent/atomic/AtomicLong":
				return NAtomicLong.invoke(methodname, cF, lv, t);
			case "java/io/UnixFileSystem":
				return NUnixFileSystem.invoke(methodname, cF, lv, t);
			case "sun/misc/Signal":
				return NSignal.invoke(methodname, cF, lv, t);
			case "sun/reflect/NativeConstructorAccessorImpl":
				return NNativeConstructorAccessorImpl.invoke(methodname, cF, lv, t);
			case "Output":
				return NOutput.invoke(methodname, cF, lv, t);
			case "java/util/zip/ZipFile":
				return NZipFile.invoke(methodname, cF, lv, t);
			case "NativeInputStream":
				return NativeInputStream.invoke(methodname, cF, lv, t);
			case "java/lang/Runtime":
				return NRuntime.invoke(methodname, cF, lv, t);
		}
		throw new RuntimeException("native method \"" + methodname + " \" was not found. reason: native class not found: " + classname);
	}
}
