package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;

public class NRuntime {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "gc()V":
				Heap.gc();
				return false;
			case "availableProcessors()I":
				availableProcessors(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static void availableProcessors(Frame cF, Object[] lv, MyThread t) {
		cF.push(1);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
