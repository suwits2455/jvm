package com.neoexpert.jvm.natives.java.io;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class NFileInputStream {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "open(Ljava/lang/String;)V":
				open(cF, lv, t);
				return false;
			case "close0()V":
				close0(cF, lv, t);
				return false;
			case "read()I":
				read(cF, lv, t);
				return false;
			case "readBytes([BII)I":
				readBytes(cF, lv, t);
				return false;
			case "available()I":
				available(cF, lv, t);
				return false;
			case "initIDs()V":
				initIDs(cF, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void close0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		FileInputStream fis = (FileInputStream) ci.getField("native_fis");
		try {
			fis.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	private static void readBytes(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int aref = (int) lv[1];
		int off = (int) lv[2];
		int len = (int) lv[3];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		byte[] ba = (byte[]) ((ArrayInstance) Heap.get(aref)).a;
		FileInputStream fis = (FileInputStream) ci.getField("native_fis");
		try {
			cF.push(fis.read(ba, off, len));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	private static void available(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		FileInputStream fis = (FileInputStream) ci.getField("native_fis");
		try {
			cF.push(fis.available());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	private static void read(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		FileInputStream fis = (FileInputStream) ci.getField("native_fis");
		try {
			cF.push(fis.read());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private static void open(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		int sref = (int) lv[1];
		int caref = (int) ((ClassInstance) Heap.get(sref))
				.getField("value");
		char[] ca = (char[]) ((ArrayInstance) Heap.get(caref)).a;

		ClassInstance ci = (ClassInstance) Heap.get(oref);
		try {
			FileInputStream fis = new FileInputStream(new String(ca));
			ci.putField("native_fis", fis);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}

	}

	private static void initIDs(Frame cF, MyThread t) {
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
