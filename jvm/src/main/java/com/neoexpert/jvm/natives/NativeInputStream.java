package com.neoexpert.jvm.natives;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class NativeInputStream extends AClass {
	private InputStream is;

	public NativeInputStream(InputStream inputStream) {
		this.is = inputStream;
	}

	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "read([BII)I":
				read(cF, lv, t);
				return false;
			case "available()I":
				avaliable(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void avaliable(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance nis = (ClassInstance) Heap.get(oref);
		NativeInputStream _is = (NativeInputStream) nis.getMyClass();
		try {
			int r = _is.is.available();
			cF.push(r);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	private static void read(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance nis = (ClassInstance) Heap.get(oref);
		int baoref = (int) lv[1];
		int i1 = (int) lv[2];
		int i2 = (int) lv[3];
		NativeInputStream _is = (NativeInputStream) nis.getMyClass();
		ArrayInstance ai = (ArrayInstance) Heap.get(baoref);
		byte[] ba = (byte[]) ai.a;
		try {
			int r = _is.is.read(ba, i1, i2);
			cF.push(r);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}


	@Override
	public void putStatic(String fname, Object v) {

	}

	@Override
	public void putStatic(int id, Object obj) {

	}

	@Override
	public boolean isInitialized() {
		return false;
	}

	@Override
	public void clinit() {

	}

	@Override
	public Constant getConstant(int i) {
		return null;
	}

	@Override
	public boolean isInstance(Object obj) {
		return false;
	}

	@Override
	public boolean isInstance(AClass cl) {
		return false;
	}

	@Override
	public String getName() {
		return "NativeInputStream";
	}

	@Override
	public Iterator<Constant> constantsIterator() {
		return null;
	}

	@Override
	public void getStatic(String str, String desc, Frame cF) {

	}

	@Override
	public Object getStatic(String name) {
		return null;
	}

	@Override
	public AMethod getMethod(int i) {
		return null;
	}

	@Override
	public AMethod getMethod(String nameAndDesc) {
		return new NativeMethod(this, nameAndDesc);
	}

	@Override
	public void getStatic(int i, Frame cF) {

	}

	@Override
	public void ldc(int id, Frame cF) {

	}

	@Override
	public void ldc2(int id, Frame cF) {

	}

	@Override
	public AClass getClass(int id) {
		return null;
	}

	@Override
	public AMethod getMyMethod(String nameAndDesc) {
		return null;
	}

	@Override
	public HashMap<String, Object> getStaticFields() {
		return null;
	}

	@Override
	public AClass getSuperClass() {
		return null;
	}

	@Override
	public int getAccessFlags() {
		return 0;
	}

	@Override
	public void getStaticReferecnes(Set<Integer> refs) {

	}

	@Override
	public List<String> getReferenceNames() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public AClass[] getInterfaces() {
		return new AClass[0];
	}

	@Override
	public String getSourceFileName() {
		return null;
	}


}
