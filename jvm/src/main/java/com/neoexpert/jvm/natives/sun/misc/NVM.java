package com.neoexpert.jvm.natives.sun.misc;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NVM {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF);
				return false;
			case "initialize()V":
				initialize(cF);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void initialize(Frame cF) {
	}

	private static void registerNatives(Frame cF) {
	}
}
