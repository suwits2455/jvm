package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.util.Iterator;

public class NThrowable {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "fillInStackTrace(I)Ljava/lang/Throwable;":
				fillInStackTrace(cF, lv, t);
				return false;
			case "getStackTraceDepth()I":
				getStackTraceDepth(cF, lv, t);
				return false;
			case "getStackTraceElement(I)Ljava/lang/StackTraceElement;":
				getStackTraceElement(cF, lv, t);
				return false;

		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void getStackTraceElement(Frame cF, Object[] lv, MyThread t) {
		int i= (int) lv[1];

		AClass stecl= Class.getClass("java/lang/StackTraceElement");
		Frame f = t.getStack().get(i);
		AMethod m = f.getMethod();
		String source_file=m.getMyClass().getSourceFileName();
		int sfsoref=0;
		if(source_file!=null)
			sfsoref= NString.createInstance(source_file);
		int steoref=new MyThread(stecl,"stack_trace_element_init").newInstance("<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V",
				NString.createInstance(m.getMyClass().getName()),
				NString.createInstance(m.getName()),
				sfsoref,
				f.getCurrentLine());
		cF.push(steoref);
	}

	private static void getStackTraceDepth(Frame cF, Object[] lv, MyThread t) {
		cF.push(t.getStackSize());
	}

	private static void fillInStackTrace(Frame cF, Object[] lv, MyThread t) {
		int i = (int) lv[1];
		int oref = (int) lv[0];
		Object ci = Heap.get(oref);
		cF.push(oref);
		FrameStack stack = t.getStack();
		ArrayInstance ai = new ArrayInstance(stack.size(), ArrayInstance.T_OBJECT);
		AClass stecl= Class.getClass("java/lang/StackTraceElement");
		Iterator<Frame> iter = stack.iterator();

		int _i=0;
		while (iter.hasNext()) {
			Frame f = iter.next();
			AMethod m = f.getMethod();
			String source_file=m.getMyClass().getSourceFileName();
			int sfsoref=0;
			if(source_file!=null)
				sfsoref= NString.createInstance(source_file);
			System.out.println(f);
			int steoref=new MyThread(stecl,"stack_trace_element_init2").newInstance("<init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V",
					NString.createInstance(m.getMyClass().getName()),
					NString.createInstance(m.getName()),
					sfsoref,
					f.getCurrentLine());
			((int[])ai.a)[_i]=steoref;
			_i++;
		}
	}


	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}


}
