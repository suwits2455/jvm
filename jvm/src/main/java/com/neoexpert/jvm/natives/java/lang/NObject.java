package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.instance.Instance;

public class NObject {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "hashCode()I":
				hashCode(cF, lv, t);
				return false;
			case "getClass()Ljava/lang/Class;":
				getClass(cF, lv, t);
				return false;
			case "notifyAll()V":
				notifyAll(cF, lv, t);
				return false;
			case "wait(J)V":
				wait(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}
	
	private static void wait(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		long d=_2i2l((int)lv[1],(int)lv[2]);
	}
	private static void notifyAll(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
	}

	private static void getClass(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		Instance ci = (Instance) Heap.get(oref);
		AClass cl = ci.getMyClass();
		if (cl == null) {
			if (ci instanceof ArrayInstance)
				cF.push(((ArrayInstance) ci).mc);
		} else
			cF.push(cl.getMyInstanceRef());
	}

	private static void hashCode(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		cF.push(ci.hashCode());
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}


}
