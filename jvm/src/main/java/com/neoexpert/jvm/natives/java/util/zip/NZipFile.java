package com.neoexpert.jvm.natives.java.util.zip;

import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm.natives.NativeInputStream;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class NZipFile {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, t);
				return false;
			case "initIDs()V":
				initIDs(cF, lv, t);
				return false;
			case "open(Ljava/lang/String;IJZ)J":
				open(cF, lv, t);
				return false;
			case "getTotal(J)I":
				getTotal(cF, lv, t);
				return false;
			case "getEntry(JLjava/lang/String;Z)J":
				getEntry(cF, lv, t);
				return false;
			case "getEntryTime(J)J":
				getEntryTime(cF, lv, t);
				return false;
			case "getEntrySize(J)J":
				getEntrySize(cF, lv, t);
				return false;
			case "getEntryCSize(J)J":
				getEntryCSize(cF, lv, t);
				return false;
			case "getEntryMethod(J)I":
				getEntryMethod(cF, lv, t);
				return false;
			case "getEntryBytes(JI)[B":
				getEntryBytes(cF, lv, t);
				return false;
			case "freeEntry(JJ)V":
				freeEntry(cF, lv, t);
				return false;
			case "getInputStream(JJ)Ljava/io/InputStream;":
				getInputStream(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void getInputStream(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int) (long)lv[4];
		int faddr = (int) (long)lv[2];
		ZipFile f = files.get(faddr);
		ZipEntry e = entries.get(entryaddr);
		try {
			NativeInputStream nis = new NativeInputStream(f.getInputStream(e));
			ClassInstance ci = new ClassInstance(nis);
			int oref = Heap.add(ci);
			cF.push(oref);
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	private static void freeEntry(Frame cF, Object[] lv, MyThread t) {
		//int l0 = (int) lv[0];
		int l1 = (int)(long) lv[1];
		//int l2 = (int) lv[2];
		//entries.remove(l2);
		int l3 = (int) (long)lv[3];
	}

	private static void getEntryBytes(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int)(long) lv[1];
		ZipEntry e = entries.get(entryaddr);
		byte[] b = e.getExtra();
		//TODO implement this
		cF.push(0);
	}

	private static void getEntryMethod(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int) (long)lv[1];
		ZipEntry e = entries.get(entryaddr);
		cF.push(e.getMethod());
	}

	private static void getEntrySize(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int)(long) lv[1];
		ZipEntry e = entries.get(entryaddr);
		long size = e.getSize();
		cF.push((long)size);
		cF.push(null);
	}

	private static void getEntryCSize(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int) (long)lv[1];
		ZipEntry e = entries.get(entryaddr);
		long size = e.getCompressedSize();
		cF.push(size);
		cF.push(null);
	}

	private static int l2i1(long l) {
		return (int) (l >> 32);
	}

	private static int l2i2(long l) {
		return (int) l;
	}

	private static void getEntryTime(Frame cF, Object[] lv, MyThread t) {
		int entryaddr = (int) (long)lv[1];
		ZipEntry e = entries.get(entryaddr);
		long time = e.getTime();
		cF.push(time);
		cF.push(null);
	}

	private static void getEntry(Frame cF, Object[] lv, MyThread t) {
		int addr = (int)(long) lv[1];
		int pathoref = (int) lv[2];
		String path = NString.toNative(pathoref);
		ZipEntry e = files.get(addr).getEntry(path);
		if (e == null) {
			cF.push(0L);
			cF.push(null);
		} else {
			cF.push((long)eaddr);
			cF.push(null);
			entries.put(eaddr, e);
			eaddr++;
		}
	}

	private static void getTotal(Frame cF, Object[] lv, MyThread t) {
		//int l0 = (int) lv[0];
		int l1 = (int) (long)lv[1];
		ZipFile f = files.get(l1);
		cF.push(f.size());
	}

	private static int addr = 1;
	private static int eaddr = 1;
	private static Map<Integer, ZipFile> files = new HashMap<>();
	private static Map<Integer, ZipEntry> entries = new HashMap<>();

	private static void open(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		String path = NString.toNative(oref);
		try {
			ZipFile f = new ZipFile(path);
			files.put(addr, f);
			cF.push((long)addr);
			cF.push(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		addr++;

	}

	private static void initIDs(Frame cF, Object[] lv, MyThread t) {
	}

	private static void registerNatives(Frame cF, MyThread t) {
	}

}
