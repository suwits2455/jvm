package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AField;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.natives.NativeMethod;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;

public class NClass {
	Map<String, NativeMethod> methods=new HashMap<>();
	public NClass(){
		AClass cl = Class.getClass("java/lang/Class");
		NativeMethod nm;
		nm = new NativeMethod(cl, "getPrimitiveClass(Ljava/lang/String;)Ljava/lang/Class;") {
			@Override
			public void invoke(Frame cF, Object[] lv, MyThread t) {
				ClassInstance ci = (ClassInstance) Heap.get((int) lv[0]);
				String ptype = NString.toNative((Integer) lv[0]);
				int oref;
				switch (ptype) {
					case "int":
						oref = Class.getPrimitiveClass("I");
						break;
					case "float":
						oref = Class.getPrimitiveClass("F");
						break;
					case "double":
						oref = Class.getPrimitiveClass("D");
						break;
					case "long":
						oref = Class.getPrimitiveClass("J");
						break;
					case "byte":
						oref = Class.getPrimitiveClass("B");
						break;
					case "char":
						oref = Class.getPrimitiveClass("C");
						break;
					case "short":
						oref = Class.getPrimitiveClass("S");
						break;
					case "boolean":
						oref = Class.getPrimitiveClass("Z");
						break;
					default:
						throw new RuntimeException(ptype + " is not known primitive type");
				}
				cF.push(oref);
			}
		};
		methods.put(nm.getName(),nm);
	}

	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "getPrimitiveClass(Ljava/lang/String;)Ljava/lang/Class;":
				getPrimitiveClass(cF, lv, t);
				return false;
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "desiredAssertionStatus0(Ljava/lang/Class;)Z":
				desiredAssertionStatus0(cF, lv, t);
				return false;
			case "getClassLoader0()Ljava/lang/ClassLoader;":
				getClassLoader0(cF, lv, t);
				return false;
			case "getName0()Ljava/lang/String;":
				getName0(cF, lv, t);
				return false;
			case "forName0(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;":
				forName0(cF, lv, t);
				return false;
			case "getDeclaredFields0(Z)[Ljava/lang/reflect/Field;":
				getDeclaredFields0(cF, lv, t);
				return false;
			case "isPrimitive()Z":
				isPrimitive(cF, lv, t);
				return false;
			case "isInterface()Z":
				isInterface(cF, lv, t);
				return false;
			case "getComponentType()Ljava/lang/Class;":
				getComponentType(cF, lv, t);
				return false;
			case "isAssignableFrom(Ljava/lang/Class;)Z":
				isAssignableFrom(cF, lv, t);
				return false;
			case "getProtectionDomain0()Ljava/security/ProtectionDomain;":
				getProtectionDomain0(cF, lv, t);
				return false;
			case "getDeclaredConstructors0(Z)[Ljava/lang/reflect/Constructor;":
				getDeclaredConstructors0(cF, lv, t);
				return false;
			case "getModifiers()I":
				getModifiers(cF, lv, t);
				return false;
			case "getSuperclass()Ljava/lang/Class;":
				getSuperClass(cF, lv, t);
				return false;
			case "isArray()Z":
				isArray(cF, lv, t);
				return false;
			case "getInterfaces0()[Ljava/lang/Class;":
				getInterfaces0(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}


	private static void getInterfaces0(Frame cF, Object[] lv, MyThread t) {
		int oref= (int) lv[0];
		ClassInstance ci= (ClassInstance) Heap.get(oref);
		Class[] interfaces = (Class[]) ((AClass) ci.cl).getInterfaces();
		if(interfaces.length>0)
			throw new RuntimeException("Implement me");
		ArrayInstance ai=new ArrayInstance(interfaces.length,ArrayInstance.T_OBJECT);
		int aref = Heap.add(ai);
		cF.push(aref);
	}

	private static void isArray(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		//if(ci.cl instanceof Class)
		cF.push(0);
		//else
		//cF.push(1);
	}

	private static void getSuperClass(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		AClass super_class = ((AClass) ci.cl).getSuperClass();
		if (super_class == null)
			cF.push(0);
		else cF.push(super_class.getMyInstanceRef());
	}

	private static void getModifiers(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		int access_flags = ((AClass) ci.cl).getAccessFlags();
		cF.push(access_flags);
	}

	private static void getProtectionDomain0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		String url = ((Class) ci.cl).getCodeSource().toString();
		try {
			URL u = new URL(url);
			u.toURI();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		int urlci = NString.createInstance(url);
		AClass c = Class.getClass("java/net/URL");
		int urloref = new MyThread(c,"get_protection_domain URL").newInstance("<init>(Ljava/lang/String;)V", urlci);
		c = Class.getClass("java/security/CodeSource");
		int codesourceoref = new MyThread(c,"getProtectionDomain CodeSource")
				.newInstance("<init>(Ljava/net/URL;[Ljava/security/cert/Certificate;)V", urloref, 0);
		c = Class.getClass("java/security/ProtectionDomain");
		int pdomainoref = new MyThread(c,"ProtectionDomain")
				.newInstance("<init>(Ljava/security/CodeSource;Ljava/security/PermissionCollection;)V", codesourceoref, 0);

		cF.push(pdomainoref);

	}

	private static void isAssignableFrom(Frame cF, Object[] lv, MyThread t) {
		cF.push(1);
	}

	private static void getComponentType(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance o = (ClassInstance) Heap.get(oref);
		String s = (String) o.getField("name");
		int coref = 0;
		if (s.charAt(0) == '[') {
			s = s.substring(1);

			if (s.charAt(0) == 'L') {
				s = s.substring(1, s.indexOf(';'));
				coref = Class.getClass(s).getMyInstanceRef();
			} else
				coref = Class.getPrimitiveClass(s);
		}
		cF.push(coref);
	}

	private static void isInterface(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) cF.lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		if (ci.cl instanceof AClass) {
			if (((AClass) ci.cl).isInterface()) {
				cF.push(1);
			} else
				cF.push(0);
		} else
			cF.push(0);
	}

	private static void isPrimitive(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		if (ci.cl instanceof String) {
			if (((String) ci.cl).length() == 1)
				cF.push(1);
			return;
		}
		cF.push(0);
	}

	/*
	 Field(Class<?> declaringClass,
	 String name,
	 Class<?> type,
	 int modifiers,
	 int slot,
	 String signature,
	 byte[] annotations)
	*/
	private static void getDeclaredFields0(Frame cF, Object[] lv, MyThread t) {
		//int b=(int)cF.stack.pop();
		//int oref=(int)cF.stack.pop();
		int oref = (int) cF.lv[0];
		int b = (int) cF.lv[1];


		ClassInstance ci = (ClassInstance) Heap.get(oref);
		Class cl = (Class) ci.cl;
		Collection<AField> fields = cl.fields.values();
		ArrayInstance ai = new ArrayInstance(fields.size(), ArrayInstance.T_OBJECT);
		int i = 0;
		for (AField f : fields) {
			AClass c = Class.getClass("java/lang/reflect/Field");
			int si = NString.createInstance(f.getName());

			int mc = f.getMyClass().getMyInstanceRef();
			int mt = f.getType();

			oref = new MyThread(c,"getDeclaredFields0").newInstance("<init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Class;IILjava/lang/String;[B)V", mc, si, mt, f.getModifiers(), 43, 0, 0);
			((int[]) ai.a)[i] = oref;
			i++;
		}
		int aref = Heap.add(ai);

		cF.push(aref);
	}

	private static void getDeclaredConstructors0(Frame cF, Object[] lv, MyThread t) {
		//int b=(int)cF.stack.pop();
		//int oref=(int)cF.stack.pop();
		int oref = (int) cF.lv[0];
		int b = (int) cF.lv[1];


		ClassInstance ci = (ClassInstance) Heap.get(oref);
		Class cl = (Class) ci.cl;
		ArrayInstance ai = new ArrayInstance(cl.constructors.size(), ArrayInstance.T_OBJECT);
		int i = 0;
		for (AMethod f : cl.constructors) {
			AClass c = Class.getClass("java/lang/reflect/Constructor");

			int mc = f.getMyClass().getMyInstanceRef();
			int si = NString.createInstance(f.getSignature());
			ArrayList<Character> args = f.args;
			ArrayInstance parameterTypes = new ArrayInstance(f.getParameterCount(), ArrayInstance.T_OBJECT);
			int parameterTypesoref = Heap.add(parameterTypes);
			int k = 0;
			for (int j = 0; j < args.size(); j++) {
				char param = args.get(j);
				AClass paramclass = Class.getClass(param + "");
				((int[]) parameterTypes.a)[k] = paramclass.getMyInstanceRef();
				k++;


				if (param == 'L' || param == 'D')
					j++;
			}
		
/*
    Constructor(Class<T> declaringClass,
                Class<?>[] parameterTypes,
                Class<?>[] checkedExceptions,
                int modifiers,
                int slot,
                String signature,
                byte[] annotations,
                byte[] parameterAnnotations) */
			oref = new MyThread(c,"getDeclaredConstructors0")
					.newInstance("<init>(Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Class;IILjava/lang/String;[B[B)V", mc, parameterTypesoref, -44, f.getModifiers(), 43, si, 0, 0);
			((int[]) ai.a)[i] = oref;
			i++;
		}
		int aref = Heap.add(ai);

		cF.push(aref);
	}


	private static void forName0(Frame cF, Object[] lv, MyThread t) {
		/*int oref1=(int)cF.stack.pop();
		int oref2=(int)cF.stack.pop();
		int oref3=(int)cF.stack.pop();
		int oref4=(int)cF.stack.pop();*/

		int oref2 = (int) lv[2];
		int oref3 = (int) lv[1];
		int oref4 = (int) lv[0];

		ClassInstance ci = (ClassInstance) Heap.get(oref4);
		int v = (int) ci.getField("value");
		ArrayInstance ai = (ArrayInstance) Heap.get(v);
		char[] ca = (char[]) ai.a;
//		for(int i=0;i<ca.length;i++){
//			ca[i]=(char)ai.a[i];
//		}
		String classname = new String(ca);
		classname = classname.replaceAll("\\.", "/");
		AClass c = Class.getClass(classname);
		cF.push(c.getMyInstanceRef());
	}

	private static void getName0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		if (ci.cl instanceof Class)
			oref = NString.createInstance(((Class) ci.cl).getName());
		else {
			oref = NString.createInstance((String) ci.cl);
		}
		cF.push(oref);
	}


	private static void desiredAssertionStatus0(Frame cF, Object[] lv, MyThread t) {
		Object i = Heap.get((int) lv[0]);
		cF.push(0);
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}

	public static int classloaderref = 0;

	private static void getClassLoader0(Frame cF, Object[] lv, MyThread t) {
        /*Class javaClass = (Class) Class.getClass("java/lang/ClassLoader");
        ClassLoader cl=new ClassLoader(javaClass);
        ClassInstance ic = new ClassInstance(cl);
        */
		cF.push(classloaderref);
	}

	private static void getPrimitiveClass(Frame cF, Object[] lv, MyThread t) {
		ClassInstance ci = (ClassInstance) Heap.get((int) lv[0]);
		String ptype = NString.toNative((Integer) lv[0]);
		int oref;
		switch (ptype) {
			case "int":
				oref = Class.getPrimitiveClass("I");
				break;
			case "float":
				oref = Class.getPrimitiveClass("F");
				break;
			case "double":
				oref = Class.getPrimitiveClass("D");
				break;
			case "long":
				oref = Class.getPrimitiveClass("J");
				break;
			case "byte":
				oref = Class.getPrimitiveClass("B");
				break;
			case "char":
				oref = Class.getPrimitiveClass("C");
				break;
			case "short":
				oref = Class.getPrimitiveClass("S");
				break;
			case "boolean":
				oref = Class.getPrimitiveClass("Z");
				break;
			default:
				throw new RuntimeException(ptype + " is not known primitive type");
		}
		cF.push(oref);
	}

	public static class ClassLoader extends AClass {

		@Override
		public Object getStatic(String name) {
			// TODO: Implement this method
			return null;
		}


		@Override
		public void putStatic(String fname, Object v) {
			// TODO: Implement this method
		}


		@Override
		public void putStatic(int id, Object obj) {
			// TODO: Implement this method
		}


		@Override
		public boolean isInitialized() {
			// TODO: Implement this method
			return false;
		}


		@Override
		public void clinit() {
			// TODO: Implement this method
		}

		private final Class java_lang_class;

		public ClassLoader(Class java_lang_class) {
			this.java_lang_class = java_lang_class;
		}

		@Override
		public Constant getConstant(int i) {
			return java_lang_class.getConstant(i);
		}

		@Override
		public boolean isInstance(Object obj) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public boolean isInstance(AClass obj) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public String getName() {
			throw new RuntimeException("isInstance");
		}

		@Override
		public Iterator<Constant> constantsIterator() {
			throw new RuntimeException("isInstance");
		}

		@Override
		public void getStatic(String str, String p1, Frame cF) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public AMethod getMethod(int i) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public AMethod getMethod(String nameAndDesc) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public void getStatic(int i, Frame t) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public void ldc(int id, Frame cF) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public void ldc2(int id, Frame cF) {

		}


		@Override
		public AClass getClass(int id) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public AMethod getMyMethod(String nameAndDesc) {
			throw new RuntimeException("isInstance");
		}

		@Override
		public HashMap<String, Object> getStaticFields() {
			throw new RuntimeException("isInstance");
		}

		@Override
		public AClass getSuperClass() {
			throw new RuntimeException("isInstance");
		}


		@Override
		public int getAccessFlags() {
			return 0;
		}

		@Override
		public void getStaticReferecnes(Set<Integer> refs) {

		}

		@Override
		public List getReferenceNames() {
			return Collections.EMPTY_LIST;
		}

		@Override
		public AClass[] getInterfaces() {
			return new AClass[0];
		}

		@Override
		public String getSourceFileName() {
			return null;
		}


	}
}
