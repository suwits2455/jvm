package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NFloat {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "floatToRawIntBits(F)I":
				floatToRawIntBits(cF, lv, t);
				return false;
			case "intBitsToFloat(I)F":
				intBitsToFloat(cF, lv, t);
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}

	private static void intBitsToFloat(Frame cF, Object[] lv, MyThread t) {
		int i = (int) lv[0];
		cF.push(Float.intBitsToFloat(i));
	}

	private static void floatToRawIntBits(Frame cF, Object[] lv, MyThread t) {
		float f = (float) lv[0];
		cF.push(Float.floatToRawIntBits(f));
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {
	}
}
