package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;

import java.util.HashMap;

public class NString {

	public static int createInstance(String str) {
		char[] ca = str.toCharArray();
		ArrayInstance ai = new ArrayInstance(ca, ArrayInstance.T_CHAR);
		int aref = Heap.add(ai);
		AClass strcl = Class.getClass("java/lang/String");
		ClassInstance ci = new ClassInstance(strcl);
		int oref = Heap.add(ci);
		MyThread t = new MyThread(strcl,"NString: createInstance");
		t.setMethodToRun("<init>([C)V");
		t.getCurrentFrame().lv[0] = oref;
		t.getCurrentFrame().lv[1] = aref;
		t.getCurrentFrame().lvpt[0] = oref;
		t.getCurrentFrame().lvpt[1] = aref;

		t.run();
		Frame f = new Frame(1, 1);

		f.lv[0] = oref;
		intern(f, f.lv, null);
		oref = (int) f.pop();
		return oref;
	}

	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "intern()Ljava/lang/String;":
				intern(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static HashMap<Integer, Integer> strings = new HashMap<>();

	private static void intern(Frame cF, Object[] lv, MyThread t) {
		int saddr = (int) lv[0];
		ClassInstance ci = (ClassInstance) Heap.get(saddr);
		int f = (int) ci.getField("value");
		ArrayInstance ai = (ArrayInstance) Heap.get(f);
		char[] a = (char[]) ai.a;
//		for(int i=0;i<a.length;i++){
//			a[i]=(char)ai.a[i];
//		}
		String s = new String(a);
		Integer addr = strings.get(s.hashCode());
		if (addr == null) {
			strings.put(s.hashCode(), saddr);
			cF.push(saddr);
			Heap.makePermanent(saddr);
		} else
			cF.push(addr);
	}


	private static void registerNatives(Frame cF) {
	}

	public static String toNative(int oref) {
		ClassInstance ci = (ClassInstance) Heap.get(oref);
		ArrayInstance ai = (ArrayInstance) Heap.get((int) ci.getField("value"));
		char[] ca = (char[]) ai.a;
		return new String(ca);
	}
}
