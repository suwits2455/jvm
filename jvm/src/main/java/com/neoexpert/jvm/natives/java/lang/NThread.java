package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.*;

public class NThread {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, t);
				return false;
			case "currentThread()Ljava/lang/Thread;":
				currentThread(cF, lv, t);
				return false;
			case "setPriority0(I)V":
				setPriority0(cF, lv, t);
				return false;
			case "isAlive()Z":
				isAlive(cF, lv, t);
				return false;
			case "start0()V":
				start0(cF, lv, t);
				return false;
			case "sleep(J)V":
				return sleep(cF, lv, t);
		}
		throw new RuntimeException("native method " + methodname + " was not found");
	}
	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}

	private static boolean sleep(Frame cF, Object[] lv, MyThread t) {
		long d=_2i2l((int)lv[0],(int)lv[1]);
		t.sleep(d);
		return true;
	}

	private static void start0(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		ClassInstance ci=(ClassInstance)Heap.get(oref);
		AClass cl=ci.getMyClass();
		MyThread th=new MyThread(cl,"start0");
		th.setPriority((int)ci.getField("priority"));
		th.setDaemon((int)ci.getField("daemon")==1);
		th.setMethodToRun("run()V");
		th.getCurrentFrame().lv[0]=oref;
		JVM.addThread(th);
	}

	private static void isAlive(Frame cF, Object[] lv, MyThread t) {
		int oref = (int) lv[0];
		cF.push(0);
	}

	private static void setPriority0(Frame cF, Object[] lv, MyThread t) {
		int p = (int) lv[1];
		int oref = (int) lv[0];

		ClassInstance ci = (ClassInstance) Heap.get(oref);

		ci.putField("priority", p);
	}

	private static void currentThread(Frame cF, Object[] lv, MyThread t) {
		cF.push(t.this_thread);
	}

	private static void registerNatives(Frame cF, MyThread t) {
	}
}
