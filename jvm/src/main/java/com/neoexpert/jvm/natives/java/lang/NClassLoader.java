package com.neoexpert.jvm.natives.java.lang;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.MyThread;

public class NClassLoader {
	public static boolean invoke(String methodname, Frame cF, Object[] lv, MyThread t) {
		switch (methodname) {
			case "registerNatives()V":
				registerNatives(cF, lv, t);
				return false;
			case "findBuiltinLib(Ljava/lang/String;)Ljava/lang/String;":
				findBuiltinLib(cF, lv, t);
				return false;
		}
		throw new RuntimeException("method " + methodname + " was not found");
	}

	private static void findBuiltinLib(Frame cF, Object[] lv, MyThread t) {
		// TODO: Implement this method
	}

	private static void registerNatives(Frame cF, Object[] lv, MyThread t) {

	}

}
