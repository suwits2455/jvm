package com.neoexpert.jvm.natives;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.method.MException;
import com.neoexpert.jvm.MyThread;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Stack;

public class NativeMethod extends AMethod {

	private int param_length;

	public NativeMethod(AClass cl, String name) {
		super(cl);
		this.name = name;
	}

	public int getModifiers() {
		return 0;
	}

	@Override
	public boolean isStatic() {
		// TODO: Implement this method
		return false;
	}

	@Override
	public int getParamsArrayLength() {
		return param_length;
	}

	@Override
	public String getSignature() {
		return name;
	}

	@Override
	public boolean isAbstract() {
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean isNative() {
		return true;
	}

	@Override
	public int[] getParams() {
		// TODO: Implement this method
		return null;
	}

	@Override
	public void setNative(boolean b) {

	}

	@Override
	public int getParameterCount() {
		return 0;
	}


	public void invoke(Frame cF,Object[] lv, MyThread t) {
	}

	public void setParamsArrayLength(int param_length) {
		this.param_length=param_length;
	}
}
