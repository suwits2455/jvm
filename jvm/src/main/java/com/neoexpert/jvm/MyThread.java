package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.method.Method;
import com.neoexpert.jvm.natives.java.lang.NString;
import com.neoexpert.jvm.executor.*;

import java.util.Iterator;
import java.util.Set;

public class MyThread {

	private final String name;
	public int this_thread;


	public int getStackSize() {
		return cE.stack.size();
	}


	public String getStackTrace() {
		Iterator<Frame> it = cE.stack.iterator();
		StringBuilder sb = new StringBuilder();
		while (it.hasNext()) {
			sb.append(it.next());
			sb.append("\n");
		}
		return sb.toString();
	}

	public AClass getCurrentClass() {
		return cE.cC;
	}

	public Frame getPrevFrame() {
		Frame f = cE.stack.pop();
		Frame pf = cE.stack.peek();
		cE.stack.push(f);
		return pf;
	}


	public int newInstance(String method, int... args) {

		Method m = (Method) cE.cC.getMyMethod(method);
		if (m == null) throw new RuntimeException("constructor " + method + " not found");
		Frame cF = new Frame(m);
		switch (cF.code_type) {
			case Method.BYTE_CODE:
				cE = rbce.cloneme(cE);
				break;
			case Method.OPTIMIZED_BYTE_CODE:
				cE = obce.cloneme(cE);
				break;
			case Method.OPTIMIZED_SHORT_CODE:
				cE = osce.cloneme(cE);
				break;
		}

		cE.stack.push(cF);
		ClassInstance ci = new ClassInstance(cE.cC);
		int oref = Heap.add(ci);

		cF.lv[0] = oref;
		cF.lvpt[0]=oref;
		for (int i = 0; i < args.length; i++) {
			cF.lv[i + 1] = args[i];
			if(m.args.get(i)=='L')
				cF.lvpt[i]=args[i];
		}
		try {
			run();
		} catch (Exception e) {
			//t.getCurrentFrame();
			throw new RuntimeException(getStackTrace(), e);
		}
		return (int) cE.last_frame.lv[0];
	}

	public int newInstance2(AClass c, String method, int... args) {
		Method m = (Method) cE.cC.getMyMethod(method);
		if (m == null) throw new RuntimeException("constructor " + method + " not found");
		Frame cF = new Frame(m);
		switch (cF.code_type) {
			case Method.BYTE_CODE:
				cE = rbce.cloneme(cE);
				break;
			case Method.OPTIMIZED_BYTE_CODE:
				cE = obce.cloneme(cE);
				break;
			case Method.OPTIMIZED_SHORT_CODE:
				cE = osce.cloneme(cE);
				break;
		}
		cE.stack.push(cF);
		ClassInstance ci = new ClassInstance(c);
		int oref = Heap.add(ci);

		cF.lv[0] = oref;
		cF.lvpt[0]=oref;
		for (int i = 0; i < args.length; i++) {
			cF.lv[i + 1] = args[i];
			if(m.args.get(i)=='L')
				cF.lvpt[i + 1] = args[i];
		}
		//run();
		return oref;
	}

	public MyThread setMethodToRun(String method, int... args) {
		Method m = (Method) cE.cC.getMyMethod(method);
		if (m == null)
			return this;
		Frame cF = new Frame(m);
		for (int i = 0; i < args.length; i++) {
			cF.lv[i] = args[i];
		}
		switch (cF.code_type) {
			case Method.BYTE_CODE:
				cE = rbce.cloneme(cE);
				break;
			case Method.OPTIMIZED_BYTE_CODE:
				cE = obce.cloneme(cE);
				break;
			case Method.OPTIMIZED_SHORT_CODE:
				cE = osce.cloneme(cE);
				break;
		}
		cE.stack.push(cF);
		return this;
	}

	public MyThread setStaticMethodToRun(String method, int... args) {
		Method m = (Method) cE.cC.getMyMethod(method);
		if (m == null)
			return this;
		Frame cF = new Frame(m);
		for (int i = 0; i < args.length; i++) {
			cF.lv[i] = args[i];
			char t = m.args.get(i);
			if(t=='L'||t=='[')
				cF.lvpt[i]=args[i];
		}
		switch (cF.code_type) {
			case Method.BYTE_CODE:
				cE = rbce.cloneme(cE);
				break;
			case Method.OPTIMIZED_BYTE_CODE:
				cE = obce.cloneme(cE);
				break;
			case Method.OPTIMIZED_SHORT_CODE:
				cE = osce.cloneme(cE);
				break;
		}
		cE.stack.push(cF);
		return this;
	}

	public int getCurrentLine() {
		if (cE.stack.isEmpty())
			return -1;
		return cE.stack.peek().getCurrentLine();
	}

	private long sleep_until;
	public void sleep(long d){
		sleep_until=System.currentTimeMillis()+d;
	}

	public MyThread(AClass c,String name) {
		this.name=name;

		cE.cC = c;
		Heap.addThread(this);

		// AClass tc=com.neoexpert.jvm._class.Class.getClass("java/lang/Thread");

		//ClassInstance ic=new ClassInstance(tc);
		//this_thread=Heap.addToHeap(ic);
	}

	public MyThread(AClass c, String name, boolean createthread) {
		this.name=name;

		cE.cC = c;
		if(createthread)
		if (name != null && JVM.system_thread_group != 0)
			createThread(name);

		Heap.addThread(this);
		// AClass tc=com.neoexpert.jvm._class.Class.getClass("java/lang/Thread");

		//ClassInstance ic=new ClassInstance(tc);
		//this_thread=Heap.addToHeap(ic);
	}

	private boolean daemon=false;
	private int priority=5;
	private int psteps=128;

	public void setPriority(int priority){
		this.priority=priority;
		psteps=priority*64;
	}

	public void setDaemon(boolean on){
		this.daemon=on;
	}

	public boolean isDaemon(){
		return daemon;
	}

	public void _wait(){
		waiting=true;
	}

	private boolean waiting=false;
	public boolean isWaiting(){
		return waiting;
	}

	public void createThread(String name) {
		int tname = NString.createInstance(name);
		AClass thc = Class.getClass("java/lang/Thread");
		MyThread t = new MyThread(thc, "thread_creator" );

		this_thread = t.newInstance2(thc, "<init>(Ljava/lang/ThreadGroup;Ljava/lang/String;)V", JVM.system_thread_group, tname);
		((ClassInstance) Heap.get(this_thread)).putField("priority", 5);
		t.this_thread = this_thread;
		Heap.makePermanent(this_thread);
		t.run();
	}

	public void pushFrame(Frame prevFrame, Method m) {
		Frame f = prevFrame.newRefFrame(m);
		cE.stack.push(f);
		cE.cC = m.getMyClass();
		switch (f.code_type) {
			case Method.BYTE_CODE:
				cE = rbce.cloneme(cE);
				break;
			case Method.OPTIMIZED_BYTE_CODE:
				cE = obce.cloneme(cE);
				break;
			case Method.OPTIMIZED_SHORT_CODE:
				cE = osce.cloneme(cE);
				break;
		}
	}

	// int pc=0;
	public boolean debuggerStep() {
		return true;
	}

	public void destroy(){
		Heap.removeThread(this);
	}
	private RawByteCodeExecutorNativeLong rbce=new RawByteCodeExecutorNativeLong(this);
	private OptimizedByteCodeExecutor obce=new OptimizedByteCodeExecutor(this);
	private OptimizedShortCodeExecutor osce=new OptimizedShortCodeExecutor(this);
	private ByteCodeExecutor cE=rbce;

	public boolean prun() {
		cE.steps=psteps;
		while(true)
		switch(cE.step()){
			case 0:
				return false;
			case 1:
				destroy();
				return true;
			case Method.BYTE_CODE:
				cE=rbce.cloneme(cE);
				continue;
			case Method.OPTIMIZED_BYTE_CODE:
				cE=obce.cloneme(cE);
				continue;
			case Method.OPTIMIZED_SHORT_CODE:
				cE=osce.cloneme(cE);
				continue;
		}
	}

	public void run() {
		cE.steps=Integer.MAX_VALUE;
		while(true)
			switch(cE.step()){
				case 0:
					continue;
				case 1:
					destroy();
					return;
				case Method.BYTE_CODE:
					//System.out.println("changed to byte code");
					cE=rbce.cloneme(cE);
					continue;
				case Method.OPTIMIZED_BYTE_CODE:
					//System.out.println("changed to optimized byte code");
					cE=obce.cloneme(cE);
					continue;
				case Method.OPTIMIZED_SHORT_CODE:
					//System.out.println("changed to optimized short code");
					cE=osce.cloneme(cE);
					continue;
			}
	}

	public static int s = 0;

	@Override
	public String toString() {
		return name;
	}

	public Frame getCurrentFrame() {
		return cE.stack.peek();
	}

	public FrameStack getStack() {
		return cE.stack;
	}

	public void checkReferences(Set<Integer> refs) {
		Iterator<Frame> it = cE.stack.iterator();
		Frame f = it.next();
		while(f!=null){
			f.checkReference(refs);
			f=it.next();
		}
	}
}
