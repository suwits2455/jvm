package com.neoexpert.jvm;

import java.util.Iterator;

public class FrameStack {
	private int pos=0;
	private Frame[] elements=new Frame[300];

	public Frame get(int position)
	{
		return elements[position];
	}

	public int size() {
		return pos;
	}

	public Frame pop() {
		--pos;
		Frame f=elements[pos];
		elements[pos]=null;
		return f;
	}

	public Frame peek() {
		return elements[pos-1];
	}

	public void push(Frame f) {
		elements[pos++]=f;
	}

	public boolean isEmpty() {
		return pos==0;
	}
	public Iterator<Frame> iterator() {
		return new StackIterator();
	}
	private class StackIterator implements Iterator<Frame>
	{
		private int n;
		public StackIterator(){
			if(pos<elements.length)
				n=pos;
			else
				n=elements.length;
		}

		@Override
		public void remove()
		{
			// TODO: Implement this method
		}

		int ipos=0;
		@Override
		public boolean hasNext() {
			return ipos<=n;
		}

		//not thread safe (may return null)
		@Override
		public Frame next() {
			try {
				return elements[ipos++];
			}catch (Exception e){
				return null;
			}
		}
	}
}
