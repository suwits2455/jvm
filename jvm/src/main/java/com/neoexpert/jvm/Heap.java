package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.instance.Instance;

import java.util.*;

public class Heap {
	public static boolean printgcstats;
	//all loaded classes which contains static non final static_references
	private static ArrayList<AClass> sclasses=new ArrayList<>(16);
	private static ArrayList<MyThread> threads=new ArrayList<>(16);

	public static void addClass(AClass cl){
		sclasses.add(cl);
	}
	public static void addThread(MyThread t){
		threads.add(t);
	}

	public static void removeThread(MyThread t) {
		if(!threads.remove(t))
			throw new RuntimeException();
	}

	public static String tostring() {
		return "gc collected: "+(addr-heap.size());
	}

	private static int addr = 1;
	private static Map<Integer, Object> heap = new HashMap<>();
	private static LinkedList<Integer> eden=new LinkedList<>();
	public static void reset() {
		heap.clear();
	}
	public static void removeFromHeap(int addr) {
		heap.remove(addr);
	}
	public static int add(Object c) {
		heap.put(addr, c);
		eden.add(addr);
		if(eden.size()>256)
			eden.removeFirst();
		//run garbage collector sometimes
		if(addr%32768==0)
			gc();
		return addr++;
	}

	private static  int gccounter=0;
	//simple garbage collector (may be buggy)
	public static void gc() {
		//if(rungc!=0)
		//	return;
		gccounter++;
		Map<Integer,Object> newheap=new HashMap<>();

		Set<Integer> roots=new HashSet<>();
		roots.addAll(permanent);


		checkStatic(roots);
		checkThreads(roots);

		Set<Integer> refs=new HashSet<>();
		roots.addAll(eden);
		roots.addAll(newObjects);
		for(Integer ref:roots){
			checkRoot(ref,refs);
		}
		for(Integer p:refs)
			newheap.put(p,heap.get(p));
		if(printgcstats) {
			System.out.println("threads: "+threads.size());
			System.out.println("gc: " + (heap.size() - newheap.size()) + " garbage objects collected");
			System.out.println("new heap size: " +  newheap.size()+" objects");
		}
		heap=newheap;
		System.gc();
		//heap.clear();
		//heap.putAll(newheap);
	}

	private static void checkRoot(Integer ref, Set<Integer> refs) {
		if(ref==null)return;
		if(ref==0)return;

		if(refs.contains(ref))
			return;
		refs.add(ref);
		Instance instance=(Instance) get(ref);
		if(instance instanceof ClassInstance) {
			ClassInstance ci= (ClassInstance) instance;
			AClass c = instance.getMyClass();
			List<String> ref_names= c.getReferenceNames();
			for(String name:ref_names){
					checkRoot((Integer) ci.getField(name), refs);
			}
			return;
		}
		if(instance instanceof ArrayInstance){
			ArrayInstance ai= (ArrayInstance) instance;
			if(ai.getType()==ArrayInstance.T_OBJECT){
				int[] array= (int[]) ai.a;
				for(int a:array)
					checkRoot(a,refs);
			}
		}
	}

	private static void checkThreads(Set<Integer> refs) {
		for(MyThread t:threads){
			t.checkReferences(refs);
		}
	}

	private static void checkStatic(Set<Integer> refs) {
		for(AClass c:sclasses){
			c.getStaticReferecnes(refs);
		}
	}

	public static void addToHeap(int oref1, Object o) {
		heap.put(oref1, o);
	}

	public static Object get(int a) {
		return heap.get(a);
	}

	private static Set<Integer> permanent=new HashSet<>();
	public static int addPermanent(ClassInstance ci) {
		int a = add(ci);
		makePermanent(a);
		return a;
	}

	public static void makePermanent(int addr) {
		permanent.add(addr);
	}

	public static void newObject(int oref){
		newObjects.add(oref);
	}

	private static Set<Integer> newObjects=new HashSet<>();

	public static void objectCreated(int i) {
		newObjects.remove(i);
	}
}
