package com.neoexpert.jvm.instance;

import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.Heap;
import com.neoexpert.jvm._abstract.AClass;

public class ArrayInstance extends Instance {
	private final byte type;
	private final int size;

	public AClass mc;

	public ArrayInstance(char[] ca, byte type) {
		this.type = type;
		this.size = ca.length;
		a = ca;
		create();
	}

	public ArrayInstance(int size, char l) {
		this.size = size;
		switch (l) {
			case 'I':
				type = T_INT;
				break;
			case 'Z':
				type = T_BOOLEAN;
				break;
			case 'B':
				type = T_BYTE;
				break;
			case 'C':
				type = T_CHAR;
				break;
			case 'D':
				type = T_DOUBLE;
				break;
			case 'F':
				type = T_FLOAT;
				break;
			case 'J':
				type = T_LONG;
				break;
			case 'S':
				type = T_SHORT;
				break;
			case 'L':
				type = T_OBJECT;
				break;
			default:
				throw new RuntimeException();
		}
		create();
	}

	public ArrayInstance(int size, byte type) {
		this.type = type;
		this.size = size;
		create();
	}

	public ArrayInstance(int dimensions, int dimension, int[] sizes, byte type) {
		if(dimension==(dimensions-1))
		{
			this.type = type;
			this.size = sizes[dimension];
			create();
			return;
		}
		this.type=T_OBJECT;
		this.size=sizes[dimension];
		create();
		int size=this.size;
		for(int i=0;i<size;i++){
			ArrayInstance ai = new ArrayInstance(dimensions, dimension + 1, sizes, type);
			((int[])a)[i]=Heap.add(ai);
		}
	}

	public int getLength() {
		switch (type) {
			case T_OBJECT:
			case T_INT:
				return ((int[]) a).length;
			case T_BOOLEAN:
				return ((boolean[]) a).length;
			case T_BYTE:
				return ((byte[]) a).length;
			case T_CHAR:
				return ((char[]) a).length;
			case T_DOUBLE:
				return ((double[]) a).length;
			case T_FLOAT:
				return ((float[]) a).length;
			case T_LONG:
				return ((long[]) a).length;
			case T_SHORT:
				return ((short[]) a).length;
			default:
				throw new RuntimeException("wrong array type");
		}
	}


	@Override
	public AClass getMyClass() {
		Object cl = ((ClassInstance) Heap.get(mc.getMyInstanceRef())).cl;
		if (cl instanceof AClass)
			return (AClass) cl;
		if (cl instanceof String)
			return Class.getClass((String) cl);
		throw new RuntimeException("no class defined");
	}

	public Object a;


	private void create() {
		switch (type) {
			case T_OBJECT:
				mc = Class.getArrayClass("[L");
				if(a==null)
					a = new int[size];
				break;
			case T_INT:
				mc = Class.getArrayClass("[I");
				if(a==null)
					a = new int[size];
				break;
			case T_BOOLEAN:
				mc = Class.getArrayClass("[Z");
				if(a==null)
					a = new boolean[size];
				break;
			case T_BYTE:
				mc = Class.getArrayClass("[B");
				if(a==null)
					a = new byte[size];
				break;
			case T_CHAR:
				mc = Class.getArrayClass("[C");
				if(a==null)
					a = new char[size];
				break;
			case T_DOUBLE:
				mc = Class.getArrayClass("[D");
				if(a==null)
					a = new double[size];
				break;
			case T_FLOAT:
				mc = Class.getArrayClass("[F");
				if(a==null)
					a = new float[size];
				break;
			case T_LONG:
				mc = Class.getArrayClass("[J");
				if(a==null)
					a = new long[size];
				break;
			case T_SHORT:
				mc = Class.getArrayClass("[S");
				if(a==null)
					a = new short[size];
				break;
			default:
				throw new RuntimeException("wrong array type");
		}
	}

	public static final byte T_OBJECT = 3;
	public static final byte T_BOOLEAN = 4;
	public static final byte T_CHAR = 5;
	public static final byte T_FLOAT = 6;
	public static final byte T_DOUBLE = 7;
	public static final byte T_BYTE = 8;
	public static final byte T_SHORT = 9;
	public static final byte T_INT = 10;
	public static final byte T_LONG = 11;

	public ArrayInstance cloneme() {
		ArrayInstance ai = new ArrayInstance(size, type);
		switch (type) {
			case T_OBJECT:
			case T_INT:
				ai.a = (((int[]) a).clone());
				break;
			case T_BOOLEAN:
				ai.a = (((boolean[]) a).clone());
				break;
			case T_BYTE:
				ai.a = (((byte[]) a).clone());
				break;
			case T_CHAR:
				ai.a = (((char[]) a).clone());
				break;
			case T_DOUBLE:
				ai.a = (((double[]) a).clone());
				break;
			case T_FLOAT:
				ai.a = (((float[]) a).clone());
				break;
			case T_LONG:
				ai.a = (((long[]) a).clone());
				break;
			case T_SHORT:
				ai.a = (((short[]) a).clone());
				break;
		}
		return ai;
	}

	public byte getType() {
		return type;
	}
}
