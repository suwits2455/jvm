package com.neoexpert.jvm.instance;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.MyThread;
import java.util.LinkedList;

public abstract class Instance {
	public abstract AClass getMyClass();
	private LinkedList<MyThread> ll=new LinkedList<>();

	public boolean monitorenter(MyThread t){
		boolean wait=false;
		if(!ll.isEmpty()){
			wait=ll.getFirst()!=t;
		}

		ll.add(t);
		return wait;
	}

	public MyThread monitorexit(MyThread t){
		if(ll.removeFirst()==t)
			return null;
		if(!ll.isEmpty())
			return ll.getFirst();
		return null;
	}
}
