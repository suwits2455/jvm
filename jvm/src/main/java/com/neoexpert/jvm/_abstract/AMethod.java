package com.neoexpert.jvm._abstract;

import com.neoexpert.jvm.method.MException;
import com.neoexpert.jvm.MyThread;

import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;

public abstract class AMethod {
	private final AClass cl;
	protected String name;
	public ArrayList<Character> args;

	public AMethod(AClass cl) {
		this.cl = cl;
	}

	public abstract boolean isStatic();

	public abstract int getParamsArrayLength();

	public final String getName() {
		return name;
	}

	public abstract String getSignature();

	public abstract boolean isAbstract();

	public abstract boolean isNative();

	public final AClass getMyClass() {
		return cl;
	}

	public abstract int[] getParams();

	public abstract int getModifiers();

	public abstract void setNative(boolean b);

	public abstract int getParameterCount();
}
