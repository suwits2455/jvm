package com.neoexpert.jvm._abstract;


import java.io.DataOutput;
import java.io.IOException;

public abstract class AField {

	protected int access_flags;
	public int getModifiers() {
		return access_flags;
	}

	public abstract void write(DataOutput raf) throws IOException;

	public abstract String getDesc();

	public abstract AClass getMyClass();

	public abstract boolean isStatic();

	public abstract String getName();

	public abstract void setName(String p0);

	public abstract int getType();

	public abstract String parseType();

	public abstract boolean isObject();
}
