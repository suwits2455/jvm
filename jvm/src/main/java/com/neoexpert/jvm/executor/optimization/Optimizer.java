package com.neoexpert.jvm.executor.optimization;

import com.neoexpert.jvm.constants.Constant;

import java.util.*;

public class Optimizer{
	private Map<Integer,Integer> p_old_to_new=new HashMap<>();
	private Map<Integer,Integer> p_new_to_old=new HashMap<>();

	//optimized code
	private List<Byte> ocode=new ArrayList<>();
	//precalculated params
	private ArrayList<Integer> params=new ArrayList<>();
	public byte[] getCode(){
		byte[] code=new byte[ocode.size()];	
		for(int i=0;i<ocode.size();i++){
			code[i]=ocode.get(i);
		}
		return code;
	}
	public int[] getParams(){
		int[] _params=new int[params.size()];	
		for(int i=0;i<params.size();i++){
			_params[i]=params.get(i);
		}
		return _params;
	}

	public void addIndex(){
		int i=params.size()-1;
		if(i>127)
			success=false;
		ocode.add((byte)i);
	}
	ArrayList<Integer> jumps=new ArrayList<>();

	public static int counter=0;
	public void addJump(int pos){
		jumps.add(pos);
	}

	private void calculateJump(int newpc,int i){
		int pindex=ocode.get(i);
		int pc=p_new_to_old.get(newpc);
		int oldjump=params.get(pindex);
		pc+=oldjump;
		int newpos=p_old_to_new.get(pc);
		params.set(pindex,newpos-newpc);
	}

	public Optimizer(byte[] code) {
		try {
			optimize(code);
		}
		catch (RuntimeException e){
			success=false;
		}
	}
	
	private boolean success=false;
	public boolean success(){
		return success;	
	}


	public void optimize(byte[] code){
		counter++;
		success=true;
		//local variables are better for performance
		//program counter
		int pc = 0;
		int i1, i2, i3, i4, aref, oref, jump;
		long l1, l2;
		float f1, f2;
		double d1, d2;
		Object obj;
		Constant fc;
		Constant fname;
		Constant ftype;

		while(pc<code.length){
			/*
			   StringBuilder sb = new StringBuilder();
			   int aa=CodeAttribute.parseOp(pc, code, sb);
			   System.out.println(sb);
			//*/

			switch (code[pc]) {
				case 0:
					pc++;
					continue;
				case (byte) 0x10:
					//bipush
				case (byte) 0x12:
					//ldc
				case (byte) 0x15:
					//iload
				case (byte) 0x16:
					//lload
				case (byte) 0x17:
					//fload
				case (byte) 0x18:
					//dload
				case (byte) 0x19:
					//aload
				case (byte) 0x36:
					//istore
				case (byte) 0x37:
					//lstore
				case (byte) 0x38:
					//fstore
				case (byte) 0x39:
					//dstore
				case (byte) 0x3A:
					//astore
				case (byte) 0xA9:
					//ret
				case (byte) 0xBC:
					//newarray
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					ocode.add(code[pc+1]);
					pc+=2;
					continue;
				case (byte) 0x99:
					//ifeq
				case (byte) 0x9A:
					//ifne
				case (byte) 0x9B:
					//iflt
				case (byte) 0x9C:
					//ifge
				case (byte) 0x9D:
					//ifgt
				case (byte) 0x9E:
					//ifle
				case (byte) 0x9F:
					//if_icmpeq
				case (byte) 0xA0:
					//if_icmpne
				case (byte) 0xA1:
					//if_icmplt
				case (byte) 0xA2:
					//if_icmpge
				case (byte) 0xA3:
					//if_icmpgt
				case (byte) 0xA4:
					//if_icmple
				case (byte) 0xA5:
					//if_acmpeq
				case (byte) 0xA6:
					//if_acmpeq
				case (byte) 0xA7:
					//goto
				case (byte) 0xA8:
					//jsr
				case (byte) 0xC6:
					//ifnull
				case (byte) 0xC7:
					//ifnonnull
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					addJump(ocode.size()-1);
					int p1=i(code[pc + 1], code[pc + 2]);
					int p2=ii(code[pc + 1], code[pc + 2]);
					params.add((int)i(code[pc + 1], code[pc + 2]));
					addIndex();
					pc += 3;
					continue;
				case (byte) 0x84:
					//iinc
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					ocode.add(code[pc+1]);
					ocode.add(code[pc+2]);
					pc += 3;
					continue;
				case (byte) 0x11:
					//sipush
				case (byte) 0x13:
					//ldc_w
				case (byte) 0x14:
					//ldc2_w
				case (byte) 0xB2:
					//getstatic
				case (byte) 0xB3:
					//putstatic
				case (byte) 0xB4:
					//getfield
				case (byte) 0xB5:
					//putfield
				case (byte) 0xB6:
					//invokevirtual
				case (byte) 0xB7:
					//invokespecial
				case (byte) 0xB8:
					//invokestatic
				case (byte) 0xBB:
					//new
				case (byte) 0xC0:
					//checkcast
				case (byte) 0xC1:
					//instanceof
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					params.add(ii(code[pc + 1], code[pc + 2]));
					addIndex();
					pc += 3;
					continue;
				case (byte) 0xAA:
					//tableswitch
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					addJump(ocode.size()-1);
					pc = pc + (4 - pc % 4);
					int default_offset = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
					params.add(default_offset);
					addIndex();
					pc += 4;
					int low = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
					params.add(low);
					pc += 4;
					int high = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
					params.add(high);
					pc += 4;
					int pc2=pc;
					
					for(i1=low;i1<=high;i1++){
						pc=pc2 + (i1 - low) * 4;
						jump= i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
						params.add(jump);
					}
					pc+=4;
					int n = high - low + 1;

					continue;
				case (byte) 0xAB:
					//lookupswitch
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					addJump(ocode.size()-1);
					pc = pc + (4 - pc % 4);
					default_offset = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
					params.add(default_offset);
					addIndex();
					pc += 4;
					i2 = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
					params.add(i2);
					pc += 4;
					for (i3 = 0; i3 < i2; i3++) {
						int key = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
						params.add(key);

						pc += 4;
						int value = i(code[pc + 0], code[pc + 1], code[pc + 2], code[pc + 3]);
						params.add(value);
						pc += 4;
					}
					continue;
				case (byte) 0xB9:
					//invokeinterface
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					//index to constantpool
					i1=ii(code[pc + 1], code[pc + 2]);
					params.add(i1);
					addIndex();

					byte count = code[pc + 3];
					ocode.add(count);
					byte zero = code[pc + 4];
					ocode.add(zero);
					pc += 5;
					continue;
				case (byte) 0xBA:
					//invokedynamic
					throw new RuntimeException("invokedynamic not implemented");
				case (byte) 0xBD:
					//anewarray
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					pc += 3;
					continue;
				case (byte) 0xC4:
					//wide
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					pc += 1;
					switch (code[pc]) {
						case (byte) 0x15:
							//iload
						case (byte) 0x16:
							//lload
						case (byte) 0x17:
							//fload
						case (byte) 0x18:
							//dload
						case (byte) 0x19:
							//aload
						case (byte) 0x36:
							//istore
						case (byte) 0x37:
							//lstore
						case (byte) 0x38:
							//fstore
						case (byte) 0x39:
							//dstore
						case (byte) 0x3A:
							//astore
							continue;
						case (byte) 0x84:
							//iinc
							//i1 = (int) lv[ii(code[pc + 1], code[pc + 2])];
							//lv[(int) code[pc + 1]] = i1 + ii(code[pc + 3], code[pc + 4]);
							pc += 5;
							continue;

					}
					throw new RuntimeException("wide not implememted");
					//continue;
				case (byte) 0xC5:
					//multianewarray
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					i1 = i(code[pc + 1], code[pc + 2]);
					//dimensions
					i2 = code[pc + 1];
					pc += 4;
					throw new RuntimeException("not implememted");
					//continue;
				case (byte) 0xC8:
					//goto
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					addJump(ocode.size()-1);
					jump=i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
					params.add(jump);
					addIndex();
					continue;
				case (byte) 0xC9:
					//jsr_w
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					addJump(ocode.size()-1);
					jump= i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
					params.add(jump);
					addIndex();
					continue;
				default:
					ocode.add(code[pc]);
					p_old_to_new.put(pc,ocode.size()-1);
					p_new_to_old.put(ocode.size()-1,pc);
					pc += 1;
			}
		}
		for(Integer j:jumps){
			byte op=ocode.get(j);
			switch (op) {
				case (byte) 0x99:
					//ifeq
				case (byte) 0x9A:
					//ifne
				case (byte) 0x9B:
					//iflt
				case (byte) 0x9C:
					//ifge
				case (byte) 0x9D:
					//ifgt
				case (byte) 0x9E:
					//ifle
				case (byte) 0x9F:
					//if_icmpeq
				case (byte) 0xA0:
					//if_icmpne
				case (byte) 0xA1:
					//if_icmplt
				case (byte) 0xA2:
					//if_icmpge
				case (byte) 0xA3:
					//if_icmpgt
				case (byte) 0xA4:
					//if_icmple
				case (byte) 0xA5:
					//if_acmpeq
				case (byte) 0xA6:
					//if_acmpeq
				case (byte) 0xA7:
					//goto
				case (byte) 0xA8:
					//jsr
				case (byte) 0xC6:
					//ifnull
				case (byte) 0xC7:
					//ifnonnull
						calculateJump(j, j + 1);
					continue;
				case (byte) 0xAA:
					//tableswitch
					int index=ocode.get(j+1);
					int default_offset = params.get(index++);
					int low= params.get(index++);
					int high= params.get(index++);
					calculateJump(j,j+1);


					for(i2=low;i2<=high;i2++){
						pc=p_new_to_old.get(j);
						int oldjump=params.get(index);
						pc+=oldjump;
						int newpos=p_old_to_new.get(pc);
						params.set(index,newpos-j);
						index++;
					}

					continue;
				case (byte) 0xAB:
					//lookupswitch
					index=ocode.get(j+1);
					index++;
					calculateJump(j,j+1);

					i2 = params.get(index++);
					for (i3 = 0; i3 < i2; i3++) {
						index++;
						pc=p_new_to_old.get(j);
						int oldjump=params.get(index);
						pc+=oldjump;
						int newpos=p_old_to_new.get(pc);
						params.set(index,newpos-j);
						index++;
					}
					continue;
				case (byte) 0xC8:
					//goto
					calculateJump(j,j+1);
					continue;
				case (byte) 0xC9:
					//jsr_w
					calculateJump(j,j+1);
					continue;
				default:
					throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
					// cs+= byteToHex(code[i])+"\n";
			}
		}
	}

	private int l2i1(long l) {
		return (int) (l >> 32);
	}

	private int l2i2(long l) {
		return (int) l;
	}


	private static short i(byte b1, byte b2) {
		return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}

	private static int ii(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(byte b1, byte b2, byte b3, byte b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}


}
