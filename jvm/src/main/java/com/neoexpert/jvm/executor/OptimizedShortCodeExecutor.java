package com.neoexpert.jvm.executor;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.instance.Instance;
import com.neoexpert.jvm.method.Method;
import com.neoexpert.jvm.natives.NativeMethod;
import com.neoexpert.jvm.natives.Natives;


public class OptimizedShortCodeExecutor extends ByteCodeExecutor{


	public OptimizedShortCodeExecutor(MyThread thread) {
		super(thread);
	}


	public void run() {
		steps=Integer.MAX_VALUE;
		step();
	}

	public static int s = 0;

	//The Holy Grail
	@Override
	public int step() {
		if (stack.isEmpty())
		{
			//thread.destroy();
			return 1;
		}
		//local variables are better for performance
		//current Frame
		Frame cF = stack.peek();
		//program counter
		int pc = cF.pc;
		ClassInstance c;
		int i1, i2, i3, i4, aref, oref;
		long l1, l2;
		float f1, f2;
		double d1, d2;
		AClass cl;
		AMethod m;
		ArrayInstance array;
		Object o1,o2,o3;
		Constant fc;
		Constant fname;
		Constant ftype;

		short[] code = (short[])cF.code;
		int[] params = cF._params;
		Object[] lv = cF.lv;
		Object[] opstack=cF.stack;
		int stackpos=cF.stackpos;
		//byte op = code[pc];

		//if (cF.isThrowing())
		//op = (byte) 0xBF;

		int steps = this.steps;
		mainLoop:
		do {
			/*
			   StringBuilder sb = new StringBuilder();
			//sb.append(s);
			//sb.append(": ");
			try {
				CodeAttribute.parseOp(pc, code, sb);
			}catch (ArrayIndexOutOfBoundsException e){}
			   System.out.println(sb);
			s++;
			//*/

			switch (code[pc]) {
				case (byte) 0x00:
					//nop
					pc += 1;
					continue;
				case (byte) 0x01:
					//aconst_null
					opstack[stackpos++] = iconst_0;
					pc += 1;
					continue;
				case (byte) 0x02:
					//iconst_m1
					opstack[stackpos++] = iconst_m1;
					pc += 1;
					continue;
				case (byte) 0x03:
					//iconst_0
					opstack[stackpos++] = iconst_0;
					pc += 1;
					continue;
				case (byte) 0x04:
					//iconst_1
					opstack[stackpos++] = iconst_1;
					pc += 1;
					continue;
				case (byte) 0x05:
					//iconst_2
					opstack[stackpos++] = iconst_2;
					pc += 1;
					continue;
				case (byte) 0x06:
					//iconst_3
					opstack[stackpos++] = iconst_3;
					pc += 1;
					continue;
				case (byte) 0x07:
					//iconst_4
					opstack[stackpos++] = iconst_4;
					pc += 1;
					continue;
				case (byte) 0x08:
					//iconst_5
					opstack[stackpos++] = iconst_5;
					pc += 1;
					continue;
				case (byte) 0x09:
					//lconst_0
					opstack[stackpos++] = lconst_0;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (byte) 0x0A:
					//lconst_1
					opstack[stackpos++] = lconst_1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (byte) 0x0B:
					//fconst_0
					opstack[stackpos++] = 0f;
					pc += 1;
					continue;
				case (byte) 0x0C:
					//fconst_1
					opstack[stackpos++] = 1f;
					pc += 1;
					continue;
				case (byte) 0x0D:
					//fconst_2
					opstack[stackpos++] = 2f;
					pc += 1;
					continue;
				case (byte) 0x0E:
					//dconst_0
					l1 = Double.doubleToRawLongBits(0.0);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;// i1;
					opstack[stackpos++] = null;//i2;
					pc += 1;
					continue;
				case (byte) 0x0F:
					//dconst_1
					l1 = Double.doubleToRawLongBits(1.0);
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;//i2;
					pc += 1;
					continue;
				case (byte) 0x10:
					//bipush
					opstack[stackpos++] = (int)code[pc+1];
					pc += 2;
					continue;
				case (byte) 0x11:
					//sipush
					opstack[stackpos++] = params[code[pc+1]];
					pc += 2;
					continue;
				case (byte) 0x12:
					//ldc
					cF.stack=opstack;
					cF.stackpos=stackpos;
					cC.ldc((int) code[pc + 1] & 0xFF, cF);
					opstack=cF.stack;
					stackpos=cF.stackpos;
					pc += 2;
					continue;
				case (byte) 0x13:
					//ldc_w
					cF.stack=opstack;
					cF.stackpos=stackpos;
					cC.ldc(params[code[pc+1]], cF);
					opstack=cF.stack;
					stackpos=cF.stackpos;
					pc += 2;
					continue;
				case (byte) 0x14:
					//ldc2_w
					cF.stack=opstack;
					cF.stackpos=stackpos;
					cC.ldc2(params[code[pc + 1]], cF);
					opstack=cF.stack;
					stackpos=cF.stackpos;
					pc += 2;
					continue;
				case (byte) 0x15:
					//iload
					opstack[stackpos++] = lv[(int)code[pc+1]];
					pc += 2;
					continue;
				case (byte) 0x16:
					//lload
					opstack[stackpos++] =lv[(int) code[pc + 1] + 1] ;
					opstack[stackpos++] =lv[(int) code[pc + 1]] ;
					pc += 2;
					continue;
				case (byte) 0x17:
					//fload
					opstack[stackpos++] =lv[(int) code[pc + 1]] ;
					pc += 2;
					continue;
				case (byte) 0x18:
					//dload
					opstack[stackpos++] =lv[(int) code[pc + 1]+1];
					opstack[stackpos++] =lv[(int) code[pc + 1]] ;
					pc += 2;
					continue;
				case (byte) 0x19:
					//aload
					opstack[stackpos++] =lv[(int) code[pc + 1]] ;
					pc += 2;
					continue;
				case (byte) 0x1A:
					//iload_0
					opstack[stackpos++] =lv[0];
					pc += 1;
					continue;
				case (byte) 0x1B:
					//iload_1
					opstack[stackpos++] =lv[1];
					pc += 1;
					continue;
				case (byte) 0x1C:
					//iload_2
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (byte) 0x1D:
					//iload_3";
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (byte) 0x1E:
					//lload_0;
					opstack[stackpos++] =lv[1];
					opstack[stackpos++] =lv[0] ;
					pc += 1;
					continue;
				case (byte) 0x1F:
					//lload_1
					opstack[stackpos++] = lv[2];
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (byte) 0x20:
					//lload_2
					opstack[stackpos++] = lv[3];
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (byte) 0x21:
					//lload_3
					opstack[stackpos++] = lv[4];
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (byte) 0x22:
					//fload_0
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (byte) 0x23:
					//fload_1
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (byte) 0x24:
					//fload_2
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (byte) 0x25:
					//fload_3;
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (byte) 0x26:
					//dload_0;
					opstack[stackpos++] = lv[1];
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (byte) 0x27:
					//dload_1
					opstack[stackpos++] = lv[2];
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (byte) 0x28:
					//dload_2
					opstack[stackpos++] = lv[3];
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (byte) 0x29:
					//dload_3
					opstack[stackpos++] = lv[4];
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (byte) 0x2A:
					//aload_0
					opstack[stackpos++] = lv[0];
					pc += 1;
					continue;
				case (byte) 0x2B:
					//aload_1
					opstack[stackpos++] = lv[1];
					pc += 1;
					continue;
				case (byte) 0x2C:
					//aload_2
					opstack[stackpos++] = lv[2];
					pc += 1;
					continue;
				case (byte) 0x2D:
					//aload_3
					opstack[stackpos++] = lv[3];
					pc += 1;
					continue;
				case (byte) 0x2E:
					//iaload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =((int[]) (array.a))[i1];
					pc += 1;
					continue;
				case (byte) 0x2F:
					//laload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					l1 = ((long[]) array.a)[i1];
					opstack[stackpos++] =(int)(l1>>32);
					opstack[stackpos++] =(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x30:
					//faload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =((float[]) array.a)[i1];
					pc += 1;
					continue;
				case (byte) 0x31:
					//daload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					d1=((double[]) array.a)[i1];
					opstack[stackpos++] = -1;
					opstack[stackpos++] = -1;
					pc += 1;
					throw new RuntimeException("double arrays not implemented");
					//continue;
				case (byte) 0x32:
					//aaload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =((int[]) array.a)[i1];
					pc += 1;
					continue;
				case (byte) 0x33:
					//baload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =(int) ((byte[]) array.a)[i1];
					pc += 1;
					continue;
				case (byte) 0x34:
					//caload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =(int) ((char[]) array.a)[i1];
					pc += 1;
					continue;
				case (byte) 0x35:
					//saload
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] =(int) ((short[]) array.a)[i1];
					pc += 1;
					continue;
				case (byte) 0x36:
					//istore
					lv[(int) code[pc + 1]] = opstack[--stackpos];
					pc += 2;
					continue;
				case (byte) 0x37:
					//lstore
					lv[(int) code[pc + 1]] = opstack[--stackpos];
					lv[(int) code[pc + 1] + 1] = opstack[--stackpos];
					pc += 2;
					continue;
				case (byte) 0x38:
					//fstore
					lv[(int) code[pc + 1]] = opstack[--stackpos];
					pc += 2;
					continue;
				case (byte) 0x39:
					//dstore
					lv[(int) code[pc + 1]] = opstack[--stackpos];
					lv[(int) code[pc + 1] + 1] = opstack[--stackpos];
					pc += 2;
					continue;
				case (byte) 0x3A:
					//astore
					oref = (int) opstack[--stackpos];
					lv[code[pc + 1]] = oref;
					cF.lvpt[code[pc + 1]] = oref;
					pc += 2;
					continue;
				case (byte) 0x3B:
					//istore_0
					lv[0] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x3C:
					//istore_1
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x3D:
					//istore_2
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x3E:
					//istore_3
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x3F:
					//lstore_0
					lv[0] = opstack[--stackpos];
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x40:
					//lstore_1
					lv[1] = opstack[--stackpos];
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x41:
					//lstore_2
					lv[2] = opstack[--stackpos];
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x42:
					//lstore_3
					lv[3] = opstack[--stackpos];
					lv[4] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x43:
					//fstore_0
					lv[0] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x44:
					//fstore_1
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x45:
					//fstore_2
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x46:
					//fstore_3"
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x47:
					//dstore_0
					lv[0] = opstack[--stackpos];
					lv[1] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x48:
					//dstore_1
					lv[1] = opstack[--stackpos];
					lv[2] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x49:
					//dstore_2
					lv[2] = opstack[--stackpos];
					lv[3] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x4A:
					//dstore_3
					lv[3] = opstack[--stackpos];
					lv[4] = opstack[--stackpos];
					pc += 1;
					continue;
				case (byte) 0x4B:
					//astore_0
					lv[0] = opstack[--stackpos];
					cF.lvpt[0]= (int) lv[0];
					pc += 1;
					continue;
				case (byte) 0x4C:
					//astore_1
					lv[1] = opstack[--stackpos];
					cF.lvpt[1]= (int) lv[1];
					pc += 1;
					continue;
				case (byte) 0x4D:
					//astore_2
					lv[2] = opstack[--stackpos];
						cF.lvpt[2] = (int) lv[2];
					pc += 1;
					continue;
				case (byte) 0x4E:
					//astore_3
					lv[3] = opstack[--stackpos];
					cF.lvpt[3]= (int) lv[3];
					pc += 1;
					continue;
				case (byte) 0x4F:
					//iastore
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((int[]) array.a)[i2] = i1;
					pc += 1;
					continue;
				case (byte) 0x50:
					//lastore
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					i3 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((long[]) array.a)[i3] = l1;
					pc += 1;
					continue;
				case (byte) 0x51:
					//fastore
					f1 = (float) opstack[--stackpos];
					i1 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((float[]) array.a)[i1] = f1;
					pc += 1;
					continue;
				case (byte) 0x52:
					//dastore
					aref = (int) opstack[--stackpos];
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					i3 = (int) opstack[--stackpos];
					pc += 1;
					throw new RuntimeException("implement me!");
					//continue;
				case (byte) 0x53:
					//aastore
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((int[]) array.a)[i2] = i1;
					pc += 1;
					continue;
				case (byte) 0x54:
					//bastore
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((byte[]) array.a)[i2] = (byte) i1;
					pc += 1;
					//throw new RuntimeException("implement me!");
					continue;
				case (byte) 0x55:
					//castore
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((char[]) array.a)[i2] = (char) i1;
					pc += 1;
					continue;
				case (byte) 0x56:
					//sastore
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					((short[]) array.a)[i2] = (short) i1;
					pc += 1;
					continue;
				case (byte) 0x57:
					//pop
					stackpos--;
					pc += 1;
					continue;
				case (byte) 0x58:
					//pop2
					stackpos-=2;
					pc += 1;
					continue;
				case (byte) 0x59:
					//dup
					i1= (int) opstack[stackpos - 1];
					opstack[stackpos++] = i1;
					pc += 1;
					continue;
				case (byte) 0x5A:
					//dup_x1
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i1;
					opstack[stackpos++] = i2;
					opstack[stackpos++] = i1;
					pc += 1;
					continue;
				case (byte) 0x5B:
					//dup_x2
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					i3 = (int) opstack[--stackpos];
					opstack[stackpos++] = i1;
					opstack[stackpos++] = i3;
					opstack[stackpos++] = i2;
					opstack[stackpos++] = i1;
					pc += 1;
					continue;
				case (byte) 0x5C:
					//dup2
					o2 =  opstack[--stackpos];
					o1 =  opstack[--stackpos];
					opstack[stackpos++] = o1;
					opstack[stackpos++] = o2;
					opstack[stackpos++] = o1;
					opstack[stackpos++] = o2;
					pc += 1;
					continue;
				case (byte) 0x5D:
					//dup2_x1
					o1 =  opstack[--stackpos];
					o2 =  opstack[--stackpos];
					o3 =  opstack[--stackpos];

					opstack[stackpos++] = o2;
					opstack[stackpos++] = o1;
					opstack[stackpos++] = o3;
					opstack[stackpos++] = o2;
					opstack[stackpos++] = o1;
					pc += 1;
					//throw new RuntimeException("check that and contniue");
					continue;
				case (byte) 0x5E:
					//dup2_x2
					i2 = (int) opstack[--stackpos];
					i1 = (int) opstack[--stackpos];
					i4 = (int) opstack[--stackpos];
					i3 = (int) opstack[--stackpos];
					opstack[stackpos++] = i1;
					opstack[stackpos++] = i2;
					opstack[stackpos++] = i3;
					opstack[stackpos++] = i4;
					opstack[stackpos++] = i1;
					opstack[stackpos++] = i2;
					pc += 1;
					throw new RuntimeException("check that and continue");
					//continue;
				case (byte) 0x5F:
					//swap
					opstack[stackpos++] = opstack[0];
					pc += 1;
					throw new RuntimeException("chack that and continue");
					//continue;
				case (byte) 0x60:
					//iadd
					o1=(Integer)opstack[--stackpos]+(Integer)opstack[--stackpos];
					opstack[stackpos++] = o1;
					pc += 1;
					continue;
				case (byte) 0x61:
					//ladd
					stackpos--;
					l1 = (long) opstack[--stackpos];//_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long)opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 + l1;
					opstack[stackpos++] =l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x62:
					//fadd
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f1+f2;
					pc += 1;
					continue;
				case (byte) 0x63:
					//dadd
					stackpos--;
					l1 = (long) opstack[--stackpos]; //_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos]; //_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 + d1;
					l1 = Double.doubleToRawLongBits(d1);
					opstack[stackpos++] = l1;//i1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (byte) 0x64:
					//isub
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 - i1;
					pc += 1;
					continue;
				case (byte) 0x65:
					//lsub
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 - l1;
					opstack[stackpos++] = l1;//(int)(l1>>32);
					opstack[stackpos++] = null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x66:
					//fsub
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2-f1;
					pc += 1;
					continue;
				case (byte) 0x67:
					//dsub
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 - d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;// i1;
					opstack[stackpos++] = null;//i2;
					pc += 1;
					continue;
				case (byte) 0x68:
					//imul
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2*i1;
					pc += 1;
					continue;
				case (byte) 0x69:
					//lmul
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l1 * l2;
					opstack[stackpos++] = l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x6A:
					//fmul
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2*f1;
					pc += 1;
					continue;
				case (byte) 0x6B:
					//dmul
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 * d1;
					l1 = Double.doubleToRawLongBits(d1);
					opstack[stackpos++] =l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x6C:
					//idiv
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 / i1;
					pc += 1;
					continue;
				case (byte) 0x6D:
					//ldiv
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 / l1;
					opstack[stackpos++] = l1;// (int)(l1>>32);
					opstack[stackpos++] = null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x6E:
					//fdiv
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					opstack[stackpos++] = f2 / f1;
					pc += 1;
					continue;
				case (byte) 0x6F:
					//ddiv
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 / d1;
					l1 = Double.doubleToRawLongBits(d1);
					opstack[stackpos++] = (int)(l1>>32);
					opstack[stackpos++] = (int)(l1);
					pc += 1;
					continue;
				case (byte) 0x70:
					//irem
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 % i1;
					pc += 1;
					continue;
				case (byte) 0x71:
					//lrem
					stackpos--;
					l1 = (long) opstack[--stackpos];//_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 % l1;
					opstack[stackpos++] = l1;//(int)(l1>>32);
					opstack[stackpos++] = null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x72:
					//frem
					f1 = (int) opstack[--stackpos];
					f2 = (int) opstack[--stackpos];
					opstack[stackpos++] = f2 % f1;
					pc += 1;
					continue;
				case (byte) 0x73:
					//drem
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 % d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;//i1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (byte) 0x74:
					//ineg
					i1=-(int)opstack[--stackpos];
					opstack[stackpos++] = i1;
					pc += 1;
					continue;
				case (byte) 0x75:
					//lneg
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = -l1;
					opstack[stackpos++] =l1;//(int)(l1>>32) ;
					opstack[stackpos++] =null;//(int)(l1) ;
					pc += 1;
					continue;
				case (byte) 0x76:
					//fneg
					f1=-(float)opstack[--stackpos];

					opstack[stackpos++] =f1;
					pc += 1;
					continue;
				case (byte) 0x77:
					//dneg
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d1 = -d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;//i2;
					pc += 1;
					continue;
				case (byte) 0x78:
					//ishl
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 << i1;
					pc += 1;
					continue;
				case (byte) 0x79:
					//lshl
					i1 = (int) opstack[--stackpos];
					stackpos--;
					l1 = (long) opstack[--stackpos];//_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos])<<i1;
					opstack[stackpos++] =l1<<i1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x7A:
					//ishr
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 >> i1;
					pc += 1;
					continue;
				case (byte) 0x7B:
					//lshr
					i1 = (int) opstack[--stackpos];
					stackpos--;
					l1 = (long) opstack[--stackpos]>>i1;// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos])>>i1;
					opstack[stackpos++] = l1;//(int)(l1>>32) ;
					opstack[stackpos++] = null;//(int)(l1) ;
					pc += 1;
					/*
					i1 = (int) opstack[--stackpos];
					l2 = _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 >> i1;
					opstack[stackpos++] = -1;
					opstack[stackpos++] = -1;
					pc += 1;*/
					continue;
				case (byte) 0x7C:
					//iushr
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 >>> i1;
					pc += 1;
					continue;
				case (byte) 0x7D:
					//lushr
					i1 = (int) opstack[--stackpos];
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos])>>>i1;
					opstack[stackpos++] =l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x7E:
					//iand
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 & i1;
					pc += 1;
					continue;
				case (byte) 0x7F:
					//land
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 & l1;
					opstack[stackpos++] = l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x80:
					//ior
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 | i1 ;
					pc += 1;
					continue;
				case (byte) 0x81:
					//lor
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 | l1;
					opstack[stackpos++] =l1;//=(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1) ;
					pc += 1;
					continue;
				case (byte) 0x82:
					//ixor
					i1 = (int) opstack[--stackpos];
					i2 = (int) opstack[--stackpos];
					opstack[stackpos++] = i2 ^ i1;
					pc += 1;
					continue;
				case (byte) 0x83:
					//lxor
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l1 = l2 ^ l1;
					opstack[stackpos++] = l1;//(int)(l1>>32);
					opstack[stackpos++] = null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x84:
					//iinc
					i1 = (int) lv[(int) code[pc + 1]];
					lv[(int) code[pc + 1]] = i1 + code[pc + 2];
					pc += 3;
					continue;
				case (byte) 0x85:
					//_i2l
					i1= (int) opstack[--stackpos];
					l1=i1;
					opstack[stackpos++] =l1;//((int)l1>>32) ;
					opstack[stackpos++] = null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x86:
					//i2f
					f1=Float.valueOf((int) opstack[--stackpos] );
					opstack[stackpos++] =f1;
					pc += 1;
					continue;
				case (byte) 0x87:
					//i2d
					i1 = (int) opstack[--stackpos];
					l1 = Double.doubleToRawLongBits(i1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;//i2;
					pc += 1;
					continue;
				case (byte) 0x88:
					//_l2i
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					opstack[stackpos++] = (int)l1;
					pc += 1;
					continue;
				case (byte) 0x89:
					//l2f
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					opstack[stackpos++] = (float)l1;
					pc += 1;
					continue;
				case (byte) 0x8A:
					//l2d
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = (double) l1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;
					pc += 1;
					continue;
				case (byte) 0x8B:
					//f2i
					i1=((Float) opstack[--stackpos]).intValue();
					opstack[stackpos++] =i1;
					pc += 1;
					continue;
				case (byte) 0x8C:
					//f2l
					f1 = (float) opstack[--stackpos];
					l1 = (long) f1;
					opstack[stackpos++] =(int)(l1>>32) ;
					opstack[stackpos++] =(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x8D:
					//f2d
					f1 = (float) opstack[--stackpos];
					d1 = (double) f1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					opstack[stackpos++] = l1;
					opstack[stackpos++] =null;// i2;
					pc += 1;
					continue;
				case (byte) 0x8E:
					//d2i
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					opstack[stackpos++] = (int)d1;
					pc += 1;
					continue;
				case (byte) 0x8F:
					//d2l
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					l1 = (long) d1;
					opstack[stackpos++] =l1;//(int)(l1>>32);
					opstack[stackpos++] =null;//(int)(l1);
					pc += 1;
					continue;
				case (byte) 0x90:
					//d2f
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					opstack[stackpos++] = (float)d1;
					pc += 1;
					throw new RuntimeException("implement me!");
					//continue;
				case (byte) 0x91:
					//i2b
					i1= (int) opstack[--stackpos];
					opstack[stackpos++] =(int) ((Integer)i1 ).byteValue();
					pc += 1;
					continue;
				case (byte) 0x92:
					//i2c
					i1= (int) opstack[--stackpos];
					opstack[stackpos++] = (int)(char)i1;
					pc += 1;
					continue;
				case (byte) 0x93:
					//i2s
						i1=(int) ((Integer) opstack[--stackpos]).shortValue();
					opstack[stackpos++] =i1;
					pc += 1;
					continue;
				case (byte) 0x94:
					//lcmp
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];//_2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					if (l2 == l1) {
						opstack[stackpos++] = 0;
					} else if (l2 > l1) {
						opstack[stackpos++] = +1;
					} else
						opstack[stackpos++] = -1;
					pc += 1;
					continue;
				case (byte) 0x95:
					//fcmpl
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					if (f2 == f1) {
						opstack[stackpos++] = 0;
					} else if (f2 > f1) {
						opstack[stackpos++] = +1;
					} else if (f2 < f1)
					opstack[stackpos++] = -1;
					else
					opstack[stackpos++] = -1;
					pc += 1;
					continue;
				case (byte) 0x96:
					//fcmpg
					f1 = (float) opstack[--stackpos];
					f2 = (float) opstack[--stackpos];
					if (f2 == f1) {
						opstack[stackpos++] = 0;
					} else if (f2 > f1) {
						opstack[stackpos++] = +1;
					} else if (f2 < f1)
					opstack[stackpos++] = -1;
					else
					opstack[stackpos++] = +1;
					pc += 1;
					continue;
				case (byte) 0x97:
					//dcmpl
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					if (d2 == d1) {
						opstack[stackpos++] = 0;
					} else if (d2 > d1) {
						opstack[stackpos++] = 1;
					} else if (d2 < d1)
					opstack[stackpos++] = -1;
					else
					opstack[stackpos++] = -1;
					pc += 1;
					continue;
				case (byte) 0x98:
					//dcmpg
					stackpos--;
					l1 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					stackpos--;
					l2 = (long) opstack[--stackpos];// _2i2l((int) opstack[--stackpos], (int) opstack[--stackpos]);
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					if (d2 == d1) {
						opstack[stackpos++] = 0;
					} else if (d2 > d1) {
						opstack[stackpos++] = +1;
					} else if (d2 < d1)
					opstack[stackpos++] = -1;
					else
					opstack[stackpos++] = +1;
					pc += 1;
					continue;
				case (byte) 0x99:
					//ifeq
					if ((int) opstack[--stackpos] == 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9A:
					//ifne
					if ((int) opstack[--stackpos] != 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9B:
					//iflt
					if ((int) opstack[--stackpos] < 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9C:
					//ifge
					if ((int) opstack[--stackpos] >= 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9D:
					//ifgt
					if ((int) opstack[--stackpos] > 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9E:
					//ifle
					if ((int) opstack[--stackpos] <= 0)
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0x9F:
					//if_icmpeq
					if ((int) opstack[--stackpos] == (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA0:
					//if_icmpne
					if ((int) opstack[--stackpos] != (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA1:
					//if_icmplt
					if ((int) opstack[--stackpos] > (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA2:
					//if_icmpge
					if ((int) opstack[--stackpos] <= (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA3:
					//if_icmpgt
					if ((int) opstack[--stackpos] < (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA4:
					//if_icmple
					if ((int) opstack[--stackpos] >= (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA5:
					//if_acmpeq
					if ((int) opstack[--stackpos] == (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA6:
					//if_acmpeq
					if ((int) opstack[--stackpos] != (int) opstack[--stackpos])
						pc += params[code[pc + 1]];
					else
						pc += 2;
					continue;
				case (byte) 0xA7:
					//goto
					pc += params[code[pc + 1]];
					continue;
				case (byte) 0xA8:
					//jsr
					opstack[stackpos++] = pc+2;
					pc += params[code[pc + 1]];
					continue;
				case (byte) 0xA9:
					//ret
					pc = (int) lv[code[pc + 1]];
					continue;
				case (byte) 0xAA:
					//tableswitch
					i1 = (int) opstack[--stackpos];
					int index=code[pc+1];
					int default_offset = params[index++];
					int low = params[index++];
					int high = params[index++];
					if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
						pc += default_offset;
						// branch to default
					} else {
						i2 = index + (i1 - low);
						pc += params[i2];
					}
					continue;
				case (byte) 0xAB:
					//lookupswitch
					i1 = (int) opstack[--stackpos];
					int cindex=code[pc+1];
					default_offset = params[cindex++];
					i2 = params[cindex++];
					for (i3 = 0; i3 < i2; i3++) {
						int key = params[cindex++];

						if (key == i1) {
							int value = params[cindex++];
							pc += value;
							continue mainLoop;
						}
						cindex++;
					}
					pc += default_offset;
					continue;
				case (byte) 0xAC:
					//ireturn
					i1 = (int) opstack[--stackpos];
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					//System.out.println("ireturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					opstack[stackpos++] = i1;
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xAD:
					//lreturn
					stackpos--;
					l1= (long) opstack[--stackpos];
					//i2 = (int) opstack[--stackpos];
					//i1 = (int) opstack[--stackpos];
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					//System.out.println("lreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					continue;
				case (byte) 0xAE:
					//freturn
					o1 = opstack[--stackpos];
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					//System.out.println("freturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					opstack[stackpos++] = o1;
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xAF:
					//dreturn
					stackpos--;
					l1 = (long) opstack[--stackpos];
					//i1 = (int) opstack[--stackpos];
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					//System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					opstack[stackpos++] = l1;
					opstack[stackpos++] = null;
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB0:
					//areturn
					i1 = (int) opstack[--stackpos];
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					//System.out.println("areturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					opstack[stackpos++] = i1;
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}

					code = (short[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB1:
					//return
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = cF.getMyClass();
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					//System.out.println("return: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB2:
					//getstatic
					// v=cC.staticfields.get(((code[i + 1] << 8) | code[i + 2]));
					cF.stack=opstack;
					cF.stackpos=stackpos;
					cC.getStatic(params[code[pc+1]], cF);
					opstack=cF.stack;
					stackpos=cF.stackpos;
					pc += 2;
					continue;
				case (byte) 0xB3:
					//putstatic
					o1 = opstack[--stackpos];
					fc = cC.getConstant(params[code[pc+1]]);
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					switch (ftype.str) {
						case "J":
							o1 = opstack[--stackpos];
							//obj =
							//		((Integer) opstack[--stackpos]).longValue() << 32 |
							//				((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							break;
						case "D":
							o1 = opstack[--stackpos];
							//l1 = ((Integer) opstack[--stackpos]).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							//obj = Double.longBitsToDouble(l1);

							//throw new RuntimeException("putfield error");
							break;
					}
					// int addrr=(code[i+1] << 8) | code[i+2];
					cC.putStatic(fname.str, o1);
					pc += 2;
					continue;
				case (byte) 0xB4:
					//getfield
					oref = (int) opstack[--stackpos];
					o1 = Heap.get(oref);
					fc = cC.getConstant(params[code[pc+1]]);
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					o1 = ((ClassInstance) o1).getField(fname.str);
					switch (ftype.str) {
						case "J":
							opstack[stackpos++] =((long) o1);
							opstack[stackpos++] =null;//(int)(((Long) obj)>>32);
							break;
						case "D":
							opstack[stackpos++] =((long) o1);
							opstack[stackpos++] =null;//(int)(((Long) obj)>>32);
							//throw new RuntimeException("getfield error");
							break;
						default:
							opstack[stackpos++] = o1;
							break;
					}
					pc += 2;
					continue;
				case (byte) 0xB5:
					//putfield
					o1 = opstack[--stackpos];
					fc = cC.getConstant(params[code[pc+1]]);
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					switch (ftype.str) {
						case "J":
							o1 = opstack[--stackpos];
							//obj = ((Integer) obj).longValue() << 32 | ((Integer) (opstack[--stackpos])).longValue() & 0xFFFFFFFFL;
							break;
						case "D":
							o1 = opstack[--stackpos];
							//obj = ((Integer) opstack[--stackpos]).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							break;
					}
					Object o = opstack[--stackpos];
					oref = (int) o;
					c = (ClassInstance) Heap.get(oref);
					if (c == null)
						throw new NullPointerException("field " + fname.str + " reference: " + oref );

					c.putField(fname.str, o1);
					pc += 2;
					continue;
				case (byte) 0xB6:
					//invokevirtual
					// int objref=(int)opstack[--stackpos];
					m = cC.getMethod(params[code[pc + 1]]);
					pc += 2;
					cF.pc = pc;
					cF.stack=opstack;
					cF.stackpos=stackpos;
					if(m instanceof NativeMethod){
						Object[] _lv=new Object[m.getParamsArrayLength()];
							for (i1 = 0; i1 < _lv.length; i1++)
								_lv[i1] = opstack[--stackpos];
						cF.stackpos=stackpos;
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, _lv,thread))
							return 1;

						pc = cF.pc;
						params=cF._params;
						code = (short[]) cF.code;
						lv = cF.lv;
						opstack = cF.stack;
						stackpos=cF.stackpos;
						continue ;
					}
					//System.out.println(s+" invokevirtual: " + m.getMyClass().getName() + "." + m.getSignature());

					//Frame nf = cF.newRefFrame(m);
					Frame nf = cF.newRefFrame((Method)m);
					oref = (int) nf.lv[0];
					//if (m.isAbstract()) 
					String mname = m.getSignature();
					cl = ((Instance) Heap.get(oref)).getMyClass();
					m = cl.getMethod(mname);
					if (m.isNative()) {
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
							cF.stackpos=stackpos;
							return cF.code_type;
						}
						pc = cF.pc;
						params=cF._params;
						code = (short[]) cF.code;
						lv = cF.lv;
						opstack = cF.stack;
						stackpos=cF.stackpos;

						continue;
					}
					if (m == null)
						throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
					nf.initLocals((Method) m);
					nf.setMethod((Method) m);

					//nf.code = m.getCode();
					cF = nf;
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;

					cC = m.getMyClass();
					stack.push(cF);
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					//cF.code = m.getCode();
					continue;
				case (byte) 0xB7:
					//invokespecial

					m = cC.getMethod(params[code[pc+1]]);

					pc += 2;
					cF.pc = pc;
					cF.stack=opstack;
					cF.stackpos=stackpos;
					//System.out.println("invokespecial: " + m.getMyClass().getName() + "." + m.getSignature());

					nf = cF.newRefFrame((Method) m);
					Heap.objectCreated((int) nf.lv[0]);
					if (m.isNative()) {
						// m.invoke(cF.stack, this);
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
							cF.stackpos=stackpos;
							return cF.code_type;
						}
						pc = cF.pc;
						params=cF._params;
						code = (short[]) cF.code;
						lv = cF.lv;
						opstack = cF.stack;
						stackpos=cF.stackpos;
						continue;
					}
					cF = nf;
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;

					cC = m.getMyClass();
					stack.push(cF);
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					continue;
				case (byte) 0xB8:
					//invokestatic
					m = cC.getMethod(params[code[pc+1]]);
					pc += 2;
					cF.pc = pc;
					cF.stack=opstack;
					cF.stackpos=stackpos;

					//System.out.println("invokestatic: " + m.getMyClass().getName() + "." + m.getSignature());

					nf = cF.newFrame((Method) m);
					if (m.isNative()) {
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
							return cF.code_type;
						}
						pc = cF.pc;
						code = (short[]) cF.code;
						params=cF._params;
						lv = cF.lv;
						opstack = cF.stack;
						stackpos=cF.stackpos;
						continue;
					}
					cF = nf;
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					cC = m.getMyClass();
					stack.push(cF);
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					continue;
				case (byte) 0xB9:
					//invokeinterface
					m = cC.getMethod(params[code[pc+1]]);
					//System.out.println("invokeinterface: " + m.getMyClass().getName() + "." + m.getSignature());

					byte count = (byte)code[pc + 2];
					byte zero = (byte)code[pc + 3];
					pc += 4;
					cF.pc = pc;
					cF.stack=opstack;
					cF.stackpos=stackpos;
					//if(count>1) {
					nf = new Frame((Method) m);

					Object[] mlv = new Object[count];
					for (int ix = 0; ix < count; ix++) {
						//nf.getLv().put(p - ix - 1, opstack[--stackpos]);
						mlv[count - ix - 1] = opstack[--stackpos];
					}

					//}
					oref = (int) mlv[0];

					ClassInstance ci = (ClassInstance) Heap.get(oref);
					AClass mc = ci.getMyClass();
					m = mc.getMethod(m.getSignature());


					if (m.isNative()) {
						//m.invoke(cF.stack, this);
						throw new RuntimeException("check.that");
						//continue;
					}
					nf.lv = new Object[((Method)m).getMaxLocals()];
					nf.lvpt = new int[((Method)m).getMaxLocals()];

					for (int j = 0; j < mlv.length; j++) {
						if(j>0)
							if(m.args.get(j-1)=='L')
								nf.lvpt[j]= (int) mlv[j];
						nf.lv[j] = mlv[j];
					}

					nf.setMethod((Method) m);
					//nf = cF.newRefFrame(m);
					//oref = (int) lv[0];
					if (m.isAbstract()) {
						m = ((Instance) Heap.get(oref)).getMyClass().getMethod(m.getSignature());
						throw new RuntimeException("check.that");
					}
					/*
					nf.code = m.getCode();
					nf.code_type=m.getCodeType();
					nf._params=m.getParams();
					*/
					cF.stackpos=stackpos;
					cF = nf;
					pc = cF.pc;
					params=cF._params;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;

					cC = m.getMyClass();
					stack.push(cF);
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					continue;
				case (byte) 0xBA:
					//invokedynamic
					throw new RuntimeException("invokedynamic not implemented");
				case (byte) 0xBB:
					//new
					c = new ClassInstance(cC.getClass(params[code[pc+1]]));
					// cC.getClass(i(code[pc + 1], code[pc + 2]));
					int a = Heap.add(c);
					Heap.newObject(a);
					opstack[stackpos++] = a;
					pc += 2;
					continue;
				case (byte) 0xBC:
					//newarray
					//i1=size
					i1 = (int) opstack[--stackpos];
					array = new ArrayInstance(i1, (byte)code[pc + 1]);
					aref = Heap.add(array);
					opstack[stackpos++] = aref;
					pc += 2;
					continue;
				case (byte) 0xBD:
					//anewarray
					i1 = (int) opstack[--stackpos];
					array = new ArrayInstance(i1, ArrayInstance.T_OBJECT);
					aref = Heap.add(array);
					opstack[stackpos++] = aref;
					pc += 1;
					continue;
				case (byte) 0xBE:
					//arraylength
					aref = (int) opstack[--stackpos];
					array = (ArrayInstance) Heap.get(aref);
					opstack[stackpos++] = array.getLength();
					pc += 1;
					continue;
				case (byte) 0xBF:
					//athrow
					oref = (int) opstack[--stackpos];
					cF.stackpos=stackpos;
					throwException(pc,oref);
					cF=stack.peek();
					if(cF.code_type!= Method.OPTIMIZED_SHORT_CODE) {
						cF.stackpos=stackpos;
						return cF.code_type;
					}
					code = (short[]) cF.code;
					lv = cF.lv;
					opstack = cF.stack;
					stackpos=cF.stackpos;
					pc=cF.pc;
					continue;
				case (byte) 0xC0:
					//checkcast
					fc = cC.getConstant(params[code[pc+1]]);
					fc = cC.getConstant(fc.index1);
					if (!fc.str.startsWith("[")) {
						AClass cl2 = Class.getClass(fc.str);
						i1 = (int) opstack[stackpos-1];
						o1 = Heap.get(i1);

						if (o1 instanceof ClassInstance) {
							cl = ((ClassInstance) o1).getMyClass();
							if (!cl.isInstance(cl2)) {
								//ClassCastException
								throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
							}
						}
					}
					pc += 2;
					continue;
				case (byte) 0xC1:
					//instanceof
					i1 = (int) opstack[--stackpos];
					if (i1 == 0) {
						opstack[stackpos++] = 0;
						pc += 2;
						continue;
					}
					o1= Heap.get(i1);
					i1 = params[code[pc+1]];
					AClass cl2 = cC.getClass(i1);
					cl = ((ClassInstance) o1).getMyClass();
					if (cl.isInstance(cl2))
					opstack[stackpos++] = 1;
					else
					opstack[stackpos++] = 0;
					pc += 2;
					continue;
				case (byte) 0xC2:
					//monitorenter
					oref = (int) opstack[--stackpos];
					Instance i=(Instance)Heap.get(oref);
					if(i.monitorenter(thread))
					{
						thread._wait();
						return 1;
					}
					pc += 1;
					continue;
				case (byte) 0xC3:
					//monitorexit
					oref = (int) opstack[--stackpos];
					Instance _i=(Instance)Heap.get(oref);
					MyThread next=_i.monitorexit(thread);
					if(next!=null)
						JVM.notify(next);
					pc += 1;
					continue;

				case (byte) 0xC4:
					//wide
					pc += 1;
					/*
					switch (code[pc]) {
						case (byte) 0x15:
							//iload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
					opstack[stackpos++] = -1;
							pc += 3;
							continue;
						case (byte) 0x16:
							//lload
							i1 = ii(code[pc + 1], code[pc + 2]);
							cF.push(lv[i1 + 1]);
					opstack[stackpos++] = -1;
							cF.push(lv[i1]);
					opstack[stackpos++] = -1;
							pc += 3;
							continue;
						case (byte) 0x17:
							//fload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
					opstack[stackpos++] = -1;
							pc += 3;
							continue;
						case (byte) 0x18:
							//dload
							i1 = ii(code[pc + 1], code[pc + 2]);
							cF.push(lv[i1 + 1]);
					opstack[stackpos++] = -1;
							cF.push(lv[i1]);
					opstack[stackpos++] = -1;
							pc += 3;
							continue;
						case (byte) 0x19:
							//aload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
					opstack[stackpos++] = -1;
							pc += 3;
							continue;
						case (byte) 0x36:
							//istore
							lv[ii(code[pc + 1], code[pc + 2])] = opstack[--stackpos];
							pc += 3;
							continue;
						case (byte) 0x37:
							//lstore
							i1 = ii(code[pc + 1], code[pc + 2]);
							lv[i1] = opstack[--stackpos];
							lv[i1 + 1] = opstack[--stackpos];
							pc += 3;
							continue;
						case (byte) 0x38:
							//fstore
							lv[ii(code[pc + 1], code[pc + 2])] = opstack[--stackpos];
							pc += 3;
							continue;
						case (byte) 0x39:
							//dstore
							i1 = ii(code[pc + 1], code[pc + 2]);
							lv[i1] = opstack[--stackpos];
							lv[i1 + 1] = opstack[--stackpos];
							pc += 3;
							continue;
						case (byte) 0x3A:
							//astore
							oref = (int) opstack[--stackpos];
							lv[ii(code[pc + 1], code[pc + 2])] = oref;
							cF.lvpt[ii(code[pc + 1], code[pc + 2])] = oref;
							pc += 3;
							continue;
						case (byte) 0x84:
							//iinc
							i1 = (int) lv[ii(code[pc + 1], code[pc + 2])];
							lv[(int) code[pc + 1]] = i1 + ii(code[pc + 3], code[pc + 4]);
							pc += 5;
							continue;

					}
					*/
					throw new RuntimeException("wide not implememted");
					//continue;
				case (byte) 0xC5:
					//multianewarray
					i1 = params[code[pc+1]];
					//dimensions
					i2 = code[pc + 2];
					int[] sizes=new int[i2];
					for(i3=0;i3<i2;i3++) {
						sizes[i3]= (int) opstack[--stackpos];
					}
					ArrayInstance ai=new ArrayInstance(i2,0,sizes,ArrayInstance.T_OBJECT);
					aref=Heap.add(ai);
					opstack[stackpos++]=aref;
					pc += 3;
					//throw new RuntimeException("not implememted");
					continue;
				case (byte) 0xC6:
					//ifnull
					if ((int) opstack[--stackpos] == 0)
						pc += params[code[pc+1]];
					else
						pc += 2;
					continue;
				case (byte) 0xC7:
					//ifnonnull
					if ((int) opstack[--stackpos] != 0)
						pc += params[code[pc+1]];
					else
						pc += 2;
					continue;
				case (byte) 0xC8:
					//goto
					pc += params[code[pc+1]];
					continue;
				case (byte) 0xC9:
					//jsr_w
					opstack[stackpos++] = pc+1;
					pc += params[code[pc+1]];
					continue;
				default:
					pc += 1;
					throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
					// cs+= byteToHex(code[i])+"\n";
			}
		} while (steps-->0);
		cF.pc = pc;
		cF.stackpos=stackpos;
		return 0;
	}


	//private static long _2i2l(int i1, int i2) {
	//	return ((long) i2) << 32 | ((long )(i1)) & 0xFFFFFFFFL;
	//}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(stack.peek().toString());
		return sb.toString();
	}

	public Frame getCurrentFrame() {
		return stack.peek();
	}

	public FrameStack getStack() {
		return stack;
	}
}
