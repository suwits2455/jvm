package com.neoexpert.jvm.executor;

import com.neoexpert.jvm.*;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.adapter.*;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.instance.ArrayInstance;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.instance.Instance;
import com.neoexpert.jvm.method.Method;
import com.neoexpert.jvm.natives.Natives;

import java.util.Iterator;
import java.util.Set;

public class RawByteCodeExecutor extends ByteCodeExecutor{

	private DebuggerAdapter classa;


	public RawByteCodeExecutor(MyThread thread) {
		super(thread);
	}


	public void run() {
		steps=Integer.MAX_VALUE;
		step();
	}

	public static int s = 0;

	//The Holy Grail
	@Override
	public int step() {
		if (stack.isEmpty())
		{
			//thread.destroy();
			return 1;
		}
		//local variables are better for performance
		//current Frame
		Frame cF = stack.peek();
		//program counter
		int pc = cF.pc;
		ClassInstance c;
		int i1, i2, i3, i4, aref, oref;
		long l1, l2;
		float f1, f2;
		double d1, d2;
		AClass cl;
		AMethod m;
		ArrayInstance array;
		Object obj;
		Constant fc;
		Constant fname;
		Constant ftype;

		byte[] code = (byte[]) cF.code;
		Object[] lv = cF.lv;
		//byte op = code[pc];

		//if (cF.isThrowing())
		//op = (byte) 0xBF;

		int steps = this.steps;
		mainLoop:
		do {
			//System.out.println(stack.get(0).stackpos);
			/*
			   StringBuilder sb = new StringBuilder();
			   sb.append(s);
			   sb.append(": ");
			   CodeAttribute.parseOp(pc, code, sb);
			   System.out.println(sb);
			//*/

			switch (code[pc]) {
				case (byte) 0x00:
					//nop
					pc += 1;
					continue;
				case (byte) 0x01:
					//aconst_null
					cF.push(0);
					pc += 1;
					continue;
				case (byte) 0x02:
					//iconst_m1
					cF.push(-1);
					pc += 1;
					continue;
				case (byte) 0x03:
					//iconst_0
					cF.push(0);
					pc += 1;
					continue;
				case (byte) 0x04:
					//iconst_1
					cF.push(1);

					pc += 1;
					continue;
				case (byte) 0x05:
					//iconst_2
					cF.push(2);
					pc += 1;
					continue;
				case (byte) 0x06:
					//iconst_3
					cF.push(3);
					pc += 1;
					continue;
				case (byte) 0x07:
					//iconst_4
					cF.push(4);
					pc += 1;
					continue;
				case (byte) 0x08:
					//iconst_5
					cF.push(5);
					pc += 1;
					continue;
				case (byte) 0x09:
					//lconst_0
					cF.push(0);
					cF.push(0);
					pc += 1;
					continue;
				case (byte) 0x0A:
					//lconst_1
					cF.push(0);
					cF.push(1);
					pc += 1;
					continue;
				case (byte) 0x0B:
					//fconst_0
					cF.push(0f);
					pc += 1;
					continue;
				case (byte) 0x0C:
					//fconst_1
					cF.push(1f);
					pc += 1;
					continue;
				case (byte) 0x0D:
					//fconst_2
					cF.push(2f);
					pc += 1;
					continue;
				case (byte) 0x0E:
					//dconst_0
					l1 = Double.doubleToRawLongBits(0.0);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x0F:
					//dconst_1
					l1 = Double.doubleToRawLongBits(1.0);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x10:
					//bipush
					cF.push((int) code[pc + 1]);
					pc += 2;
					continue;
				case (byte) 0x11:
					//sipush
					cF.push((ii(code[pc + 1], code[pc + 2])));
					pc += 3;
					continue;
				case (byte) 0x12:
					//ldc
					cC.ldc((int) code[pc + 1] & 0xFF, cF);
					pc += 2;
					continue;
				case (byte) 0x13:
					//ldc_w
					cC.ldc(ii(code[pc + 1], code[pc + 2]), cF);
					pc += 3;
					continue;
				case (byte) 0x14:
					//ldc2_w
					cC.ldc(ii(code[pc + 1], code[pc + 2]), cF);
					pc += 3;
					continue;
				case (byte) 0x15:
					//iload
					cF.push(lv[(int) code[pc + 1]]);
					pc += 2;
					continue;
				case (byte) 0x16:
					//lload
					cF.push(lv[(int) code[pc + 1] + 1]);
					cF.push(lv[(int) code[pc + 1]]);
					pc += 2;
					continue;
				case (byte) 0x17:
					//fload
					cF.push(lv[(int) code[pc + 1]]);
					pc += 2;
					continue;
				case (byte) 0x18:
					//dload
					cF.push(lv[(int) code[pc + 1] + 1]);
					cF.push(lv[(int) code[pc + 1]]);
					pc += 2;
					continue;
				case (byte) 0x19:
					//aload
					cF.push(lv[(int) code[pc + 1]]);
					pc += 2;
					continue;
				case (byte) 0x1A:
					//iload_0
					cF.push(lv[0]);
					pc += 1;
					continue;
				case (byte) 0x1B:
					//iload_1
					cF.push(lv[1]);
					pc += 1;
					continue;
				case (byte) 0x1C:
					//iload_2
					cF.push(lv[2]);
					pc += 1;
					continue;
				case (byte) 0x1D:
					//iload_3";
					cF.push(lv[3]);
					pc += 1;
					continue;
				case (byte) 0x1E:
					//lload_0;
					cF.push(lv[1]);
					cF.push(lv[0]);
					pc += 1;
					continue;
				case (byte) 0x1F:
					//lload_1
					cF.push(lv[2]);
					cF.push(lv[1]);
					pc += 1;
					continue;
				case (byte) 0x20:
					//lload_2
					cF.push(lv[3]);
					cF.push(lv[2]);
					pc += 1;
					continue;
				case (byte) 0x21:
					//lload_3
					cF.push(lv[4]);
					cF.push(lv[3]);
					pc += 1;
					continue;
				case (byte) 0x22:
					//fload_0
					cF.push(lv[0]);
					pc += 1;
					continue;
				case (byte) 0x23:
					//fload_1
					cF.push(lv[1]);
					pc += 1;
					continue;
				case (byte) 0x24:
					//fload_2
					cF.push(lv[2]);
					pc += 1;
					continue;
				case (byte) 0x25:
					//fload_3;
					cF.push(lv[3]);
					pc += 1;
					continue;
				case (byte) 0x26:
					//dload_0;
					cF.push(lv[1]);
					cF.push(lv[0]);
					pc += 1;
					continue;
				case (byte) 0x27:
					//dload_1
					cF.push(lv[2]);
					cF.push(lv[1]);
					pc += 1;
					continue;
				case (byte) 0x28:
					//dload_2
					cF.push(lv[3]);
					cF.push(lv[2]);
					pc += 1;
					continue;
				case (byte) 0x29:
					//dload_3
					cF.push(lv[4]);
					cF.push(lv[3]);
					pc += 1;
					continue;
				case (byte) 0x2A:
					//aload_0
					cF.push(lv[0]);
					pc += 1;
					continue;
				case (byte) 0x2B:
					//aload_1
					cF.push(lv[1]);
					pc += 1;
					continue;
				case (byte) 0x2C:
					//aload_2
					cF.push(lv[2]);
					pc += 1;
					continue;
				case (byte) 0x2D:
					//aload_3
					cF.push(lv[3]);
					pc += 1;
					continue;
				case (byte) 0x2E:
					//iaload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push(((int[]) (array.a))[i1]);
					pc += 1;
					continue;
				case (byte) 0x2F:
					//laload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					l1 = ((long[]) array.a)[i1];
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x30:
					//faload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push(((float[]) array.a)[i1]);
					pc += 1;
					continue;
				case (byte) 0x31:
					//daload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push(((double[]) array.a)[i1]);
					cF.push(42);
					cF.push(42);
					pc += 1;
					throw new RuntimeException("double arrays not implemented");
					//continue;
				case (byte) 0x32:
					//aaload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push(((int[]) array.a)[i1]);
					pc += 1;
					continue;
				case (byte) 0x33:
					//baload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push((int) ((byte[]) array.a)[i1]);
					pc += 1;
					continue;
				case (byte) 0x34:
					//caload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push((int) ((char[]) array.a)[i1]);
					pc += 1;
					continue;
				case (byte) 0x35:
					//saload
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push((int) ((short[]) array.a)[i1]);
					pc += 1;
					continue;
				case (byte) 0x36:
					//istore
					lv[(int) code[pc + 1]] = (int) cF.pop();
					pc += 2;
					continue;
				case (byte) 0x37:
					//lstore
					lv[(int) code[pc + 1]] = cF.pop();
					lv[(int) code[pc + 1] + 1] = cF.pop();
					pc += 2;
					continue;
				case (byte) 0x38:
					//fstore
					lv[(int) code[pc + 1]] = cF.pop();
					pc += 2;
					continue;
				case (byte) 0x39:
					//dstore
					lv[(int) code[pc + 1]] = cF.pop();
					lv[(int) code[pc + 1] + 1] = cF.pop();
					pc += 2;
					continue;
				case (byte) 0x3A:
					//astore
					oref = (int) cF.pop();
					lv[code[pc + 1]] = oref;
					cF.lvpt[code[pc + 1]] = oref;
					pc += 2;
					continue;
				case (byte) 0x3B:
					//istore_0
					lv[0] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x3C:
					//istore_1
					lv[1] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x3D:
					//istore_2
					lv[2] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x3E:
					//istore_3
					lv[3] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x3F:
					//lstore_0
					lv[0] = cF.pop();
					lv[1] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x40:
					//lstore_1
					lv[1] = cF.pop();
					lv[2] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x41:
					//store_2
					lv[2] = cF.pop();
					lv[3] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x42:
					//lstore_3
					lv[3] = cF.pop();
					lv[4] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x43:
					//fstore_0
					lv[0] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x44:
					//fstore_1
					lv[1] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x45:
					//fstore_2
					lv[2] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x46:
					//fstore_3"
					lv[3] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x47:
					//dstore_0
					lv[0] = cF.pop();
					lv[1] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x48:
					//dstore_1
					lv[1] = cF.pop();
					lv[2] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x49:
					//dstore_2
					lv[2] = cF.pop();
					lv[3] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x4A:
					//dstore_3
					lv[3] = cF.pop();
					lv[4] = cF.pop();
					pc += 1;
					continue;
				case (byte) 0x4B:
					//astore_0
					lv[0] = cF.pop();
					cF.lvpt[0]= (int) lv[0];
					pc += 1;
					continue;
				case (byte) 0x4C:
					//astore_1
					lv[1] = cF.pop();
					cF.lvpt[1]= (int) lv[1];
					pc += 1;
					continue;
				case (byte) 0x4D:
					//astore_2
					lv[2] = cF.pop();
					cF.lvpt[2] = (int) lv[2];
					pc += 1;
					continue;
				case (byte) 0x4E:
					//astore_3
					lv[3] = cF.pop();
					cF.lvpt[3]= (int) lv[3];
					pc += 1;
					continue;
				case (byte) 0x4F:
					//iastore
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((int[]) array.a)[i2] = i1;
					pc += 1;
					continue;
				case (byte) 0x50:
					//lastore
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					i3 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((long[]) array.a)[i3] = l1;
					pc += 1;
					continue;
				case (byte) 0x51:
					//fastore
					f1 = (float) cF.pop();
					i1 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((float[]) array.a)[i1] = f1;
					pc += 1;
					continue;
				case (byte) 0x52:
					//dastore
					aref = (int) cF.pop();
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					i3 = (int) cF.pop();
					pc += 1;
					throw new RuntimeException("implement me!");
					//continue;
				case (byte) 0x53:
					//aastore
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((int[]) array.a)[i2] = i1;
					pc += 1;
					continue;
				case (byte) 0x54:
					//bastore
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((byte[]) array.a)[i2] = (byte) i1;
					pc += 1;
					//throw new RuntimeException("implement me!");
					continue;
				case (byte) 0x55:
					//castore
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((char[]) array.a)[i2] = (char) i1;
					pc += 1;
					continue;
				case (byte) 0x56:
					//sastore
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					((short[]) array.a)[i2] = (short) i1;
					pc += 1;
					continue;
				case (byte) 0x57:
					//pop
					cF.pop();
					pc += 1;
					continue;
				case (byte) 0x58:
					//pop2
					cF.pop();
					cF.pop();
					pc += 1;
					continue;
				case (byte) 0x59:
					//dup
					cF.push(cF.peek());
					pc += 1;
					continue;
				case (byte) 0x5A:
					//dup_x1
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i1);
					cF.push(i2);
					cF.push(i1);
					pc += 1;
					continue;
				case (byte) 0x5B:
					//dup_x2
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					i3 = (int) cF.pop();
					cF.push(i1);
					cF.push(i3);
					cF.push(i2);
					cF.push(i1);
					pc += 1;
					continue;
				case (byte) 0x5C:
					//dup2
					i2 = (int) cF.pop();
					i1 = (int) cF.pop();
					cF.push(i1);
					cF.push(i2);
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x5D:
					//dup2_x1
					i2 = (int) cF.pop();
					i1 = (int) cF.pop();
					i3 = (int) cF.pop();
					cF.push(i1);
					cF.push(i2);
					cF.push(i3);
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					throw new RuntimeException("check that and contniue");
					//continue;
				case (byte) 0x5E:
					//dup2_x2
					i2 = (int) cF.pop();
					i1 = (int) cF.pop();
					i4 = (int) cF.pop();
					i3 = (int) cF.pop();
					cF.push(i1);
					cF.push(i2);
					cF.push(i3);
					cF.push(i4);
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					throw new RuntimeException("check that and continue");
					//continue;
				case (byte) 0x5F:
					//swap
					cF.push(cF.get(0));
					pc += 1;
					continue;
				case (byte) 0x60:
					//iadd
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i1 + i2);
					pc += 1;
					continue;
				case (byte) 0x61:
					//ladd
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 + l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x62:
					//fadd
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					cF.push(f1 + f2);
					pc += 1;
					continue;
				case (byte) 0x63:
					//dadd
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 + d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x64:
					//isub
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 - i1);
					pc += 1;
					continue;
				case (byte) 0x65:
					//lsub
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 - l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x66:
					//fsub
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					cF.push(f2 - f1);
					pc += 1;
					continue;
				case (byte) 0x67:
					//dsub
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 - d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x68:
					//imul
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 * i1);
					pc += 1;
					continue;
				case (byte) 0x69:
					//lmul
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l1 * l2;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x6A:
					//fmul
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					cF.push(f2 * f1);
					pc += 1;
					continue;
				case (byte) 0x6B:
					//dmul
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 * d1;
					l1 = Double.doubleToRawLongBits(d1);
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x6C:
					//idiv
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 / i1);
					pc += 1;
					continue;
				case (byte) 0x6D:
					//ldiv
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 / l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x6E:
					//fdiv
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					cF.push(f2 / f1);
					pc += 1;
					continue;
				case (byte) 0x6F:
					//ddiv
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 / d1;
					l1 = Double.doubleToRawLongBits(d1);
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x70:
					//irem
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 % i1);
					pc += 1;
					continue;
				case (byte) 0x71:
					//lrem
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 % l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x72:
					//frem
					f1 = (int) cF.pop();
					f2 = (int) cF.pop();
					cF.push(f2 % f1);
					pc += 1;
					continue;
				case (byte) 0x73:
					//drem
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					d1 = d2 % d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x74:
					//ineg
					cF.push(-(int) cF.pop());
					pc += 1;
					continue;
				case (byte) 0x75:
					//lneg
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = -l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x76:
					//fneg
					cF.push(-(float) cF.pop());
					pc += 1;
					continue;
				case (byte) 0x77:
					//dneg
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d1 = -d1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x78:
					//ishl
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 << i1);
					pc += 1;
					continue;
				case (byte) 0x79:
					//lshl
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					i3 = (int) cF.pop();
					l1 = _2i2l(i2, i3) << i1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x7A:
					//ishr
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 >> i1);
					pc += 1;
					continue;
				case (byte) 0x7B:
					//lshr
					i1 = (int) cF.pop();
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 >> i1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x7C:
					//iushr
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 >>> i1);
					pc += 1;
					continue;
				case (byte) 0x7D:
					//lushr
					i1 = (int) cF.pop();
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 >>> i1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x7E:
					//iand
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 & i1);
					pc += 1;
					continue;
				case (byte) 0x7F:
					//land
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 & l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x80:
					//ior
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 | i1);
					pc += 1;
					continue;
				case (byte) 0x81:
					//lor
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 | l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x82:
					//ixor
					i1 = (int) cF.pop();
					i2 = (int) cF.pop();
					cF.push(i2 ^ i1);
					pc += 1;
					continue;
				case (byte) 0x83:
					//lxor
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					l1 = l2 ^ l1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x84:
					//iinc
					i1 = (int) lv[(int) code[pc + 1]];
					lv[(int) code[pc + 1]] = i1 + code[pc + 2];
					pc += 3;
					continue;
				case (byte) 0x85:
					//i2l
					i1= (int) cF.pop();
					l1=i1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x86:
					//i2f
					cF.push(Float.valueOf((int) cF.pop()));
					pc += 1;
					continue;
				case (byte) 0x87:
					//i2d
					i1 = (int) cF.pop();
					l1 = Double.doubleToRawLongBits(i1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x88:
					//l2i
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					cF.push((int) l1);
					pc += 1;
					continue;
				case (byte) 0x89:
					//l2f
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					cF.push((float) l1);
					pc += 1;
					continue;
				case (byte) 0x8A:
					//l2d
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = (double) l1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x8B:
					//f2i
					cF.push(((Float) cF.pop()).intValue());
					pc += 1;
					continue;
				case (byte) 0x8C:
					//f2l
					f1 = (float) cF.pop();
					l1 = (long) f1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x8D:
					//f2d
					f1 = (float) cF.pop();
					d1 = (double) f1;
					l1 = Double.doubleToRawLongBits(d1);
					i1 = (int) (l1 >> 32);
					i2 = (int) l1;
					cF.push(i1);
					cF.push(i2);
					pc += 1;
					continue;
				case (byte) 0x8E:
					//d2i
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					cF.push((int) d1);
					pc += 1;
					continue;
				case (byte) 0x8F:
					//d2l
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					l1 = (long) d1;
					cF.push(l2i1(l1));
					cF.push(l2i2(l1));
					pc += 1;
					continue;
				case (byte) 0x90:
					//d2f
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					cF.push((float) d1);
					pc += 1;
					continue;
				case (byte) 0x91:
					//i2b
					cF.push((int) ((Integer) cF.pop()).byteValue());
					pc += 1;
					continue;
				case (byte) 0x92:
					//i2c
					cF.push(((Integer) cF.pop()).intValue());
					pc += 1;
					continue;
				case (byte) 0x93:
					//i2s
					cF.push((int) ((Integer) cF.pop()).shortValue());
					pc += 1;
					continue;
				case (byte) 0x94:
					//lcmp
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					if (l2 == l1) {
						cF.push(0);
					} else if (l2 > l1) {
						cF.push(1);
					} else
						cF.push(-1);
					pc += 1;
					continue;
				case (byte) 0x95:
					//fcmpl
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					if (f2 == f1) {
						cF.push(0);
					} else if (f2 > f1) {
						cF.push(1);
					} else if (f2 < f1)
						cF.push(-1);
					else
						cF.push(-1);
					pc += 1;
					continue;
				case (byte) 0x96:
					//fcmpg
					f1 = (float) cF.pop();
					f2 = (float) cF.pop();
					if (f2 == f1) {
						cF.push(0);
					} else if (f2 > f1) {
						cF.push(1);
					} else if (f2 < f1)
						cF.push(-1);
					else
						cF.push(1);
					pc += 1;
					continue;
				case (byte) 0x97:
					//dcmpl
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					if (d2 == d1) {
						cF.push(0);
					} else if (d2 > d1) {
						cF.push(1);
					} else if (d2 < d1)
						cF.push(-1);
					else
						cF.push(-1);
					pc += 1;
					continue;
				case (byte) 0x98:
					//dcmpg
					l1 = _2i2l((int) cF.pop(), (int) cF.pop());
					l2 = _2i2l((int) cF.pop(), (int) cF.pop());
					d1 = Double.longBitsToDouble(l1);
					d2 = Double.longBitsToDouble(l2);
					if (d2 == d1) {
						cF.push(0);
					} else if (d2 > d1) {
						cF.push(1);
					} else if (d2 < d1)
						cF.push(-1);
					else
						cF.push(1);
					pc += 1;
					continue;
				case (byte) 0x99:
					//ifeq
					if ((int) cF.pop() == 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9A:
					//ifne
					if ((int) cF.pop() != 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9B:
					//iflt
					if ((int) cF.pop() < 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9C:
					//ifge
					if ((int) cF.pop() >= 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9D:
					//ifgt
					if ((int) cF.pop() > 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9E:
					//ifle
					if ((int) cF.pop() <= 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0x9F:
					//if_icmpeq
					if ((int) cF.pop() == (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA0:
					//if_icmpne
					if ((int) cF.pop() != (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA1:
					//if_icmplt
					if ((int) cF.pop() > (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA2:
					//if_icmpge
					if ((int) cF.pop() <= (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA3:
					//if_icmpgt
					if ((int) cF.pop() < (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA4:
					//if_icmple
					if ((int) cF.pop() >= (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA5:
					//if_acmpeq
					if ((int) cF.pop() == (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA6:
					//if_acmpeq
					if ((int) cF.pop() != (int) cF.pop())
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xA7:
					//goto
					pc += i(code[pc + 1], code[pc + 2]);
					continue;
				case (byte) 0xA8:
					//jsr
					cF.push(pc + 3);
					pc += i(code[pc + 1], code[pc + 2]);
					continue;
				case (byte) 0xA9:
					//ret
					pc = (int) lv[code[pc + 1]];
					continue;
				case (byte) 0xAA:
					//tableswitch
					i1 = (int) cF.pop();
					int pc2 = pc + (4 - pc % 4);
					int default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					int low = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					int high = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					if (i1 < low || i1 > high) {  // if its less than <low> or greater than <high>,
						pc += default_offset;
						// branch to default
					} else {
						pc2 += (i1 - low) * 4;
						pc += i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					}
					continue;
				case (byte) 0xAB:
					//lookupswitch
					i1 = (int) cF.pop();
					pc2 = pc + (4 - pc % 4);
					default_offset = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					i2 = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
					pc2 += 4;
					for (i3 = 0; i3 < i2; i3++) {
						int key = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);

						if (key == i1) {
							pc2 += 4;
							int value = i(code[pc2 + 0], code[pc2 + 1], code[pc2 + 2], code[pc2 + 3]);
							pc += value;
							continue mainLoop;
						}
						pc2 += 8;
					}
					pc += default_offset;
					continue;
				case (byte) 0xAC:
					//ireturn
					i1 = (int) cF.pop();
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					//System.out.println("ireturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					if (classa != null)
						classa.setFrame(cF);
					cF.push(i1);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xAD:
					//lreturn
					i2 = (int) cF.pop();
					i1 = (int) cF.pop();
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					if (classa != null)
						classa.setFrame(cF);
					cF.push(i1);
					cF.push(i2);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					//System.out.println("lreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					continue;
				case (byte) 0xAE:
					//freturn
					obj = cF.pop();
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					//System.out.println("freturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					if (classa != null)
						classa.setFrame(cF);
					cF.push(obj);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xAF:
					//dreturn
					i2 = (int) cF.pop();
					i1 = (int) cF.pop();
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					//System.out.println("dreturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					if (classa != null)
						classa.setFrame(cF);
					cF.push(i1);
					cF.push(i2);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB0:
					//areturn
					i1 = (int) cF.pop();
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					//System.out.println("areturn: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					if (classa != null)
						classa.setFrame(cF);
					cF.push(i1);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB1:
					//_return
					pc += 1;
					last_frame = stack.pop();
					if (stack.isEmpty()) {
						last_frame.pc=pc;
						//thread.destroy();
						return 1;
					}
					cF = stack.peek();
					pc = cF.pc;
					lv = cF.lv;
					cC = cF.getMyClass();
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					//System.out.println("return: " + cF.getMethod().getMyClass().getName() + "." + cF.getMethod().getSignature());
					if (classa != null)
						classa.setFrame(cF);
					// this.code=c.getMethod(11).attrs[0].code;
					continue;
				case (byte) 0xB2:
					//getstatic
					// v=cC.staticfields.get(((code[i + 1] << 8) | code[i + 2]));
					cC.getStatic(ii(code[pc + 1], code[pc + 2]), cF);
					pc += 3;
					continue;
				case (byte) 0xB3:
					//putstatic
					obj = cF.pop();
					fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					switch (ftype.str) {
						case "J":
							obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							break;
						case "D":
							l1 = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							obj = Double.longBitsToDouble(l1);

							//throw new RuntimeException("putfield error");
							break;
					}
					// int addrr=(code[i+1] << 8) | code[i+2];
					cC.putStatic(fname.str, obj);
					pc += 3;
					continue;
				case (byte) 0xB4:
					//getfield
					oref = (int) cF.pop();
					obj = Heap.get(oref);
					fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					if (obj != null)
						obj = ((ClassInstance) obj).getField(fname.str);
					else obj = 0;
					switch (ftype.str) {
						case "J":
							cF.push(l2i2((Long) obj));
							cF.push(l2i1((Long) obj));
							break;
						case "D":
							cF.push(l2i2((Long) obj));
							cF.push(l2i1((Long) obj));
							//throw new RuntimeException("getfield error");
							break;
						default:
							cF.push(obj);
							break;
					}
					pc += 3;
					continue;
				case (byte) 0xB5:
					//putfield
					obj = cF.pop();
					fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
					fc = cC.getConstant(fc.index2);
					fname = cC.getConstant(fc.index1);
					ftype = cC.getConstant(fc.index2);
					switch (ftype.str) {
						case "J":
							obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							break;
						case "D":
							obj = ((Integer) cF.pop()).longValue() << 32 | ((Integer) (obj)).longValue() & 0xFFFFFFFFL;
							break;
					}
					Object o = cF.pop();
					oref = (int) o;
					c = (ClassInstance) Heap.get(oref);
					if (c == null)
						throw new NullPointerException("field " + fname.str + " reference: " + oref );

					c.putField(fname.str, obj);
					pc += 3;
					continue;
				case (byte) 0xB6:
					//invokevirtual
					// int objref=(int)cF.pop();

					m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
					pc += 3;
					cF.pc = pc;
					//System.out.println("invokevirtual: " + m.getMyClass().getName() + "." + m.getSignature());
					//Frame nf = cF.newRefFrame(m);
					Frame nf = cF.newRefFrame((Method)m);
					oref = (int) nf.lv[0];
					//if (m.isAbstract()) 
					String mname = m.getSignature();
					cl=null;

					cl = ((Instance) Heap.get(oref)).getMyClass();
					m = cl.getMethod(mname);
					if (m.isNative()) {
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
								return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.BYTE_CODE)
							return cF.code_type;
						pc = cF.pc;
						code = (byte[]) cF.code;
						lv = cF.lv;

						continue;
					}
					if (m == null)
						throw new RuntimeException("method " + cl.getName() + "." + mname + " not found");
					nf.initLocals((Method) m);
					nf.setMethod((Method) m);

					//nf.code = m.getCode();
					cF = nf;
					pc = cF.pc;
					lv = cF.lv;

					cC = m.getMyClass();
					if (classa != null)
						classa.setFrame(cF);
					stack.push(cF);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					//cF.code = m.getCode();
					continue;
				case (byte) 0xB7:
					//invokespecial

					m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
					pc += 3;
					cF.pc = pc;
					//System.out.println("invokespecial: " + m.getMyClass().getName() + "." + m.getSignature());

					nf = cF.newRefFrame((Method) m);
					Heap.objectCreated((int) nf.lv[0]);
					if (m.isNative()) {
						// m.invoke(cF.stack, this);
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.BYTE_CODE)
							return cF.code_type;
						pc = cF.pc;
						code = (byte[]) cF.code;
						lv = cF.lv;
						continue;
					}
					cF = nf;
					pc = cF.pc;
					lv = cF.lv;
					cC = m.getMyClass();
					if (classa != null)
						classa.setFrame(cF);
					stack.push(cF);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					continue;
				case (byte) 0xB8:
					//invokestatic
					m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
					pc += 3;
					cF.pc = pc;

					//System.out.println("invokestatic: " + m.getMyClass().getName() + "." + m.getSignature());

					nf = cF.newFrame((Method) m);
					if (m.isNative()) {
						if(Natives.invoke(m.getMyClass().getName(), m.getSignature(), cF, nf.lv,thread))
							return 1;
						cF = stack.peek();
						if(cF.code_type!= Method.BYTE_CODE)
							return cF.code_type;
						pc = cF.pc;
						code = (byte[]) cF.code;
						lv = cF.lv;
						continue;
					}
					cF = nf;
					pc = cF.pc;
					code = (byte[]) cF.code;
					lv = cF.lv;
					cC = m.getMyClass();
					if (classa != null)
						classa.setFrame(cF);
					stack.push(cF);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					continue;
				case (byte) 0xB9:
					//invokeinterface
					m = cC.getMethod(ii(code[pc + 1], code[pc + 2]));
					//System.out.println("invokeinterface: " + m.getMyClass().getName() + "." + m.getSignature());

					byte count = code[pc + 3];
					byte zero = code[pc + 4];
					pc += 5;
					cF.pc = pc;
					//if(count>1) {
					nf = new Frame((Method) m);

					Object[] mlv = new Object[count];
					for (int ix = 0; ix < count; ix++) {
						//nf.getLv().put(p - ix - 1, cF.pop());
						mlv[count - ix - 1] = cF.pop();
					}

					//}
					oref = (int) mlv[0];

					ClassInstance ci = (ClassInstance) Heap.get(oref);
					AClass mc = ci.getMyClass();
					m = mc.getMethod(m.getSignature());


					if (m.isNative()) {
						//m.invoke(cF.stack, this);
						throw new RuntimeException("check.that");
						//continue;
					}
					nf.lv = new Object[((Method)m).getMaxLocals()];
					nf.lvpt = new int[((Method)m).getMaxLocals()];

					for (int j = 0; j < mlv.length; j++) {
						if(j>0)
							if(m.args.get(j-1)=='L')
								nf.lvpt[j]= (int) mlv[j];
						nf.lv[j] = mlv[j];
					}

					nf.setMethod((Method) m);
					//nf = cF.newRefFrame(m);
					//oref = (int) lv[0];
					if (m.isAbstract()) {
						m = ((Instance) Heap.get(oref)).getMyClass().getMethod(m.getSignature());
					}
					//nf.code = m.getCode();
					cF = nf;
					pc = cF.pc;
					lv = cF.lv;

					cC = m.getMyClass();
					if (classa != null)
						classa.setFrame(cF);
					stack.push(cF);
					if(cF.code_type!= Method.BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					continue;
				case (byte) 0xBA:
					//invokedynamic
					throw new RuntimeException("invokedynamic not implemented");
				case (byte) 0xBB:
					//new

					c = new ClassInstance(cC.getClass(i(code[pc + 1], code[pc + 2])));
					// cC.getClass(i(code[pc + 1], code[pc + 2]));
					int a = Heap.add(c);
					Heap.newObject(a);
					cF.push(a);
					pc += 3;
					continue;
				case (byte) 0xBC:
					//newarray
					//i1=size
					i1 = (int) cF.pop();
					array = new ArrayInstance(i1, code[pc + 1]);
					aref = Heap.add(array);
					cF.push(aref);
					pc += 2;
					continue;
				case (byte) 0xBD:
					//anewarray
					i1 = (int) cF.pop();
					array = new ArrayInstance(i1, ArrayInstance.T_OBJECT);
					aref = Heap.add(array);
					cF.push(aref);
					pc += 3;
					continue;
				case (byte) 0xBE:
					//arraylength
					aref = (int) cF.pop();
					array = (ArrayInstance) Heap.get(aref);
					cF.push(array.getLength());
					pc += 1;
					continue;
				case (byte) 0xBF:
					//athrow
					oref = (int) cF.pop();
					throwException(pc,oref);
					cF=stack.peek();
					if(cF.code_type!= Method.OPTIMIZED_BYTE_CODE)
						return cF.code_type;
					code = (byte[]) cF.code;
					lv = cF.lv;

					continue;
				case (byte) 0xC0:
					//checkcast
					fc = cC.getConstant(ii(code[pc + 1], code[pc + 2]));
					fc = cC.getConstant(fc.index1);
					if (!fc.str.startsWith("[")) {
						AClass cl2 = Class.getClass(fc.str);
						i1 = (int) cF.peek();
						obj = Heap.get(i1);

						if (obj instanceof ClassInstance) {
							cl = ((ClassInstance) obj).getMyClass();
							if (!cl.isInstance(cl2)) {
								//ClassCastException
								throw new RuntimeException("CLASSCASTEXCEPTION: " + cl.getName() + " is not " + cl2.getName());
							}
						}
					}
					pc += 3;
					continue;
				case (byte) 0xC1:
					//instanceof
					i1 = (int) cF.pop();
					if (i1 == 0) {
						cF.push(0);
						pc += 3;
						continue;
					}
					obj = Heap.get(i1);
					i1 = ii(code[pc + 1], code[pc + 2]);
					AClass cl2 = cC.getClass(i1);
					cl = ((ClassInstance) obj).getMyClass();
					if (cl.isInstance(cl2))
						cF.push(1);
					else
						cF.push(0);
					pc += 3;
					continue;
				case (byte) 0xC2:
					//monitorenter
					oref = (int) cF.pop();
					Instance i=(Instance)Heap.get(oref);
					if(i.monitorenter(thread))
					{
						thread._wait();
						return 1;
					}
					pc += 1;
					continue;
				case (byte) 0xC3:
					//monitorexit
					oref = (int) cF.pop();
					Instance _i=(Instance)Heap.get(oref);
					MyThread next=_i.monitorexit(thread);
					if(next!=null)
						JVM.notify(next);
					pc += 1;
					continue;

				case (byte) 0xC4:
					//wide
					pc += 1;
					switch (code[pc]) {
						case (byte) 0x15:
							//iload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
							pc += 3;
							continue;
						case (byte) 0x16:
							//lload
							i1 = ii(code[pc + 1], code[pc + 2]);
							cF.push(lv[i1 + 1]);
							cF.push(lv[i1]);
							pc += 3;
							continue;
						case (byte) 0x17:
							//fload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
							pc += 3;
							continue;
						case (byte) 0x18:
							//dload
							i1 = ii(code[pc + 1], code[pc + 2]);
							cF.push(lv[i1 + 1]);
							cF.push(lv[i1]);
							pc += 3;
							continue;
						case (byte) 0x19:
							//aload
							cF.push(lv[ii(code[pc + 1], code[pc + 2])]);
							pc += 3;
							continue;
						case (byte) 0x36:
							//istore
							lv[ii(code[pc + 1], code[pc + 2])] = cF.pop();
							pc += 3;
							continue;
						case (byte) 0x37:
							//lstore
							i1 = ii(code[pc + 1], code[pc + 2]);
							lv[i1] = cF.pop();
							lv[i1 + 1] = cF.pop();
							pc += 3;
							continue;
						case (byte) 0x38:
							//fstore
							lv[ii(code[pc + 1], code[pc + 2])] = cF.pop();
							pc += 3;
							continue;
						case (byte) 0x39:
							//dstore
							i1 = ii(code[pc + 1], code[pc + 2]);
							lv[i1] = cF.pop();
							lv[i1 + 1] = cF.pop();
							pc += 3;
							continue;
						case (byte) 0x3A:
							//astore
							oref = (int) cF.pop();
							lv[ii(code[pc + 1], code[pc + 2])] = oref;
							cF.lvpt[ii(code[pc + 1], code[pc + 2])] = oref;
							pc += 3;
							continue;
						case (byte) 0x84:
							//iinc
							i1 = (int) lv[ii(code[pc + 1], code[pc + 2])];
							lv[(int) code[pc + 1]] = i1 + ii(code[pc + 3], code[pc + 4]);
							pc += 5;
							continue;

					}
					throw new RuntimeException("wide not implememted");
					//continue;
				case (byte) 0xC5:
					//multianewarray
					i1 = i(code[pc + 1], code[pc + 2]);
					//dimensions
					i2 = code[pc + 1];
					pc += 4;
					throw new RuntimeException("not implememted");
					//continue;
				case (byte) 0xC6:
					//ifnull
					if ((int) cF.pop() == 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xC7:
					//ifnonnull
					if ((int) cF.pop() != 0)
						pc += i(code[pc + 1], code[pc + 2]);
					else
						pc += 3;
					continue;
				case (byte) 0xC8:
					//goto
					pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
					continue;
				case (byte) 0xC9:
					//jsr_w
					cF.push(pc + 5);
					pc += i(code[pc + 1], code[pc + 2], code[pc + 3], code[pc + 4]);
					continue;
				default:
					pc += 1;
					throw new UnsupportedOperationException("opcode:" + Integer.toHexString(code[pc]));
					// cs+= byteToHex(code[i])+"\n";
			}
		} while (steps-->0);
		cF.pc = pc;
		return 0;
	}

	private int l2i1(long l) {
		return (int) (l >> 32);
	}

	private int l2i2(long l) {
		return (int) l;
	}


	private static short i(byte b1, byte b2) {
		return (short) (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static long _2i2l(int i1, int i2) {
		return ((Integer) i2).longValue() << 32 | ((Integer) (i1)).longValue() & 0xFFFFFFFFL;
	}

	private static int ii(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	private static int i(byte b1, byte b2, byte b3, byte b4) {
		return ((0xFF & b1) << 24) | ((0xFF & b2) << 16) |
				((0xFF & b3) << 8) | (0xFF & b4);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(stack.peek().toString());
		return sb.toString();
	}

	public Frame getCurrentFrame() {
		return stack.peek();
	}

	public FrameStack getStack() {
		return stack;
	}
}
