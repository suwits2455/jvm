package com.neoexpert.jvm.executor;
import com.neoexpert.jvm.*;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm.adapter.DebuggerAdapter;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.method.MException;

public abstract class ByteCodeExecutor{
	/*
	Object o=1;
	is equivalent to
	Object o = new Integer(1);
	to prevent the JVM to invoke everytime the Integer constructor
	we define here some Integer constants and use them instead.
	 */
	protected static Integer iconst_m1=-1;
	protected static Integer iconst_0=0;
	protected static Integer iconst_1=1;
	protected static Integer iconst_2=2;
	protected static Integer iconst_3=3;
	protected static Integer iconst_4=4;
	protected static Integer iconst_5=5;
	protected static Long lconst_0=0L;
	protected static Long lconst_1=1L;
	public Frame last_frame;
	public MyThread thread;
	private DebuggerAdapter da;

	public ByteCodeExecutor(MyThread thread){
		this.thread=thread;
	}
	public FrameStack stack = new FrameStack();
	public int psteps=128;
	public int steps;
	// current class
	public AClass cC;
	public abstract int step();
	public int prun() {
		this.steps=psteps;
		return step();
	}

	public ByteCodeExecutor cloneme(ByteCodeExecutor cE){
		cC=cE.cC;
		stack=cE.stack;
		psteps=cE.psteps;
		thread=cE.thread;
		steps=cE.steps;
		return this;
	}

	public void throwException(int pc, int oref){
		Frame cF=stack.peek();
		ClassInstance c = (ClassInstance) Heap.get(oref);
		while (!stack.isEmpty()) {
			MException e = cF.m.checkException(pc);
			AClass cl=null;
			boolean _catch=false;

			if(e!=null) {
				cl = cC.getClass(e.catch_type);
				_catch=c.getMyClass().equals(cl);
			}
			if (!_catch)
			{
				last_frame = stack.pop();
				if (stack.isEmpty()) {
					thread.destroy();
					System.out.println("Exception printing still not implemented");
					int outoref= (int) Class.getClass("java/lang/System").getStatic("out");
					AClass tcl = Class.getClass("java/lang/Throwable");
					new MyThread(tcl,"stacktrace_printer").setMethodToRun("printStackTrace(Ljava/io/PrintStream;)V",oref,outoref).run();
					throw new RuntimeException(c.getMyClass().getName());
				}
				cF = stack.peek();
				pc = cF.pc;
				cC = cF.getMyClass();

			} else {
				cF.pc = e.handler_pc;
				cF.clearOperandStack();
				cF.push(oref);
				break;
			}
		}
	}
	public void setDebuggerAdapter(DebuggerAdapter da) {
		this.da = da;
		da.setFrameStack(stack);
	}
}
