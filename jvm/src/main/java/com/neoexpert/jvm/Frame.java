package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.instance.Instance;
import com.neoexpert.jvm.method.Method;

import java.util.ArrayList;
import java.util.Set;

public class Frame {
	public boolean brk;

	public int[] lvpt;

	public Object get(int pos) {
		return stack[pos];
	}

	public Object peek() {
		return stack[stackpos - 1];
	}
	public void clearOperandStack(){
		stackpos=0;
	}

	public Method getMethod() {
		return m;
	}


	public int getCurrentLine() {
		return m.getCurrentLine(pc);
	}


	@Override
	public String toString() {
		return m.getMyClass().getName() + "." + m.getSignature() + ": " + getCurrentLine();
	}

	public int pc = 0;
	public Object code;
	public int[] _params;
	public int code_type;
	//local variables
	//private HashMap<Integer,Object> lv=new HashMap<>();
	//op-stack
	public int stackpos = 0;
	public Object[] stack;

	public void push(Object v) {
		stack[stackpos++] = v;
	}

	public Object pop() {
		return stack[--stackpos];
	}

	public Object[] lv;

	public Method m;
	private Class c;

	public AClass getMyClass() {
		return m.getMyClass();
	}

	public Frame(int lvsize, int stacksize) {
		lv = new Object[lvsize];
		lvpt = new int[lvsize];
		stack = new Object[stacksize];
	}

	public Frame(Method m) {
		this.m = m;
		stack = new Object[m.getMaxStack()];
		//this.consts=cp;
		code = m.getCode();
		_params=m.getParams();
		code_type=m.getCodeType();
		if (code != null) {
			lv = new Object[m.getMaxLocals()];
			lvpt = new int[m.getMaxLocals()];
		}
	}


	public Frame newRefFrame(Method m) {
		Frame f = new Frame(m);
		f.code = m.getCode();
		f._params=m.getParams();
		f.code_type=m.getCodeType();
		int p = m.getParamsArrayLength() + 1;

		if (f.lv == null) {
			f.lv = new Object[p];
			f.lvpt = new int[p];
		}
		ArrayList<Character> args = m.args;
		for (int i = p - 1; i >= 0; i--) {
			Character pt = 'L';
			if (i >= 1)
				pt = args.get(i - 1);
			if (pt == 'J' || pt == 'D') {
				f.lv[i - 1] = pop();
				f.lv[i] = pop();
				i--;
			} else
				f.lv[i] = pop();
			if(pt=='L'||pt=='[')
				f.lvpt[i]= (int) f.lv[i];
			//lv.add(0,pop());

		}



		return f;
	}

	public Frame newFrame(Method m) {
		Frame f = new Frame(m);
		f.code = m.getCode();
		f._params=m.getParams();
		f.code_type=m.getCodeType();
		int p = m.getParamsArrayLength();
		ArrayList<Character> args = m.args;
		if (f.lv == null) {
			f.lv = new Object[p];
			f.lvpt = new int[p];
		}
		for (int i = p - 1; i >= 0; i--) {
			Character pt = args.get(i);
			if (pt == 'J' || pt == 'D') {
				f.lv[i - 1] = pop();
				f.lv[i] = pop();
				i--;
			} else
				f.lv[i] = pop();
			if(pt=='L'||pt=='[')
				f.lvpt[i]= (int) f.lv[i];
			//lv.add(0,pop());

		}

		return f;
	}

/*
	public Frame newFrame(AMethod m)
	{
		Frame f=new Frame(m);
		int p=m.getParamsCount();
		int pos=0;
		for(int i=0;i<p;i++)
		{
			f.getLv().put(p-i-1,stack.pop());
			switch(m.args.get(i)){
				case "J":
				case "D":
					lv.add(0,stack.pop());
					//f.getLv().put(p-i-2,stack.pop());
			}
		}
		return f;
	}

 */

	public void setMethod(Method m) {
		this.m = m;
		code = m.getCode();
		_params=m.getParams();
		code_type=m.getCodeType();
		stack = new Object[m.getMaxStack()];
	}

	public void initLocals(Method m) {
		this.m = m;
		code = m.getCode();
		_params=m.getParams();
		code_type=m.getCodeType();
		Object[] oldlv = lv;
		int[] oldlvpt = lvpt;
		lv = new Object[m.getMaxLocals()];
		lvpt = new int[m.getMaxLocals()];
		for (int i = 0; i < Math.min(lv.length, oldlv.length); i++) {
			lv[i] = oldlv[i];
			lvpt[i]=oldlvpt[i];
		}
	}

	public void checkReference(Set<Integer> refs) {
		if(lvpt==null)
			return;
		for(int a:lvpt)
			if(a!=0){
				refs.add(a);
					((Instance) Heap.get(a)).getMyClass().getStaticReferecnes(refs);
			}
	}

	//int l0, l1,l2,l3;

}
