package com.neoexpert.jvm.constants;


import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Constant {
	public static final byte CUtf8 = 1;
	public static final byte CInteger = 3;
	public static final byte CFloat = 4;
	public static final byte CLong = 5;
	public static final byte CDouble = 6;
	public static final byte CClass = 7;
	public static final byte CString = 8;
	public static final byte CFieldRef = 9;
	public static final byte CMethodRef = 10;
	public static final byte CInterfaceMethodRef = 11;
	public static final byte CNameAndType = 12;
	public static final byte CMethodHandle = 15;
	public static final byte CMethodType = 16;
	public static final byte CInvokeDynamic = 18;


	public Object value;

	public String str;

	private byte tag;
	public int index1 = -1, index2 = -1;

	private short id;

	public short getId() {
		return id;
	}

	public Constant(short id, ByteBuffer buf) {
		this.id = id;
		tag = buf.get();
		//String s;
		switch (tag) {
			default:
				throw new RuntimeException("unknown pool item type " + buf.get(buf.position() - 1));
			case CUtf8:
				str = decodeString(buf);
				//log(str);
				return;
			case CClass:
			case CString:
			case CMethodType:
				//s = "%s ref=%d%n";
				index1 = buf.getChar();
				break;
			case CFieldRef:
			case CMethodRef:
			case CInterfaceMethodRef:
			case CNameAndType:
				//s = "%s ref1=%d, ref2=%d%n";
				index1 = buf.getChar();
				index2 = buf.getChar();
				break;
			case CInteger:
				value = buf.getInt();
				//s = "%s value=" + ivalue + "%n";
				break;
			case CFloat:
				value = buf.getFloat();
				//s = "%s value=" + fvalue + "%n";
				break;
			case CDouble:
				value = buf.getDouble();
				//s = "%s value=" + dvalue + "%n";
				break;
			case CLong:
				value = buf.getLong();
				//s = "%s value=" + lvalue + "%n";
				break;
			case CMethodHandle:
				//s = "%s kind=%d, ref=%d%n";
				index1 = buf.get();
				index2 = buf.getChar();
				break;
			case CInvokeDynamic:
				//s = "%s bootstrap_method_attr_index=%d, ref=%d%n";
				index1 = buf.getChar();
				index2 = buf.getChar();
				break;
		}

		//log(String.format(s, FMT[tag],index1,index2));
	}

	public void write(DataOutput raf) throws IOException {
		raf.write(tag);
		switch (tag) {
			default:
				//log("unknown pool item type " + buf.get(buf.position() - 1));
				return;
			case CUtf8:
				byte[] bytes = str.getBytes();
				raf.writeShort(bytes.length);
				raf.write(bytes);
				return;
			case CClass:
			case CString:
			case CMethodType:
				raf.writeChar(index1);
				//s = "%s ref=%d%n"; index1 = buf.getChar();
				break;
			case CFieldRef:
			case CMethodRef:
			case CInterfaceMethodRef:
			case CNameAndType:
				//s = "%s ref1=%d, ref2=%d%n";
				//index1 = buf.getChar(); index2 = buf.getChar();
				raf.writeChar(index1);
				raf.writeChar(index2);
				break;
			case CInteger:
				//ivalue=buf.getInt();
				//s = "%s value=" + ivalue + "%n";
				raf.writeInt((Integer) value);
				break;
			case CFloat:
				//fvalue=buf.getFloat();
				//s = "%s value=" + fvalue + "%n";
				raf.writeFloat((Float) value);
				break;
			case CDouble:
				//dvalue=buf.getDouble();
				//s = "%s value=" + dvalue + "%n"; 
				raf.writeDouble((Double) value);
				break;
			case CLong:
				//lvalue=buf.getLong();
				//s = "%s value=" + lvalue + "%n";
				raf.writeLong((Long) value);
				break;
			case CMethodHandle:
				//s = "%s kind=%d, ref=%d%n"; index1 = buf.get(); index2 = buf.getChar();
				raf.writeChar(index1);
				raf.writeChar(index2);
				break;
			case CInvokeDynamic:
				//s = "%s bootstrap_method_attr_index=%d, ref=%d%n";
				//index1 = buf.getChar(); index2 = buf.getChar();
				raf.writeChar(index1);
				raf.writeChar(index2);
				break;
		}
	}

	private static String[] FMT = {
			null, "Utf8", null, "Integer", "Float", "Long", "Double", "Class",
			"String", "Field", "Method", "Interface Method", "Name and Type",
			null, null, "MethodHandle", "MethodType", null, "InvokeDynamic"
	};

	public byte getTag() {
		return tag;
	}

	//byte[] d;
	private String decodeString(ByteBuffer buf) {
		int size = buf.getChar(), oldLimit = buf.limit();
		buf.limit(buf.position() + size);
		StringBuilder sb = new StringBuilder(size + (size >> 1) + 16);
		while (buf.hasRemaining()) {
			byte b = buf.get();
			if (b > 0) sb.append((char) b);
			else {
				int b2 = buf.get();
				if ((b & 0xf0) != 0xe0)
					sb.append((char) ((b & 0x1F) << 6 | b2 & 0x3F));
				else {
					int b3 = buf.get();
					sb.append((char) ((b & 0x0F) << 12 | (b2 & 0x3F) << 6 | b3 & 0x3F));
				}
			}
		}
		buf.limit(oldLimit);
		return (sb.toString());
	}

	@Override
	public String toString() {
		StringBuilder sb=new StringBuilder();
		switch (tag) {
			default:
				sb.append("unknown pool item type ");
				break;
			case CUtf8:
				sb.append("CUtf8: ");
				sb.append(str);
				break;
			case CClass:
				sb.append("CClass: ");
				sb.append(index1);
				break;
			case CString:
				sb.append("CString: ");
				sb.append(index1);
				break;
			case CMethodType:
				sb.append("CMethodType: ");
				sb.append(index1);
				break;
			case CFieldRef:
				sb.append("CFieldRef: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
			case CMethodRef:
				sb.append("CMethodRef: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
			case CInterfaceMethodRef:
				sb.append("CInterfaceMethodRef: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
			case CNameAndType:
				sb.append("CNameAndType: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
			case CInteger:
				sb.append("CInteger: ");
				sb.append(value);
				break;
			case CFloat:
				sb.append("CFloat: ");
				sb.append(value);
				break;
			case CDouble:
				sb.append("CDouble: ");
				sb.append(value);
				break;
			case CLong:
				sb.append("CLong: ");
				sb.append(value);
				break;
			case CMethodHandle:
				sb.append("CMethodHandle: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
			case CInvokeDynamic:
				sb.append("CInvokeDynamic: ");
				sb.append(index1);
				sb.append(",");
				sb.append(index2);
				break;
		}
		return sb.toString();
	}

}
