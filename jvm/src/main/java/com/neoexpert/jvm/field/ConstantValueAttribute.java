package com.neoexpert.jvm.field;

import com.neoexpert.jvm.Attribute;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm.constants.Constant;

import java.nio.ByteBuffer;

/*
ConstantValue_attribute {
        u2 attribute_name_index;
        u4 attribute_length;
        u2 constantvalue_index;
        }

 */
public class ConstantValueAttribute extends Attribute {
	public final Constant value;

	public ConstantValueAttribute(Attribute a, Class c) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		int constand_value_index = buf.getChar();
		value = c.getConstant(constand_value_index);
		info=null;

	}
}
