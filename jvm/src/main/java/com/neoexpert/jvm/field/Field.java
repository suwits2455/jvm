package com.neoexpert.jvm.field;

import com.neoexpert.jvm.Attribute;
import com.neoexpert.jvm._class.Class;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AField;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Field extends AField {


	private ConstantValueAttribute constant_value;

	public Object getDefaultValue() {
		switch (desc) {
			case "I":
				if (constant_value != null)
					return constant_value.value.value;
				else return 0;
			case "J":
				if (constant_value != null)
					return constant_value.value.value;
				else return 0L;
			case "D":
				if (constant_value != null)
					return constant_value.value.value;
				else return 0.0;
			case "F":
				if (constant_value != null)
					return constant_value.value.value;
				else return 0.0f;
			case "Ljava/lang/String;":
				if (constant_value != null) {
					String str = getMyClass().getConstant(constant_value.value.index1).str;
					return NString.createInstance(str);
				} else return 0;
		}
		return 0;
	}

	@Override
	public String getDesc() {
		return desc;
	}


	@Override
	public String parseType() {
		String type = desc;

		if (type.charAt(0) == '[') {
			return parseType(type.substring(1) + "[]");
		}
		return parseType(type);

	}

	@Override
	public boolean isObject() {
		char type=desc.charAt(0);
		return type=='L'||type=='[';
	}

	private String parseType(String type) {
		if (type.length() == 1) {
			return parsePrimitiveType(type);
		}
		if (type.charAt(0) == 'L')
			return type.substring(1, type.indexOf(";"));

		return type;
	}

	private String parsePrimitiveType(String type) {
		switch (type) {
			case "I":
				return "int";
			case "Z":
				return "boolean";
			case "B":
				return "byte";
			case "C":
				return "char";
			case "D":
				return "double";
			case "F":
				return "float";
			case "J":
				return "long";
			case "S":
				return "short";
			default:
				throw new RuntimeException("unknown primitive: " + type);
		}
	}


	public String parseModifiers() {
		StringBuilder sb = new StringBuilder();

		if (isPublic())
			sb.append("public ");
		if (isPrivate())
			sb.append("private ");
		if (isProtected())
			sb.append("protected ");
		if (isStatic())
			sb.append("static ");
		if (isFinal())
			sb.append("final ");
		if (isVolatile())
			sb.append("volatile ");
		if (isTransient())
			sb.append("transient ");
		if (isSynthetic())
			sb.append("synthetic ");
		if (isEnum())
			sb.append("enum ");
		return sb.toString();
	}

	@Override
	public int getType() {
		if (desc.startsWith("L")) {
			String classname = desc.substring(1, desc.indexOf(";"));
			return Class.getClass(classname, false).getMyInstanceRef();
		}
		if (desc.startsWith("[")) {
			return Class.getArrayClass(desc).getMyInstanceRef();
		}
		if(desc.length()==1)
			return Class.getPrimitiveClass(desc);
		return 0;
	}


	private Class c;

	@Override
	public AClass getMyClass() {
		return c;
	}





	private short id;


	private char name_index;

	private char descriptor_index;

	private char attributes_count;

	//private ArrayList<Attribute> attrs;

	private String name;

	private String desc;

	//	field_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}
	private static int i(byte b1, byte b2) {
		return (((b1 & 0xff) << 8) | (b2 & 0xff));
	}

	public Field() {
	}

	public Field(ByteBuffer buf, Class c) {
		this.c = c;
		access_flags = buf.getChar();
		name_index = buf.getChar();

		name = c.getConstant(name_index).str;

		descriptor_index = buf.getChar();
		//int di=(int)descriptor_index;

		desc = c.getConstant(descriptor_index).str;


		attributes_count = buf.getChar();
		//attrs = new ArrayList<>(attributes_count);
		for (int ix = 0; ix < attributes_count; ix++) {
			Attribute a = new Attribute(buf);
			String name2 = c.getConstant(a.attribute_name_index).str;
			if (name2.equals("ConstantValue"))
				constant_value = new ConstantValueAttribute(a, c);

			//attrs.add(a);
		}
	}

	@Override
	public void write(DataOutput raf) throws IOException {
		raf.writeChar(access_flags);
		raf.writeChar(name_index);
		raf.writeChar(descriptor_index);

		//raf.writeChar(attrs.size());
		/*for (Attribute a : attrs) {

			a.write(raf);
		}*/
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getName() {
		return name;
	}

	public String getNameAndDesc() {
		return name + desc;
	}

	private static final int ACC_PUBLIC = 0x0001;//	Declared public; may be accessed from outside its package.
	private static final int ACC_PRIVATE = 0x0002;//	Declared private; usable only within the defining class.
	private static final int ACC_PROTECTED = 0x0004;//	Declared protected; may be accessed within subclasses.
	private static final int ACC_STATIC = 0x0008;//	Declared static.
	private static final int ACC_FINAL = 0x0010;//	Declared final; never directly assigned to after object construction (JLS §17.5).
	private static final int ACC_VOLATILE = 0x0040;//	Declared volatile; cannot be cached.
	private static final int ACC_TRANSIENT = 0x0080;//	Declared transient; not written or read by a persistent object manager.
	private static final int ACC_SYNTHETIC = 0x1000;//	Declared synthetic; not present in the source code.
	private static final int ACC_ENUM = 0x4000;//	Declared

	private boolean isPublic() {
		return (access_flags & ACC_PUBLIC) != 0;
	}

	private boolean isPrivate() {
		return (access_flags & ACC_PRIVATE) != 0;
	}

	private boolean isProtected() {
		return (access_flags & ACC_PROTECTED) != 0;
	}

	public boolean isStatic() {
		return (access_flags & ACC_STATIC) != 0;
	}

	public boolean isFinal() {
		return (access_flags & ACC_FINAL) != 0;
	}

	private boolean isVolatile() {
		return (access_flags & ACC_VOLATILE) != 0;
	}

	private boolean isTransient() {
		return (access_flags & ACC_TRANSIENT) != 0;
	}

	private boolean isSynthetic() {
		return (access_flags & ACC_SYNTHETIC) != 0;
	}

	private boolean isEnum() {
		return (access_flags & ACC_ENUM) != 0;
	}


	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(parseModifiers());
		s.append("\nField: ").append(name).append("\n");
		s.append("access_flags: ").append(access_flags).append("\n");
		s.append("name_index: ").append((int) name_index).append("\n");
		s.append("descriptor_index: ").append((int) descriptor_index).append("\n");
		s.append("attributes_count: ").append((int) attributes_count).append("\n");
		for (int ix = 0; ix < attributes_count; ix++) {
			//s.append("attribute ").append(ix).append("\n");
			//s.append(attrs.get(ix).toString());
		}
		return s + "\n";
	}

}
