package com.neoexpert.jvm.classpath;

public abstract class ClassPath {

	public abstract ClassSource getClassSource(String s);
}
