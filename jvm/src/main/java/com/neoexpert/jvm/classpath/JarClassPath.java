package com.neoexpert.jvm.classpath;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class JarClassPath extends ClassPath {
	private final ZipFile jar;
	private final URL url;
	public String prefix = "";

	public JarClassPath(File f, String prefix) {
		this(f);
		this.prefix = prefix;
	}

	public JarClassPath(File f) {
		try {
			this.url = f.toURI().toURL();
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}

		try {
			this.jar = new ZipFile(f);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public ClassSource getClassSource(String classname) {
		ZipEntry e = jar.getEntry(prefix + classname + ".class");
		if (e != null) {
			try {
				InputStream is = jar.getInputStream(e);
				return new ClassSource(is, url,e.getSize());
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}
		return null;
	}
}
