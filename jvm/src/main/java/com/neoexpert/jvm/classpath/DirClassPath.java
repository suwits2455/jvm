package com.neoexpert.jvm.classpath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;

public class DirClassPath extends ClassPath {
	private final File parent;

	public DirClassPath(File parent) {
		this.parent = parent;

	}

	@Override
	public ClassSource getClassSource(String s) {
		try {
			File f = new File(parent, s + ".class");
			if (!f.exists())
				return null;
			InputStream is = new FileInputStream(f);
			return new ClassSource(is, parent.toURI().toURL(),f.length());
		} catch (FileNotFoundException e) {
			return null;
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
}
