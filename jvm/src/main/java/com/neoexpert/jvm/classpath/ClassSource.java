package com.neoexpert.jvm.classpath;

import java.io.InputStream;
import java.net.URL;

public class ClassSource {
	private final InputStream is;
	private final URL url;
	private long size;

	public ClassSource(InputStream is, URL url,long size) {
		this.is = is;
		this.url = url;
		this.size=size;
	}

	public InputStream getInputStream() {
		return is;
	}

	public URL getURL() {
		return url;
	}
	public long getSize(){
		return size;
	}
}
