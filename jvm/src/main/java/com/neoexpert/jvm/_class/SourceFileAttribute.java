package com.neoexpert.jvm._class;

import com.neoexpert.jvm.Attribute;
import com.neoexpert.jvm._abstract.AClass;

import java.nio.ByteBuffer;

public class SourceFileAttribute extends Attribute {
	private final String sourcefile;

	protected SourceFileAttribute(Attribute a, AClass c) {
		super(a);
		ByteBuffer buf = ByteBuffer.wrap(info);
		int index = buf.getChar();
		sourcefile=
		c.getConstant(index).str;
	}

	public String getSourceFileName() {
		return sourcefile;
	}
}
