package com.neoexpert.jvm._class;


import com.neoexpert.jvm.*;
import com.neoexpert.jvm._abstract.AClass;
import com.neoexpert.jvm._abstract.AField;
import com.neoexpert.jvm._abstract.AMethod;
import com.neoexpert.jvm.classpath.ClassPath;
import com.neoexpert.jvm.classpath.ClassSource;
import com.neoexpert.jvm.classpath.DirClassPath;
import com.neoexpert.jvm.classpath.JarClassPath;
import com.neoexpert.jvm.constants.Constant;
import com.neoexpert.jvm.field.Field;
import com.neoexpert.jvm.instance.ClassInstance;
import com.neoexpert.jvm.method.Method;
import com.neoexpert.jvm.natives.java.lang.reflect.NArray;
import com.neoexpert.jvm.natives.java.lang.NString;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

public class Class extends AClass {
	private static final int HEAD = 0xcafebabe;
	private static final ArrayList<ClassPath> classpath = new ArrayList<>(4);
	private static final Map<String, NArray> array_classes = new HashMap<>();
	private static final Map<String, Integer> primitive_classes = new HashMap<>();
	private static final HashMap<String, AClass> classes = new HashMap<>();
	public static int getPrimitiveClass(String classname) {
		switch (classname) {
			case "I":
			case "Z":
			case "B":
			case "C":
			case "D":
			case "F":
			case "J":
			case "S":
				Integer oref = primitive_classes.get(classname);
				if (oref == null) {
					AClass cl = getClass("java/lang/Class");
					ClassInstance ci = new ClassInstance(cl);
					ci.cl = classname;
					oref = Heap.add(ci);
					primitive_classes.put(classname, oref);
				}
				return oref;
		}
		throw new RuntimeException("wrong primitive class");
	}

	public static AClass getArrayClass(String a) {
		NArray ac = array_classes.get(a);
		if (ac == null) {
			ac = new NArray(a.charAt(1));
			ClassInstance ci = new ClassInstance(ac);
			int oref = Heap.addPermanent(ci);
			ac.setMyInstenceRef(oref);
			ci.putField("name", a);
			ci.cl = a;
			array_classes.put(a, ac);
		}
		return ac;
	}

	public static void addToClasspath(String s) {
		classpath.add(new DirClassPath(new File(s)));
	}

	public static void addToClasspath(ClassPath cp) {
		classpath.add(cp);
	}
	public static AClass getClass(String classname) {
		return getClass(classname, true);
	}
	public static void reset() {
		classes.clear();
	}
	private static ArrayList<ClassPath> jars = new ArrayList<>();

	private static ClassPath jar;

	public static void setJar(File _jar) {
		jar = new JarClassPath(_jar);
	}

	public static void addJar(File jar) {
		if (jar.isDirectory()) {
			addToClasspath(jar.getAbsolutePath());
		} else
			jars.add(new JarClassPath(jar));
	}


	public static AClass getClass(String classname, boolean init) {
		AClass c = classes.get(classname);
		if (c != null) {
			if (init && !c.isInitialized())
				c.clinit();
			return c;
		}

		if(JVM.verbose)
			System.out.print("[  ] loading class: "+classname);
//		try
//		{
//			java.lang.Class jc=java.lang.Class.forName(classname.replaceAll("/", "."));
//			if(jc!=null){
//				c=new NativeClass(jc);
//				classes.put(classname,c);
//				return c;
//			}
//		}
//		catch (ClassNotFoundException e)
//		{
//			//throw new RuntimeException(e);
//		}

		for (ClassPath cp : classpath) {
			ClassSource e = cp.getClassSource(classname);
			if (e == null)
				continue;
			try {
				InputStream is = e.getInputStream();
				return new Class(is, init,(int)e.getSize())
						.setCodeSource(e.getURL());
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}

		if (jar != null) {
			ClassSource e = jar.getClassSource(classname);
			if (e != null) {
				try {
					InputStream is = e.getInputStream();
					return new Class(is, init,(int)e.getSize())
							.setCodeSource(e.getURL());
				} catch (Exception ex) {
					throw new RuntimeException(ex);
				}
			}
		}

		for (ClassPath jar : jars) {
			try {
				ClassSource e = jar.getClassSource(classname);
				if (e == null) {
					continue;
				}
				InputStream is = e.getInputStream();
				return new Class(is, init, (int)e.getSize())
						.setCodeSource(e.getURL());
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			}
		}

		if (classname.charAt(0) == '[') {
			char at = classname.charAt(1);
			switch (at) {
				case 'I':
				case 'Z':
				case 'B':
				case 'C':
				case 'D':
				case 'F':
				case 'J':
				case 'S':
				case 'L':
					return getArrayClass(classname);
			}
		}
		System.out.println(classname + " class not found");
		throw new RuntimeException(classname + " class not found");
	}



	private final String source_file;
	private URL code_source;
	private String name;


	public final ArrayList<AMethod> constructors;


	public final ArrayList<String> references=new ArrayList<>();
	private final ArrayList<String> static_references =new ArrayList<>(8);
	private final HashMap<String, Object> staticfields = new HashMap<>();
	private final HashSet<Integer> addrs = new HashSet<>();

	private final int constant_pool_count;

	private final ArrayList<Constant> constants;
	private final int interfaces_count;
	private final int[] interfaces;

	private final int fields_count;

	private final int methods_count;

	private final int super_class;

	private final int this_class;


	private boolean initialized;



	@Override
	public boolean isInterface() {
		return super.isInterface();
	}


	public void putStatic(String fname, Object obj) {
		staticfields.put(fname, obj);
	}

	@Override
	public void putStatic(int id, Object obj) {
		Constant c = getConstant(id);
		Constant classref = getConstant(c.index1);
		Constant cls = getConstant(classref.index1);
		if (c.index1 == this_class) {
			//nameandtyperef
			Constant ntref = getConstant(c.index2);
			Constant fname = getConstant(ntref.index1);
			Constant ftype = getConstant(ntref.index2);
			staticfields.put(fname.str, obj);


		} else {
			throw new RuntimeException();
		}
	}


	@Override
	public boolean isInitialized() {
		return initialized;
	}


	@Override
	public Iterator<Constant> constantsIterator() {
		return constants.iterator();
	}


	public void addConstant(Constant c) {
		constants.add(c);
	}

	public Constant getConstant(int i) {
		try {
			return constants.get(i - 1);
		} catch (IndexOutOfBoundsException e) {
			throw new RuntimeException("wrong index, class: " + name);
		}
	}

	@Override
	public boolean isInstance(Object obj) {
		if (obj instanceof ClassInstance)
			return isInstance(((ClassInstance) obj).getMyClass());

		return false;
	}

	@Override
	public boolean isInstance(AClass cl) {
		if (name.equals(cl.getName()))
			return true;
		for (int i : interfaces) {
			AClass iface = getClass(i);
			if (cl.getName().equals(iface.getName()))
				return true;
		}
		AClass scl = getSuperClass();
		if (scl != null)
			return scl.isInstance(cl);


		return false;
	}



	@Override
	public HashMap<String, Object> getStaticFields() {
		return staticfields;
	}


	//	ClassFile {
//		u4             magic;
//		u2             minor_version;
//		u2             major_version;
//		u2             constant_pool_count;
//		cp_info        constant_pool[constant_pool_count-1];
//		u2             access_flags;
//		u2             this_class;
//		u2             super_class;
//		u2             interfaces_count;
//		u2             interfaces[interfaces_count];
//		u2             fields_count;
//		field_info     fields[fields_count];
//		u2             methods_count;
//		method_info    methods[methods_count];
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}

	/*
	@Override
	public int getFieldId(String fname) {
		AField f = fields.get(fname);
		if (f == null)
			return getSuperClass().getFieldId(fname);
		return f.getId();
	}
	 */

	@Override
	public void ldc(int id, Frame cF) {
		Constant c = getConstant(id);
		Integer oref;
		switch (c.getTag()) {
			case Constant.CString:
				oref= (Integer) c.value;
				if(oref==null) {
					c = getConstant(c.index1);
					oref = NString.createInstance(c.str);
					c.value=oref;
				}
				cF.push(oref);
				return;
			case Constant.CFloat:
				cF.push(c.value);
				return;
			case Constant.CInteger:
				cF.push(c.value);
				return;
			case Constant.CClass:
				Constant cref = getConstant(c.index1);
				String cname = cref.str;
				if (cname.charAt(0) == '[') {
					oref = Class.getArrayClass(cname).getMyInstanceRef();
					cF.push(oref);
					return;
				}
				oref = getClass(cref.str, false).getMyInstanceRef();
				cF.push(oref);
				return;
			default:
				throw new RuntimeException("wrong constant type");
		}
	}

	@Override
	public void ldc2(int id, Frame cF) {
		Constant c = getConstant(id);
		Long l;
		switch (c.getTag()) {
			case Constant.CLong:
				l= (Long) c.value;
				cF.push(l);
				cF.push(null);
				return;
			case Constant.CDouble:
				l = Double.doubleToRawLongBits((Double) c.value);
				cF.push(l);
				cF.push(null);
				return;
			default:
				throw new RuntimeException("wrong constant type");
		}
	}


	public AMethod getMyMethod(String nameAndDesc) {
		return methodsbyname.get(nameAndDesc);
	}


	public AClass getClass(byte id) {
		if (id == 0)
			return this;
		return null;
	}

	public AClass getSuperClass() {
		if (super_class == 0)
			return null;
		Constant c = getConstant(super_class);
		c = getConstant(c.index1);
		return getClass(c.str);
	}


	@Override
	public int getAccessFlags() {
		return access_flags;
	}

	@Override
	public void getStaticReferecnes(Set<Integer> refs) {
		for(String name:static_references){
			refs.add((int)staticfields.get(name));
		}
	}

	@Override
	public List<String> getReferenceNames() {
		return references;
	}

	@Override
	public AClass[] getInterfaces() {
		AClass[] ics=new Class[interfaces_count];
		for (int ix = 0; ix < interfaces_count; ix++) {
			ics[ix]=getClass(interfaces[ix]);
		}
		return ics;
	}

	@Override
	public String getSourceFileName() {
		return source_file;
	}


	@Override
	public void getStatic(int i2, Frame cF) {
		Constant fref = getConstant(i2);
		Constant ntref = getConstant(fref.index2);
		Constant fnref = getConstant(ntref.index1);
		Constant ftref = getConstant(ntref.index2);

		if (fref.index1 == this_class) {
			getStatic(fnref.str, ftref.str, cF);
		} else {
			Constant cref = getConstant(fref.index1);

			String classname = getConstant(cref.index1).str;
			AClass c = getClass(classname);


			if (cref.index1 == this_class)
				getStatic(fnref.str, ftref.str, cF);
			else
				c.getStatic(fnref.str, ftref.str, cF);
		}
	}

	@Override
	public void getStatic(String name, String desc, Frame cF) {
		Object obj = staticfields.get(name);
		if (obj == null) {
			getSuperClass().getStatic(name, desc, cF);
			return;
		}
		switch (desc) {
			case "J":
				long l = (long) obj;
				cF.push(l);
				cF.push(null);
				break;
			case "D":
				l = (long) obj;
				cF.push(l);
				cF.push(null);
				break;
			default:
				cF.push(obj);
				break;
		}
	}

	@Override
	public Object getStatic(String name) {
		return staticfields.get(name);
	}

	private int l2i1(long l) {
		return (int) (l >> 32);
	}

	private int l2i2(long l) {
		return (int) l;
	}


	public Class(InputStream raw, boolean init,int size) throws Exception {
		boolean verbose=JVM.verbose;
		if(verbose)
			System.out.print("\r[▌ ] ");

		if(size<=0)
			size=16384;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream(size);

		int nRead;
		byte[] bytes = new byte[size];

		while ((nRead = raw.read(bytes, 0, bytes.length)) != -1) {
			buffer.write(bytes, 0, nRead);
		}
		if(verbose)
			System.out.print("\r[█ ] ");

		bytes = buffer.toByteArray();


		ByteBuffer buf = ByteBuffer.wrap(bytes);

		if (buf.order(ByteOrder.BIG_ENDIAN).getInt() != HEAD) {
			throw new Exception("not a valid class file");
		}
		int minor = buf.getChar(), ver = buf.getChar();

		constant_pool_count = buf.getChar();

		constants = new ArrayList<>(constant_pool_count);
		for (int ix = 1; ix < constant_pool_count; ix++) {
			Constant c = new Constant((short) ix, buf);
			if (c.getTag() == Constant.CLong || c.getTag() == Constant.CDouble) {
				ix++;
				constants.add(c);
			}


			constants.add(c);

		}
		if(verbose)
			System.out.print("\r[█▌] ");

		access_flags = buf.getChar();


		this_class = buf.getChar();

		name = getConstant(getConstant(this_class).index1).str;
		super_class = buf.getChar();


		interfaces_count = buf.getChar();


		interfaces = new int[interfaces_count];
		for (int ix = 0; ix < interfaces_count; ix++) {
			interfaces[ix] = buf.getChar();
		}

		fields_count = buf.getChar();

		boolean hasStaticNonFinalReferences=false;
		for (int ix = 0; ix < fields_count; ix++) {
			Field f = new Field(buf, this);
			fields.put(f.getName(), f);
			if (f.isStatic()) {
				putStatic(f.getName(), f.getDefaultValue());
				if(f.isObject()) {
					static_references.add(f.getName());
					//if (!f.isFinal())
						hasStaticNonFinalReferences = true;
				}

			}
			else
				if(f.isObject())
					references.add(f.getName());
		}
		if(verbose)
			System.out.print("\r[█▉] ");

		if(hasStaticNonFinalReferences)
			Heap.addClass(this);
		methods_count = buf.getChar();


		//AMethod[] methods = new AMethod[methods_count];
		constructors = new ArrayList<>(16);
		for (int ix = 0; ix < methods_count; ix++) {

			Method m = new Method(buf, this);

			methodsbyname.put(m.getSignature(), m);
			if (m.getSignature().startsWith("<init>")) {
				constructors.add(m);
			}
			//methods[ix] = m;
			//log(m.toString());
		}
		if(verbose)
			System.out.print("\r[██] ");
		int attributes_count = buf.getChar();
		attrs = new ArrayList<>(attributes_count);
		String source_file=null;
		for (int ix = 0; ix < attributes_count; ix++) {
			Attribute a = new Attribute(buf);
			if(a.getName(this).equals("SourceFile")){
				source_file=new SourceFileAttribute(a,this).getSourceFileName();
			}

			//attrs.add(a);
		}
		this.source_file=source_file;

		while (buf.hasRemaining())
			System.out.println((buf.position() + " -> " + buf.get()));

//        
		classes.put(name, this);
		initialized = init;
		if(verbose)
			System.out.println("\r[OK] ");
		myoref = new MyThread(Class.getClass("java/lang/Class"), "class_init").newInstance("<init>()V");
		Heap.makePermanent(myoref);
		ClassInstance ci = (ClassInstance) Heap.get(myoref);

		ci.cl = this;


		if (init)
			clinit();
		AClass sc = getSuperClass();
		if (sc != null)
			references.addAll(sc.getReferenceNames());
	}

	private Class setCodeSource(URL code_source) {
		this.code_source = code_source;
		return this;
	}

	public URL getCodeSource() {
		return code_source;
	}

	private ArrayList<Attribute> attrs;

	/*
	public class MyDataOutput implements DataOutput
	{

		private DataOutput out;
		public MyDataOutput(DataOutput out){
			this.out=out;
			
		}
		DataOutput raf;
		@Override
		public void write(int p1) throws IOException
		{
			int b=MainActivity.raf.read();
			if(p1!=b)
			throw new RuntimeException(p1 + "!="+b);
			out.write(p1);
			
		}

		@Override
		public void write(byte[] p1) throws IOException
		{
			byte[] ba=new byte[p1.length];
			MainActivity.raf.read(ba);
			for(int i=0;i<ba.length;i++){
				if(ba[i]!=p1[i])
					throw new RuntimeException();
			}
			out.write(p1);
		}

		@Override
		public void write(byte[] p1, int p2, int p3) throws IOException
		{
			out.write(p1,p2,p3);
		}

		@Override
		public void writeBoolean(boolean p1) throws IOException
		{
			if(p1!=MainActivity.raf.readBoolean())
				throw new RuntimeException(p1+"");
			out.writeBoolean(p1);
		}

		@Override
		public void writeByte(int p1) throws IOException
		{
			if(p1!=MainActivity.raf.read())
				throw new RuntimeException(p1+"");
			out.writeByte(p1);
		}

		@Override
		public void writeShort(int p1) throws IOException
		{
			short sh=MainActivity.raf.readShort();
			if(p1!=sh)
			throw new RuntimeException(p1 + " != "+sh);
			out.writeShort(p1);
		}

		@Override
		public void writeChar(int p1) throws IOException
		{
			int ch=MainActivity.raf.readChar();
			if(p1!=ch)
				throw new RuntimeException(Integer.toBinaryString(p1) + "!="+Integer.toBinaryString(ch));
			out.writeChar(p1);
		}

		@Override
		public void writeInt(int p1) throws IOException
		{
			if(p1!=MainActivity.raf.readInt())
				throw new RuntimeException(p1+"");
			out.writeInt(p1);
		}

		@Override
		public void writeLong(long p1) throws IOException
		{
			if(p1!=MainActivity.raf.readLong())
				throw new RuntimeException(p1+"");
			out.writeLong(p1);
		}

		@Override
		public void writeFloat(float p1) throws IOException
		{
			if(p1!=MainActivity.raf.readFloat())
				throw new RuntimeException(p1+"");
			out.writeFloat(p1);
		}

		@Override
		public void writeDouble(double p1) throws IOException
		{
			if(p1!=MainActivity.raf.readDouble())
				throw new RuntimeException(p1+"");
			out.writeDouble(p1);
		}

		@Override
		public void writeBytes(String p1) throws IOException
		{
			
			out.writeBytes(p1);
		}

		@Override
		public void writeChars(String p1) throws IOException
		{
			
			out.writeChars(p1);
		}

		@Override
		public void writeUTF(String p1) throws IOException
		{
			out.writeUTF(p1);
		}
		
		
	}

	 */
	public void save(File f) throws IOException {

		DataOutput raf =
				new RandomAccessFile(f, "rw");
		raf.writeInt(HEAD);
		//Version
		raf.writeShort(0);
		raf.writeShort(52);
		//constant_pool_count
		raf.writeShort(constant_pool_count);
		for (int i = 0; i < constants.size(); i++) {
			Constant c = constants.get(i);
			c.write(raf);
			if (c.getTag() == Constant.CLong || c.getTag() == Constant.CDouble) {
				i++;
			}
		}
		raf.writeChar(access_flags);
		raf.writeChar(this_class);
		raf.writeChar(super_class);
		raf.writeChar(interfaces_count);
		for (int ix = 0; ix < interfaces_count; ix++) {
			raf.writeChar(interfaces[ix]);
		}
		raf.writeChar(fields_count);
		for (AField field : fields.values()) {
			field.write(raf);
		}
		raf.writeChar(methods_count);

		for (Method m : methodsbyname.values()) {
			m.write(raf);
		}
		raf.writeChar(attrs.size());
		for (Attribute a : attrs) {
			a.write(raf);
		}
	}

	public MyThread createInitMethod() {
		return new MyThread(this, "clinit",true)
				.setMethodToRun("<clinit>()V");
	}

	@Override
	public void clinit() {
		//System.out.println("calling clinit for "+name);

		MyThread t = createInitMethod();
		try {
			initialized = true;
			t.run();
			methodsbyname.remove("<clinit>()V");
		} catch (Exception e) {
			throw new RuntimeException(name + ".<clinit>()V" + t.getStackTrace(), e);
		}
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}


//    public AMethod getMyMethod(int id) {
//        Constant mc = getConstant(id);
//        int cref = mc.index1;
//        int mref = mc.index2;
//        //int nid = cp.getConstant(mref - 1).index1;
//        //int tid = cp.getConstant(mref - 1).index2;
//        //id=constants.getConstant(ntref-1).index1;
//        //String mname=cp.getConstant(nid - 1).str;
//        //mname+=cp.getConstant(tid - 1).str;
//
//        if (cref == this_class) {
//            return methods.get(id);
//        } else {
//            int nid = getConstant(mref).index1;
//            int tid = getConstant(mref).index2;
//            //id=cp.getConstant(ntref-1).index1;
//            String mname = getConstant(nid).str;
//            String parameters = getConstant(tid).str;
//            AMethod m = methodsbyname.get(mname);
//            if (m != null)
//                return m;
//
//            return getClass(cref).getMethod(mname, parameters, id - 1);
//        }
//    }

	Map<Integer, AMethod> resolvedMethods=new HashMap<>();
	public AMethod getMethod(int id) {
		AMethod m=resolvedMethods.get(id);
		if(m==null) {
			Constant mc = getConstant(id);
			int cref = mc.index1;
			int mref = mc.index2;
			//int nid = cp.getConstant(mref - 1).index1;
			//int tid = cp.getConstant(mref - 1).index2;
			//id=constants.getConstant(ntref-1).index1;
			//String mname=cp.getConstant(nid - 1).str;
			//mname+=cp.getConstant(tid - 1).str;

			int nid = getConstant(mref).index1;
			int tid = getConstant(mref).index2;
			//id=cp.getConstant(ntref-1).index1;
			String mname = getConstant(nid).str;
			String parameters = getConstant(tid).str;
			//parameters=parameters.substring(0,parameters.indexOf(')')+1);
			if (cref == this_class) {
				m= getMethod(mname + parameters);
			} else {

				m= getClass(cref).getMethod(mname+parameters);
			}
			resolvedMethods.put(id,m);
		}
		return m;
	}

	@Override
	public AMethod getMethod(String nameAndDesc) {
		AMethod m = methodsbyname.get(nameAndDesc);
		if (m != null)
			return m;
		AClass sc = getSuperClass();
		if (sc == null) return null;
		return sc.getMethod(nameAndDesc);
	}



	public AClass getClass(int cref) {
		Constant c = getConstant(cref);
		c = getConstant(c.index1);
		String s = c.str;
		return getClass(s);
	}


//	method_info {
//		u2             access_flags;
//		u2             name_index;
//		u2             descriptor_index;
//		u2             attributes_count;
//		attribute_info attributes[attributes_count];
//	}

//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}


	@Override
	public String toString() {
		return name;
	}

}
