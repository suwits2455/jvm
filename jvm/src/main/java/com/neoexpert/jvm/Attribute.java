package com.neoexpert.jvm;

import com.neoexpert.jvm._abstract.AClass;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Attribute {
	protected Attribute(Attribute a) {
		this.attribute_length = a.attribute_length;
		this.info = a.info;
		this.attribute_name_index = a.attribute_name_index;
	}

	//	attribute_info {
//		u2 attribute_name_index;
//		u4 attribute_length;
//		u1 info[attribute_length];
//	}
	public Attribute(ByteBuffer buf) {
		attribute_name_index = buf.getChar();
		//log("attribute_name_index: "+Integer.toHexString(af1));
		attribute_length = buf.getInt();
		info = new byte[attribute_length];
		buf.get(info);
		//log("attribute_length: "+Integer.toHexString(attribute_length));
		//for(int ix=0; ix<attribute_length; ix++){
		//	info[ix]=buf.get();
		//}

		//info=new byte[attribute_length];
	}

	public int attribute_name_index;
	public int attribute_length;
	public byte[] info;

	public void write(DataOutput raf) throws IOException {
		raf.writeChar(attribute_name_index);
		raf.writeInt(info.length);
		raf.write(info);
	}

	@Override
	public String toString() {
		String s = "";
		s += "attribute_name_index: " + attribute_name_index + "\n";
		s += "attribute_length: " + attribute_length + "\n";

		return s + bytesToHex(info);
	}

	private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

	private static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public String getName(AClass c) {
		return c.getConstant(attribute_name_index).str;
	}
}
