package com.neoexpert.jvm;
import java.util.*;

public class ThreadScheduler{
	private LinkedList<MyThread> threads=new LinkedList<>();
	private LinkedList<MyThread> newthreads=new LinkedList<>();
	public void notify(MyThread t){
		newthreads.add(t);
	}
	public void setMainThread(MyThread main_thread){
		threads.add(main_thread);
	}
	public void add(MyThread t){
		if(t.isDaemon()){}
		else
			newthreads.add(t);
	}

	public void run(){
		while(!threads.isEmpty()){
			ListIterator<MyThread> it=threads.listIterator(0);
			while(it.hasNext()){
				if(it.next().prun())
				{
					it.remove();
				}
			}	
			if(!newthreads.isEmpty()){
				threads.addAll(newthreads);
				newthreads.clear();
			}
		}
	}
}
