package com.neoexpert.jvm.adapter;

import com.neoexpert.jvm.Frame;
import com.neoexpert.jvm.FrameStack;

public interface DebuggerAdapter {
	void setFrame(Frame f);

	void setFrameStack(FrameStack stack);
}
