# JVM in pure java

This JVM can run simple Java programs and some advanced ones. The goal was to be able to run it on itself. This goal is achieved. However some of bytecode OPs are still not implemented. To run some advanced software you need to implement missing native methods. All the current native method implementations can be found under "jvm/src/main/java/com/neoexpert/jvm/natives".

you need JDK8 to compile the JDK-classes
```
javac Main.java
mvn clean install
cd jvm/jvm/target
java -jar java.jar -jar java.jar Main
```
