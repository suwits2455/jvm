package com.neoexpert.android;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.*;
import java.nio.*;
import fr.xgouchet.axml.*;
import org.w3c.dom.*;
import com.neoexpert.jvm._class.*;

public class Main{
	private static void printNode(Node rootNode, String spacer) {
			System.out.println(spacer + rootNode.getNodeName() + " -> " + rootNode.getNodeValue());
			NodeList nl = rootNode.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++)
					printNode(nl.item(i), spacer + "   ");
	}
	public static void main(String[] args) throws Exception{
		ZipFile apk=new ZipFile(args[0]);

		ZipEntry manifest=apk.getEntry("AndroidManifest.xml");
		InputStream raw = apk.getInputStream(manifest);
		Document manifestdoc=new CompressedXmlParser().parseDOM(raw);
		Element manifest_element=manifestdoc.getDocumentElement();
		System.out.println(manifest_element.getTagName());
		String _package=manifest_element.getAttribute("package");
		Element application_element=(Element)manifest_element.getElementsByTagName("application").item(0);
		NodeList activities=manifest_element.getElementsByTagName("activity");

		for (int i = 0; i < activities.getLength(); i++) {
				Element activity_element = (Element) activities.item(i);
				String name=activity_element.getAttribute("android:name");
				System.out.println(name);
		}
		ZipEntry classes=apk.getEntry("classes.dex");

		raw = apk.getInputStream(classes);
		DexClass.load(raw,(int)classes.getSize());
	}

}
